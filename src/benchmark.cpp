#include <benchmark/benchmark.h>
#include <png.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <random>
#include <string>

#include "container/grid2.h"
#include "container/kary_tree.h"
#include "noise/basis/cellular_noise.h"
#include "noise/basis/fbm_noise.h"
#include "noise/basis/modified_noise.h"
#include "noise/basis/ridge_noise.h"
#include "noise/basis/simplex_noise.h"
#include "noise/basis/turbulence_noise.h"
#include "noise/noise_function.h"
#include "noise/sampling/poisson_sampler.h"
#include "terrain/erosion/droplet_erosion_simulation.h"
#include "terrain/erosion/flow_erosion_simulation.h"
#include "terrain/heightmap/heightmap_renderer.h"
#include "terrain/road/road_generator.h"
#include "terrain/road/road_renderer.h"

auto blend = 0.33;
auto width = 4096;
auto height = 4096;
auto frequency = 8;
auto pert_magnitude = 1.0 / frequency;
auto num_nodes = 1000;

using namespace noise;

static void save_png(grid2f const& noise, std::string const& file_name)
{
    // Initialize libpng.
    auto png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    auto info_ptr = png_create_info_struct(png_ptr);
    auto depth = 8;

    png_set_IHDR(png_ptr,
                 info_ptr,
                 noise.width(),
                 noise.height(),
                 depth,
                 PNG_COLOR_TYPE_GRAY,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_BASE);

    // Write rows.
    auto row_pointers = static_cast<png_bytepp>(png_malloc(png_ptr, noise.height() * sizeof(png_byte*)));
    for (size_t y = 0; y < noise.height(); ++y)
    {
        auto row = static_cast<png_bytep>(png_malloc(png_ptr, 3 * sizeof(uint8_t) * noise.width()));
        row_pointers[y] = row;
        for (size_t x = 0; x < noise.width(); ++x)
        {
            *row++ = static_cast<png_byte>(255 * noise(x, noise.height() - y - 1));
        }
    }

    // Write to file.
    auto fp = fopen(file_name.c_str(), "wb");
    png_init_io(png_ptr, fp);
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, NULL);

    // Free memory.
    for (size_t y = 0; y < noise.height(); y++)
    {
        png_free(png_ptr, row_pointers[y]);
    }
    png_free(png_ptr, row_pointers);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
}

template <typename T>
static void benchmark_noise(benchmark::State& state, T const& noise, std::string const& filename)
{
    auto modified = modified_noise(&noise);
    modified.add_normalize_modifier(0, 1);

    auto renderer = heightmap_renderer(width, height);
    auto bounds = heightmap_renderer::bounds_type(0, 0, frequency, frequency);
    for (auto _ : state)
    {
        renderer.draw_noise(modified, bounds);
    }

    save_png(renderer.heightmap(), filename);
}

static cellular_noise<1, euclidean_distance_metric> get_cellular_noise_f1(seed_type seed = 0)
{
    auto weights = std::array<decimal_type, 1> { 1 };
    auto noise = cellular_noise<1, euclidean_distance_metric>(seed, weights);
    return noise;
}

static cellular_noise<2, euclidean_distance_metric> get_cellular_noise_f1_f2(seed_type seed = 1)
{
    auto weights = std::array<decimal_type, 2> { -1, 1 };
    auto noise = cellular_noise<2, euclidean_distance_metric>(seed, weights);
    return noise;
}

static grid2f get_terrain_heightmap(seed_type seed = 0)
{
    auto heightmap = grid2f(width, height);

    // Base noise
    auto simplex1 = simplex_noise(seed);
    auto fractal = fbm_noise(&simplex1, 8);
    auto cellular = get_cellular_noise_f1_f2(seed);

    // Domain warp
    auto simplex2 = simplex_noise(seed + 1);
    auto simplex3 = simplex_noise(seed + 2);
    auto warp_x = fbm_noise(&simplex2);
    auto warp_y = fbm_noise(&simplex3);

    auto noise = modified_noise(&fractal);
    // noise.add_domain_warp_modifier(&warp_x, &warp_y, pert_magnitude);
    noise.add_normalize_modifier(0, 1);

    auto renderer = heightmap_renderer(heightmap.width(), heightmap.height());
    auto bounds = heightmap_renderer::bounds_type(0, 0, frequency, frequency);
    renderer.draw_noise(noise, bounds);
    return renderer.heightmap();
}

static std::vector<road_vertex> get_road_vertices()
{
    auto random = std::uniform_real_distribution<decimal_type>(0, 1);
    auto random_engine = random_engine_type(0);
    auto vertices = std::vector<road_vertex>(num_nodes);

    for (size_t i = 0; i < vertices.size(); ++i)
    {
        auto x = random(random_engine);
        auto y = random(random_engine);
        vertices[i] = road_vertex(x, y);
    }

    return vertices;
}

static void benchmark_scale(benchmark::State& state, interpolation_mode mode)
{
    auto heightmap = get_terrain_heightmap();

    auto heightmap_scaled = grid2f(width >> 1, height >> 1);
    for (auto _ : state)
    {
        heightmap.scale(heightmap_scaled, mode);
    }
}

static void benchmark_scale_nearest_neighbour(benchmark::State& state)
{
    benchmark_scale(state, interpolation_mode::nearest_neighbour);
}

static void benchmark_scale_bilinear(benchmark::State& state)
{
    benchmark_scale(state, interpolation_mode::bilinear);
}

static void benchmark_scale_bicubic(benchmark::State& state)
{
    benchmark_scale(state, interpolation_mode::bicubic);
}

static void benchmark_simplex_noise(benchmark::State& state)
{
    auto noise = simplex_noise();
    benchmark_noise(state, noise, "simplex_noise_heightmap.png");
}

static void benchmark_cellular_fbm_noise(benchmark::State& state)
{
    auto basis = get_cellular_noise_f1_f2();
    auto noise = fbm_noise(&basis, 8);
    benchmark_noise(state, noise, "cellular_fmb_noise_heightmap.png");
}

static void benchmark_fbm_noise(benchmark::State& state)
{
    auto basis = simplex_noise();
    auto noise = fbm_noise(&basis, 8);
    benchmark_noise(state, noise, "fmb_noise_heightmap.png");
}

static void benchmark_ridge_noise(benchmark::State& state)
{
    auto basis = simplex_noise();
    auto noise = ridge_noise(&basis, 8, 1.0f);
    benchmark_noise(state, noise, "ridge_noise_heightmap.png");
}

static void benchmark_turbulence_noise(benchmark::State& state)
{
    auto basis = simplex_noise();
    auto noise = turbulence_noise(&basis, 8, 1.0f);
    benchmark_noise(state, noise, "turbulence_noise_heightmap.png");
}

static void benchmark_cellular_noise_f1(benchmark::State& state)
{
    auto noise = get_cellular_noise_f1();
    benchmark_noise(state, noise, "cellular_noise_f1_heightmap.png");
}

static void benchmark_cellular_noise_f1_f2(benchmark::State& state)
{
    auto noise = get_cellular_noise_f1_f2();
    benchmark_noise(state, noise, "cellular_noise_f1_f2_heightmap.png");
}

static void benchmark_terrain_heightmap(benchmark::State& state)
{
    auto heightmap = grid2f();
    for (auto _ : state)
    {
        heightmap = get_terrain_heightmap();
    }

    save_png(heightmap, "terrain_heightmap.png");
}

static void benchmark_steepness_map(benchmark::State& state)
{
    auto heightmap = get_terrain_heightmap();
    auto renderer = heightmap_renderer(heightmap.width(), heightmap.height());

    for (auto _ : state)
    {
        renderer.clear();
        renderer.draw_steepness_map(heightmap, vec3f(128, 128, 128));
    }

    save_png(renderer.heightmap(), "steepnessmap.png");
}

static void benchmark_generate_road_graph(benchmark::State& state)
{
    auto vertices = get_road_vertices();
    auto generator = road_generator();

    for (auto _ : state)
    {
        generator.generate_road_graph(vertices);
    }
}

static void benchmark_generate_road_paths(benchmark::State& state)
{
    auto vertices = get_road_vertices();
    auto generator = road_generator();
    auto heightmap = get_terrain_heightmap();
    auto edges = generator.generate_road_graph(vertices);

    for (auto _ : state)
    {
        generator.generate_road_paths(vertices, edges, heightmap);
    }
}

static void benchmark_draw_road_paths(benchmark::State& state)
{
    auto vertices = get_road_vertices();
    auto generator = road_generator();
    auto heightmap = get_terrain_heightmap();
    auto edges = generator.generate_road_graph(vertices);
    auto renderer = road_renderer(width, height);
    auto paths = generator.generate_road_paths(vertices, edges, heightmap);

    for (auto _ : state)
    {
        renderer.draw(paths);
    }

    save_png(renderer.result(), "road_heightmap.png");
}

static void benchmark_poisson_sampler(benchmark::State& state)
{
    auto params = poisson_sampler_params();
    params.seed = 0;
    params.distance = 0.01;
    params.num_candidates = 30;
    auto sampler = poisson_sampler(params);

    for (auto _ : state)
    {
        auto points = sampler.sample();
    }
}

static void benchmark_draw_poisson_sampler(benchmark::State& state)
{
    auto params = poisson_sampler_params();
    params.seed = 0;
    params.distance = 0.001;
    params.num_candidates = 30;
    auto sampler = poisson_sampler(params);
    auto points = sampler.sample();

    auto renderer = heightmap_renderer(width, height);

    for (auto _ : state)
    {
        renderer.set_color(1.0);
        renderer.fill();
        renderer.set_thickness(1.0);
        renderer.set_color(0.0);

        for (auto const& p : points)
        {
            auto point = vec2f(width * p.x, height * p.y);
            renderer.draw_point(point);
        }
    }

    save_png(renderer.heightmap(), "poisson_samples.png");
}

static void benchmark_terrain_flow_erosion(benchmark::State& state)
{
    auto params = flow_erosion_simulation_params();

    params.terrain_size = params.terrain_size.make_right_up_forward(256, 128, 256);
    params.capacity = 4.0;
    params.deposition = 1.0;
    params.dissolve = 0.5;
    params.evaporation = 0.5;
    params.gravity = 9.81;
    params.hydraulic_time_step = 0.02;
    params.max_erosion_depth = 10.0;
    params.rain = 0.1;
    params.talus_angle = degree_to_radian(70);
    params.thermal_time_step = 0.02;
    params.virtual_pipe_area = 1.0;
    params.virtual_pipe_length = 1.0;

    auto iterations = 100;
    auto heightmap = get_terrain_heightmap();
    auto simulation = flow_erosion_simulation(heightmap, params);

    for (auto _ : state)
    {
        simulation.update(iterations);
    }

    for (size_t i = 0; i < heightmap.size(); ++i)
    {
        heightmap[i] = simulation.heightmap()[i] / params.terrain_size.up();
    }

    save_png(heightmap, "flow_erosion_heightmap.png");
}

static void benchmark_terrain_droplet_erosion(benchmark::State& state)
{
    auto params = droplet_erosion_simulation_params();
    params.capacity = 4.0;
    params.deposition = 0.3;
    params.dissolve = 0.3;
    params.droplet_inertia = 0.05;
    params.droplet_lifetime = 30;
    params.droplet_radius = 3;
    params.droplet_speed = 1;
    params.droplet_volume = 1;
    params.evaporation = 0.01;
    params.gravity = 9.81;
    params.minimum_capacity = 0.01;
    params.seed = 0;
    params.terrain_size = params.terrain_size.make_right_up_forward(256, 100, 256);

    auto droplets = 100000;
    auto heightmap = get_terrain_heightmap();
    auto simulation = droplet_erosion_simulation(heightmap, params);

    for (auto _ : state)
    {
        simulation.update(droplets);
    }

    save_png(simulation.heightmap(), "droplet_erosion_heightmap.png");
}

static void benchmark_quadtree_bfs(benchmark::State& state)
{
    auto tree = KAryTree<int, 4>(10);
    for (auto _ : state)
    {
        for (auto it = tree.begin_bfs(); it != tree.end_bfs(); ++it)
        {
            it->data() = it->depth();
        }
    }
}

static void benchmark_quadtree_dfs(benchmark::State& state)
{
    auto tree = KAryTree<int, 4>(10);
    for (auto _ : state)
    {
        for (auto it = tree.begin_dfs(); it != tree.end_dfs(); ++it)
        {
            it->data() = it->depth();
        }
    }
}

BENCHMARK(benchmark_simplex_noise)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_fbm_noise)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_ridge_noise)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_turbulence_noise)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_cellular_noise_f1)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_cellular_noise_f1_f2)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_cellular_fbm_noise)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_terrain_flow_erosion)->Unit(benchmark::kMillisecond);

BENCHMARK(benchmark_terrain_heightmap)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_steepness_map)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_scale_nearest_neighbour)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_scale_bicubic)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_scale_bilinear)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_generate_road_graph)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_generate_road_paths)->Unit(benchmark::kMillisecond);
// BENCHMARK(benchmark_draw_road_paths)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_poisson_sampler)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_draw_poisson_sampler)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_terrain_droplet_erosion)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_quadtree_bfs)->Unit(benchmark::kMillisecond);
BENCHMARK(benchmark_quadtree_dfs)->Unit(benchmark::kMillisecond);

BENCHMARK_MAIN();
