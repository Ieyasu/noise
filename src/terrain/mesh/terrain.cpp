#include "terrain/mesh/terrain.h"

#include "math/math.h"

namespace noise
{
    grid2f const& Terrain::heightmap() const
    {
        return m_heightmap;
    }

    Terrain::lod_type Terrain::max_lod() const
    {
        return m_max_lod;
    }

    vec2s const& Terrain::patch_count() const
    {
        return m_patch_count;
    }

    vec2s const& Terrain::patch_resolution() const
    {
        return m_patch_resolution;
    }

    vec3f const& Terrain::patch_size() const
    {
        return m_patch_size;
    }

    vec3f const& Terrain::world_size() const
    {
        return m_world_size;
    }

    void Terrain::set_heightmap(grid2f const& heightmap)
    {
        if (heightmap.width() == 0 || heightmap.height() == 0)
        {
            throw std::invalid_argument("Heightmap resolution must not be zero.");
        }

        if (!is_power_of_two(heightmap.width() - 1) || !is_power_of_two(heightmap.height() - 1))
        {
            throw std::invalid_argument("Heightmap resolution must be power of two + 1.");
        }

        m_heightmap = heightmap;

        update_patch_size();
        update_quadtree_depth();
        update_bounding_boxes();
    }

    void Terrain::set_world_size(vec3f const& world_size)
    {
        m_world_size = vec3f::max(world_size, vec3f::zero());

        update_patch_size();
    }

    TerrainPatch Terrain::generate_patch(lod_type lod) const
    {
        auto const width = m_patch_resolution.x >> lod;
        auto const height = m_patch_resolution.y >> lod;

        auto patch = TerrainPatch();
        patch.vertices.resize((width + 1) * (height + 1));
        patch.triangles.resize(6 * width * height);

        for (size_t y = 0; y < height + 1; ++y)
        {
            for (size_t x = 0; x < width + 1; ++x)
            {
                patch.vertices[x + y * (width + 1)] = vec3f::make_right_up_forward(x, 0, y);
            }
        }

        for (size_t y = 0; y < height; ++y)
        {
            for (size_t x = 0; x < width; ++x)
            {
                auto const idx = 6 * (x + y * width);
                auto const vert_idx = x + y * (width + 1);

                if ((x + y) % 2 == 0)
                {
                    patch.triangles[idx + 0] = vert_idx;
                    patch.triangles[idx + 1] = vert_idx + 1;
                    patch.triangles[idx + 2] = vert_idx + width + 1;

                    patch.triangles[idx + 3] = vert_idx + width + 2;
                    patch.triangles[idx + 4] = vert_idx + width + 1;
                    patch.triangles[idx + 5] = vert_idx + 1;
                }
                else
                {
                    patch.triangles[idx + 0] = vert_idx + 1;
                    patch.triangles[idx + 1] = vert_idx + width + 2;
                    patch.triangles[idx + 2] = vert_idx;

                    patch.triangles[idx + 3] = vert_idx + width + 1;
                    patch.triangles[idx + 4] = vert_idx;
                    patch.triangles[idx + 5] = vert_idx + width + 2;
                }
            }
        }

        return patch;
    }

    void Terrain::update_quadtree_depth()
    {
        auto const depth = std::log2(std::max(m_patch_count.x, m_patch_count.y));
        m_quadtree.resize(depth);
    }

    void Terrain::update_bounding_boxes()
    {
        for (auto it = m_quadtree.rbegin_bfs(); it != m_quadtree.rend_bfs(); ++it)
        {
            if (it->is_leaf())
            {
                auto const coord = morton_decode(it->level_index());
                auto const width = m_patch_resolution.x / static_cast<decimal_type>(m_heightmap.width());
                auto const depth = m_patch_resolution.y / static_cast<decimal_type>(m_heightmap.height());
                auto const height = m_heightmap(coord.x * m_patch_resolution.x, coord.y * m_patch_resolution.y);
                auto const min = vec3f::make_right_up_forward(coord.x * width, coord.y * depth, 0);
                auto const max = vec3f::make_right_up_forward(width, depth, height) + min;
                it->data() = AABB(min, max);
            }
            else
            {
                it->data() = AABB::combine({ it->child(0).data(), it->child(1).data(), it->child(2).data(), it->child(3).data() });
            }
        }
    }

    void Terrain::update_patch_size()
    {
        m_patch_resolution.x = std::min(static_cast<size_t>(17), m_heightmap.width());
        m_patch_resolution.y = std::min(static_cast<size_t>(17), m_heightmap.height());
        m_patch_count.x = (m_heightmap.width() - 1) / (m_patch_resolution.x - 1);
        m_patch_count.y = (m_heightmap.height() - 1) / (m_patch_resolution.y - 1);
        m_patch_size = vec3f::make_right_up_forward(m_world_size.right() * m_patch_resolution.x / m_heightmap.width(),
                                                    m_world_size.up(),
                                                    m_world_size.forward() * m_patch_resolution.y / m_heightmap.height());
        m_max_lod = lod_type(std::log2(std::min(m_patch_resolution.x - 1, m_patch_resolution.y - 1)));
    }

    void Terrain::update_visibility()
    {
        m_visible_patches.clear();
        for (auto it = m_quadtree.begin_dfs(); it != m_quadtree.end_dfs();)
        {
            // Scale bounds to world scale.
            auto bounds = it->data();
            bounds.min *= m_world_size;
            bounds.max *= m_world_size;

            auto const distance = bounds.distance_to_point(position);
            auto const is_visible = true;  //(distance >) || (frustrum cull);

            // Add visible nodes to list.
            if (is_visible && it->is_leaf())
            {
                auto occlusion_result = TerrainOcclusionResult();
                occlusion_result.lod = calculate_lod(distance);
                occlusion_result.coord = morton_decode(it->level_index());
                m_visible_patches.push_back(occlusion_result);
            }

            // Jump over non visible sub trees.
            if (!is_visible)
            {
                it.jump_over();
            }
            else
            {
                ++it;
            }
        }
    }

    inline Terrain::lod_type Terrain::calculate_lod(decimal_type distance) const
    {
        return 0;
    }

    inline bool Terrain::calculate_visibility(AABB const& bounds) const
    {
    }
}  // namespace noise
