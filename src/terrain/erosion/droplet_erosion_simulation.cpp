#include "terrain/erosion/droplet_erosion_simulation.h"

#include <iostream>

namespace noise
{
    droplet_erosion_simulation::droplet_erosion_simulation(grid2f const& heightmap, droplet_erosion_simulation_params const& params) :
        m_erosion_brush(get_brush(params.droplet_radius)),
        m_grid_size(heightmap.size()),
        m_grid_width(heightmap.width()),
        m_grid_height(heightmap.height()),
        m_heightmap(heightmap),
        m_random_engine(params.seed),
        m_params(params)
    {
    }

    grid2f const& droplet_erosion_simulation::heightmap() const
    {
        return m_heightmap;
    }

    droplet_erosion_simulation_params const& droplet_erosion_simulation::params() const
    {
        return m_params;
    }

    void droplet_erosion_simulation::update(size_t iterations)
    {
        auto dist_x = std::uniform_real_distribution<decimal_type>(0, m_grid_width - 1);
        auto dist_y = std::uniform_real_distribution<decimal_type>(0, m_grid_height - 1);
        for (size_t i = 0; i < iterations; ++i)
        {
            auto const xpos = dist_x(m_random_engine);
            auto const ypos = dist_y(m_random_engine);
            simulate_droplet(vec2f(xpos, ypos));
        }

        // auto const radius = m_params.droplet_lifetime;
        // auto const cells_x = static_cast<size_t>(std::ceil(m_grid_width / static_cast<decimal_type>(4 * radius)));
        // auto const cells_y = static_cast<size_t>(std::ceil(m_grid_height / static_cast<decimal_type>(4 * radius)));
        // auto const num_cells = cells_x * cells_y;
        // auto dist_x = std::uniform_real_distribution<decimal_type>(0, 2 * radius - 1);
        // auto dist_y = std::uniform_real_distribution<decimal_type>(0, 2 * radius - 1);

        // std::cout << "START" << std::endl;
        // std::cout << radius << std::endl;
        // std::cout << cells_x << std::endl;
        // std::cout << cells_y << std::endl;
        // for (size_t i = 0; i < iterations; ++i)
        // {
        //     // #pragma omp parallel
        //     {
        //         // #pragma omp for
        //         for (size_t j = 0; j < num_cells; ++j)
        //         {
        //             auto const x = 2 * (j % cells_x);
        //             auto const y = 2 * (j / cells_x);
        //             auto const xpos = dist_x(m_random_engine) + 2 * radius * x;
        //             auto const ypos = dist_y(m_random_engine) + 2 * radius * y;
        //             if (xpos <= m_grid_width - 1 && ypos <= m_grid_height - 1)
        //             {
        //                 simulate_droplet(vec2f(xpos, ypos));
        //             }
        //         }

        //         // #pragma omp for
        //         for (size_t j = 0; j < num_cells; ++j)
        //         {
        //             auto const x = 2 * (j % cells_x);
        //             auto const y = 2 * (j / cells_x);
        //             auto const xpos = dist_x(m_random_engine) + 2 * radius * (x + 1);
        //             auto const ypos = dist_y(m_random_engine) + 2 * radius * y;
        //             if (xpos <= m_grid_width - 1 && ypos <= m_grid_height - 1)
        //             {
        //                 simulate_droplet(vec2f(xpos, ypos));
        //             }
        //         }

        //         // #pragma omp for
        //         for (size_t j = 0; j < num_cells; ++j)
        //         {
        //             auto const x = 2 * (j % cells_x);
        //             auto const y = 2 * (j / cells_x);
        //             auto const xpos = dist_x(m_random_engine) + 2 * radius * x;
        //             auto const ypos = dist_y(m_random_engine) + 2 * radius * (y + 1);
        //             if (xpos <= m_grid_width - 1 && ypos <= m_grid_height - 1)
        //             {
        //                 simulate_droplet(vec2f(xpos, ypos));
        //             }
        //         }

        //         // #pragma omp for
        //         for (size_t j = 0; j < num_cells; ++j)
        //         {
        //             auto const x = 2 * (j % cells_x);
        //             auto const y = 2 * (j / cells_x);
        //             // std::cout << x << " " << y << std::endl;

        //             auto const xpos = dist_x(m_random_engine) + 2 * radius * (x + 1);
        //             auto const ypos = dist_y(m_random_engine) + 2 * radius * (y + 1);
        //             if (xpos <= m_grid_width - 1 && ypos <= m_grid_height - 1)
        //             {
        //                 simulate_droplet(vec2f(xpos, ypos));
        //             }
        //         }
        //     }
        // }
    }

    void droplet_erosion_simulation::simulate_droplet(vec2f pos)
    {
        auto const k_capacity = m_params.capacity;
        auto const k_deposition = m_params.deposition;
        auto const k_dissolve = m_params.dissolve;
        auto const k_evaporation = m_params.evaporation;
        auto const k_gravity = m_params.gravity;
        auto const k_inertia = m_params.droplet_inertia;
        auto const k_min_capacity = m_params.minimum_capacity;

        // Create water droplet at random point on map
        auto dir = vec2f();
        auto sediment = decimal_type(0);
        auto speed = m_params.droplet_speed;
        auto water = m_params.droplet_volume;

        for (size_t i = 0; i < m_params.droplet_lifetime; ++i)
        {
            auto const node = vec2i(pos.x, pos.y);
            auto const cell_offset = vec2f(pos.x - node.x, pos.y - node.y);

            auto const height_with_gradients = calculate_height_with_gradients(pos);
            dir.x = lerp_unclamped(-height_with_gradients.y, dir.x, k_inertia);
            dir.y = lerp_unclamped(-height_with_gradients.z, dir.y, k_inertia);
            if (dir.x == 0 && dir.y == 0)
            {
                break;
            }

            dir.normalize();
            pos += dir;

            if (pos.x < 0 || pos.x >= m_grid_width - 1 || pos.y < 0 || pos.y >= m_grid_height - 1)
            {
                break;
            }

            auto const new_height = calculate_height(pos);
            auto const delta_height = new_height - height_with_gradients.x;
            auto const capacity = std::max(k_min_capacity, -delta_height * speed * water * k_capacity);

            // If moving uphill, try fill up to the current height,
            if (delta_height > 0)
            {
                auto const sediment_change = std::min(delta_height, sediment);
                sediment -= sediment_change;

                auto const droplet_index = node.y * m_grid_width + node.x;
                m_heightmap[droplet_index] += sediment_change * (1 - cell_offset.x) * (1 - cell_offset.y);
                m_heightmap[droplet_index + 1] += sediment_change * cell_offset.x * (1 - cell_offset.y);
                m_heightmap[droplet_index + m_grid_width] += sediment_change * (1 - cell_offset.x) * cell_offset.y;
                m_heightmap[droplet_index + m_grid_width + 1] += sediment_change * cell_offset.x * cell_offset.y;
            }
            // Deposit excess sediment.
            else if (sediment > capacity)
            {
                auto const sediment_change = k_deposition * (sediment - capacity);
                sediment -= sediment_change;

                auto const droplet_index = node.y * m_grid_width + node.x;
                m_heightmap[droplet_index] += sediment_change * (1 - cell_offset.x) * (1 - cell_offset.y);
                m_heightmap[droplet_index + 1] += sediment_change * cell_offset.x * (1 - cell_offset.y);
                m_heightmap[droplet_index + m_grid_width] += sediment_change * (1 - cell_offset.x) * cell_offset.y;
                m_heightmap[droplet_index + m_grid_width + 1] += sediment_change * cell_offset.x * cell_offset.y;
            }
            // Erode.
            else
            {
                auto const erode_amount = std::min(-delta_height, k_dissolve * (capacity - sediment));
                for (size_t j = 0; j < m_erosion_brush.deltas.size(); ++j)
                {
                    auto const coord = node + m_erosion_brush.deltas[j];
                    if (coord.x >= 0 && coord.x < static_cast<int>(m_grid_width) && coord.y >= 0 && coord.y < static_cast<int>(m_grid_height))
                    {
                        auto const sediment_change = std::min(m_heightmap(coord.x, coord.y), erode_amount * m_erosion_brush.weights[j]);
                        m_heightmap(coord.x, coord.y) -= sediment_change;
                        sediment += sediment_change;
                    }
                }
            }

            // Update droplet's speed and water content
            speed = std::sqrt(speed * speed + delta_height * k_gravity);
            water *= (1 - k_evaporation);
        }
    }

    droplet_erosion_simulation::erosion_brush droplet_erosion_simulation::get_brush(int radius) const
    {
        auto brush = erosion_brush();
        auto weight_sum = decimal_type(0);
        for (auto y = -radius; y <= radius; ++y)
        {
            for (auto x = -radius; x <= radius; ++x)
            {
                auto const distance_sqr = decimal_type(x * x + y * y);
                if (distance_sqr < radius * radius)
                {
                    auto const weight = 1.0 - std::sqrt(distance_sqr) / radius;
                    brush.deltas.push_back(vec2i(x, y));
                    brush.weights.push_back(weight);
                    weight_sum += weight;
                }
            }
        }

        for (auto& w : brush.weights)
        {
            w /= weight_sum;
        }

        return brush;
    }

    inline decimal_type droplet_erosion_simulation::calculate_height(vec2f const& pos) const
    {
        auto const cx = static_cast<size_t>(pos.x);
        auto const cy = static_cast<size_t>(pos.y);

        auto const fx = pos.x - cx;
        auto const fy = pos.y - cy;

        auto const i = cx + cy * m_grid_width;
        auto const val11 = m_heightmap[i];
        auto const val21 = m_heightmap[i + 1];
        auto const val12 = m_heightmap[i + m_grid_width];
        auto const val22 = m_heightmap[i + m_grid_width + 1];
        auto const val1 = lerp_unclamped(val11, val21, fx);
        auto const val2 = lerp_unclamped(val12, val22, fx);
        auto const height = lerp_unclamped(val1, val2, fy);

        return height;
    }

    inline vec3f droplet_erosion_simulation::calculate_height_with_gradients(vec2f const& pos) const
    {
        auto const cx = static_cast<size_t>(pos.x);
        auto const cy = static_cast<size_t>(pos.y);

        auto const fx = pos.x - cx;
        auto const fy = pos.y - cy;

        auto const i = cx + cy * m_grid_width;
        auto const val11 = m_heightmap[i];
        auto const val21 = m_heightmap[i + 1];
        auto const val12 = m_heightmap[i + m_grid_width];
        auto const val22 = m_heightmap[i + m_grid_width + 1];
        auto const val1 = lerp_unclamped(val11, val21, fx);
        auto const val2 = lerp_unclamped(val12, val22, fx);
        auto const height = lerp_unclamped(val1, val2, fy);

        auto const gradient_x = lerp_unclamped(val21 - val11, val22 - val12, fy);
        auto const gradient_y = lerp_unclamped(val12 - val11, val22 - val21, fx);

        return vec<decimal_type, 3>(height, gradient_x, gradient_y);
    }
}  // namespace noise
