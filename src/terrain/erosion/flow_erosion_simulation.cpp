#include "terrain/erosion/flow_erosion_simulation.h"

#include <algorithm>
#include <cmath>
#include <iostream>

#include "math/math.h"
#include "noise/basis/fractal_noise.h"
#include "noise/basis/simplex_noise.h"

namespace noise
{
    flow_erosion_simulation::flow_erosion_simulation(grid2f const& heightmap, flow_erosion_simulation_params const& params) :
        m_grid_size(heightmap.size()),
        m_grid_width(heightmap.width()),
        m_grid_height(heightmap.height()),
        m_params(params),
        m_random_generator(m_params.seed),
        m_heightmap(m_grid_width, m_grid_height),
        m_steepness(m_grid_width, m_grid_height),
        m_sediment(m_grid_width, m_grid_height),
        m_tmp_buffer(m_grid_width, m_grid_height),
        m_water(m_grid_width, m_grid_height),
        m_l_flux(m_grid_width, m_grid_height),
        m_r_flux(m_grid_width, m_grid_height),
        m_b_flux(m_grid_width, m_grid_height),
        m_t_flux(m_grid_width, m_grid_height),
        m_velocity(m_grid_width, m_grid_height)
    {
        for (size_t i = 0; i < m_grid_size; ++i)
        {
            m_heightmap[i] = heightmap[i] * m_params.terrain_size.up();
        }
    }

    grid2f const& flow_erosion_simulation::heightmap() const
    {
        return m_heightmap;
    }

    grid2f const& flow_erosion_simulation::steepness() const
    {
        return m_steepness;
    }

    grid2f const& flow_erosion_simulation::water() const
    {
        return m_water;
    }

    grid2f const& flow_erosion_simulation::sediment() const
    {
        return m_sediment;
    }

    grid2f const& flow_erosion_simulation::left_flux() const
    {
        return m_l_flux;
    }

    grid2f const& flow_erosion_simulation::right_flux() const
    {
        return m_r_flux;
    }

    grid2f const& flow_erosion_simulation::bottom_flux() const
    {
        return m_b_flux;
    }

    grid2f const& flow_erosion_simulation::top_flux() const
    {
        return m_t_flux;
    }

    grid2<vec2f> const& flow_erosion_simulation::velocity() const
    {
        return m_velocity;
    }

    flow_erosion_simulation_params const& flow_erosion_simulation::params() const
    {
        return m_params;
    }

    void flow_erosion_simulation::update(unsigned int steps)
    {
        for (unsigned int i = 0; i < steps; ++i)
        {
            update_flux();
            update_water();
            update_material_slippage();
            update_erosion();
            update_sediment_transportation();
        }
    }

    inline void flow_erosion_simulation::update_flux()
    {
        auto const time_step = m_params.hydraulic_time_step;
        auto const area = m_params.virtual_pipe_area;
        auto const cell_width = m_params.terrain_size.right() / m_grid_width;
        auto const cell_height = m_params.terrain_size.forward() / m_grid_height;
        auto const flux_factor_x = time_step * area * m_params.gravity / cell_width;
        auto const flux_factor_y = time_step * area * m_params.gravity / cell_height;

#pragma omp parallel for
        for (size_t y = 0; y < m_grid_height; ++y)
        {
            for (size_t x = 0; x < m_grid_height; ++x)
            {
                auto const h = m_heightmap(x, y) + m_water(x, y);

                // Left outflow.
                if (x > 0)
                {
                    auto const dh = h - m_heightmap(x - 1, y) - m_water(x - 1, y);
                    auto const new_flux = m_l_flux(x, y) + flux_factor_x * dh;
                    m_l_flux(x, y) = std::max(decimal_type(0), new_flux);
                }

                // Right outflow.
                if (x < m_grid_width - 1)
                {
                    auto const dh = h - m_heightmap(x + 1, y) - m_water(x + 1, y);
                    auto const new_flux = m_r_flux(x, y) + flux_factor_x * dh;
                    m_r_flux(x, y) = std::max(decimal_type(0), new_flux);
                }

                // Bottom outflow.
                if (y > 0)
                {
                    auto const dh = h - m_heightmap(x, y - 1) - m_water(x, y - 1);
                    auto const new_flux = m_b_flux(x, y) + flux_factor_y * dh;
                    m_b_flux(x, y) = std::max(decimal_type(0), new_flux);
                }

                // Top outflow.
                if (y < m_grid_height - 1)
                {
                    auto const dh = h - m_heightmap(x, y + 1) - m_water(x, y + 1);
                    auto const new_flux = m_t_flux(x, y) + flux_factor_y * dh;
                    m_t_flux(x, y) = std::max(decimal_type(0), new_flux);
                }

                // If outflow is larger than the amount of water, scale outflow down.
                auto const out_flow = time_step * (m_l_flux(x, y) + m_r_flux(x, y) + m_b_flux(x, y) + m_t_flux(x, y));
                auto const water_amount = m_water(x, y) * cell_width * cell_height;
                if (out_flow > water_amount)
                {
                    auto const k = water_amount / out_flow;
                    m_l_flux(x, y) *= k;
                    m_r_flux(x, y) *= k;
                    m_b_flux(x, y) *= k;
                    m_t_flux(x, y) *= k;
                }
            }
        }
    }

    inline void flow_erosion_simulation::update_water()
    {
        auto const time_step = m_params.hydraulic_time_step;
        auto const cell_width = m_params.terrain_size.right() / m_grid_width;
        auto const cell_height = m_params.terrain_size.forward() / m_grid_height;
        auto const cell_size_inv = 1 / (cell_width * cell_height);
        auto const evaporation = std::max(decimal_type(0), 1 - m_params.evaporation * m_params.hydraulic_time_step);
        auto const rain_amount = m_params.rain * m_params.hydraulic_time_step;

#pragma omp parallel for
        for (size_t y = 0; y < m_grid_height; ++y)
        {
            for (size_t x = 0; x < m_grid_width; ++x)
            {
                // Calculate in and out flows.
                auto const l_flux = (x < m_grid_width - 1) ? m_l_flux(x + 1, y) : decimal_type(0);
                auto const r_flux = (x > 0) ? m_r_flux(x - 1, y) : decimal_type(0);
                auto const b_flux = (y < m_grid_height - 1) ? m_b_flux(x, y + 1) : decimal_type(0);
                auto const t_flux = (y > 0) ? m_t_flux(x, y - 1) : decimal_type(0);
                auto const in_flow = l_flux + r_flux + b_flux + t_flux;
                auto const out_flow = m_l_flux(x, y) + m_r_flux(x, y) + m_b_flux(x, y) + m_t_flux(x, y);

                // Calculate new water level.
                auto const dv = time_step * (in_flow - out_flow);
                auto const old_water = m_water(x, y);
                auto const new_water = std::max(decimal_type(0), old_water + dv * cell_size_inv);
                auto const mean_water = old_water + new_water;
                m_water(x, y) = new_water * evaporation + rain_amount;

                // Calculate velocity.
                if (mean_water < 0.0001)
                {
                    m_velocity(x, y) = vec2f();
                }
                else
                {
                    auto const u = (r_flux - m_l_flux(x, y) + m_r_flux(x, y) - l_flux) / (cell_height * mean_water);
                    auto const v = (t_flux - m_b_flux(x, y) + m_t_flux(x, y) - b_flux) / (cell_width * mean_water);
                    m_velocity(x, y) = vec2f(u, v);
                }
            }
        }

        // auto dist_x = std::uniform_int_distribution<int>(0, m_grid_width - 1);
        // auto dist_y = std::uniform_int_distribution<int>(0, m_grid_height - 1);
        // auto const radius = 3;

        // for (size_t i = 0; i < m_grid_width; ++i)
        // {
        //     auto const cx = dist_x(m_random_generator);
        //     auto const cy = dist_y(m_random_generator);

        //     for (auto y = -radius; y <= radius; ++y)
        //     {
        //         for (auto x = -radius; x <= radius; ++x)
        //         {
        //             auto const px = cx + x;
        //             if (px < 0 || px >= static_cast<int>(m_grid_width))
        //             {
        //                 continue;
        //             }

        //             auto const py = cy + y;
        //             if (py < 0 || py >= static_cast<int>(m_grid_height))
        //             {
        //                 continue;
        //             }

        //             if (x * x + y * y < radius * radius)
        //             {
        //                 {
        //                     m_water(px, py) += rain_amount;
        //                 }
        //             }
        //         }
        //     }
        // }
    }

    inline void flow_erosion_simulation::update_material_slippage()
    {
        auto const time_step = m_params.thermal_time_step;
        auto const cell_width = m_params.terrain_size.right() / m_grid_width;
        auto const cell_height = m_params.terrain_size.forward() / m_grid_height;
        auto const cell_width_inv = 1 / cell_width;
        auto const cell_height_inv = 1 / cell_height;
        auto const cell_diagonal_inv = 1 / std::sqrt(cell_width * cell_width + cell_height * cell_height);
        auto const max_dx = cell_width * std::tan(m_params.talus_angle);
        auto const max_dy = cell_height * std::tan(m_params.talus_angle);

#pragma omp parallel for
        for (size_t y = 0; y < m_grid_height; ++y)
        {
            auto const y_low = (y == 0) ? y : y - 1;
            auto const y_high = (y == m_grid_height - 1) ? y : y + 1;

            for (size_t x = 0; x < m_grid_width; ++x)
            {
                auto const x_low = (x == 0) ? x : x - 1;
                auto const x_high = (x == m_grid_width - 1) ? x : x + 1;

                auto const height = m_heightmap(x, y);
                auto const dl = height - m_heightmap(x_low, y);
                auto const dr = height - m_heightmap(x_high, y);
                auto const db = height - m_heightmap(x, y_low);
                auto const dt = height - m_heightmap(x, y_high);
                auto const dlb = height - m_heightmap(x_low, y_low);
                auto const drb = height - m_heightmap(x_high, y_low);
                auto const dlt = height - m_heightmap(x_low, y_high);
                auto const drt = height - m_heightmap(x_high, y_high);

                auto tan = decimal_type(0);
                if (dl > 0)
                {
                    tan += dl * cell_width_inv;
                }
                if (dr > 0)
                {
                    tan += dr * cell_width_inv;
                }
                if (db > 0)
                {
                    tan += db * cell_height_inv;
                }
                if (dt > 0)
                {
                    tan += dt * cell_height_inv;
                }
                auto const angle = std::atan(decimal_type(0.25) * tan);

                m_sediment(x, y) = m_tmp_buffer(x, y);
                m_steepness(x, y) = std::max(std::sin(angle), decimal_type(0.1));

                // Make sure no spikes on the terrain.
                if (dl >= 0 && dr >= 0 && db >= 0 && dt >= 0)
                {
                    m_tmp_buffer(x, y) = 0.2 * (height + m_heightmap(x_low, y) + m_heightmap(x_high, y) + m_heightmap(x, y_low) + m_heightmap(x, y_high));
                }
                else
                {
                    decimal_type dh = 0;
                    dh += get_delta_height(dl, max_dx);
                    dh += get_delta_height(dr, max_dx);
                    dh += get_delta_height(db, max_dy);
                    dh += get_delta_height(dt, max_dy);
                    dh += get_delta_height(dlb, max_dx);
                    dh += get_delta_height(drb, max_dx);
                    dh += get_delta_height(dlt, max_dy);
                    dh += get_delta_height(drt, max_dy);
                    m_tmp_buffer(x, y) = height - time_step * dh;
                }

                // auto const dx = cell_width_inv * (m_heightmap(x_low, y) - m_heightmap(x_high, y));
                // auto const dy = cell_height_inv * (m_heightmap(x, y_low) - m_heightmap(x, y_high));
                // auto const normal_up = 2 / std::sqrt(4 + dx * dx + dy * dy);
                // auto const angle = std::acos(normal_up);

                // m_sediment(x, y) = m_tmp_buffer(x, y);
                // m_steepness(x, y) = std::max(std::sin(angle), decimal_type(0.1));
                // if ((std::abs(dl) > max_dx || std::abs(dr) > max_dx) && dl * dr > 0)
                // {
                //     m_tmp_buffer(x, y) = 0.2 * (height + hl + hr + hb + ht);
                // }
                // else if ((std::abs(db) > max_dy || std::abs(dt) > max_dy) && db * dt > 0)
                // {
                //     m_tmp_buffer(x, y) = 0.2 * (height + hl + hr + hb + ht);
                // }
                // else
                // {
                //     m_tmp_buffer(x, y) = height;
                // }
            }
        }
    }

    inline void flow_erosion_simulation::update_erosion()
    {
        auto const k_capacity = m_params.capacity;
        auto const k_dissolve = m_params.hydraulic_time_step * m_params.dissolve;
        auto const k_deposition = m_params.hydraulic_time_step * m_params.deposition;

#pragma omp parallel for
        for (size_t i = 0; i < m_grid_size; ++i)
        {
            // Calculate capacity the water can carry.
            auto const angle = m_steepness[i];
            auto const speed = m_velocity[i].magnitude();
            auto const capacity = k_capacity * angle * speed;

            auto const height = m_tmp_buffer[i];
            auto const sediment = m_sediment[i];
            auto const water = m_water[i];

            // Dissolve
            if (capacity > sediment)
            {
                auto const sediment_change = std::min(height, k_dissolve * (capacity - sediment));
                m_heightmap[i] = height - sediment_change;
                m_sediment[i] = sediment + sediment_change;
                m_water[i] = water + sediment_change;
            }
            // or deposit...
            else
            {
                auto const sediment_change = std::min(water, std::min(sediment, k_deposition * (sediment - capacity)));
                m_heightmap[i] = height + sediment_change;
                m_sediment[i] = sediment - sediment_change;
                m_water[i] = water - sediment_change;
            }
        }
    }

    inline void flow_erosion_simulation::update_sediment_transportation()
    {
        auto const time_step = m_params.hydraulic_time_step;

#pragma omp parallel for
        for (size_t y = 0; y < m_grid_height; ++y)
        {
            for (size_t x = 0; x < m_grid_width; ++x)
            {
                auto const velocity = m_velocity(x, y);
                auto const from_pos_x = std::clamp(x - velocity.x * time_step, decimal_type(0), decimal_type(m_grid_width - 1));
                auto const from_pos_y = std::clamp(y - velocity.y * time_step, decimal_type(0), decimal_type(m_grid_height - 1));

                // Interpolate.
                auto const x0 = static_cast<int>(from_pos_x);
                auto const y0 = static_cast<int>(from_pos_y);
                auto const x1 = (x0 == static_cast<int>(m_grid_width) - 1) ? x0 : x0 + 1;
                auto const y1 = (y0 == static_cast<int>(m_grid_height) - 1) ? y0 : y0 + 1;
                auto const fx = from_pos_x - x0;
                auto const fy = from_pos_y - y0;
                m_tmp_buffer(x, y) = lerp_unclamped(lerp_unclamped(m_sediment(x0, y0), m_sediment(x1, y0), fx),
                                                    lerp_unclamped(m_sediment(x0, y1), m_sediment(x1, y1), fx),
                                                    fy);
            }
        }
    }

    inline decimal_type flow_erosion_simulation::get_delta_height(decimal_type delta, decimal_type max_delta)
    {
        if (delta < -max_delta)
        {
            return delta + max_delta;
        }
        else if (delta > max_delta)
        {
            return delta - max_delta;
        }
        return 0;
    }
}  // namespace noise
