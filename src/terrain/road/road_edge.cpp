#include "terrain/road/road_edge.h"

#include <cmath>

namespace noise
{
    road_edge::road_edge() :
        m_length(0),
        m_id1(0),
        m_id2(0),
        m_radius(0)
    {
    }

    road_edge::road_edge(road_vertex const& v1, road_vertex const& v2, vertex_t id1, vertex_t id2) :
        m_length(std::sqrt((v1.x() - v2.x()) * (v1.x() - v2.x()) + (v1.y() - v2.y()) * (v1.y() - v2.y()))),
        m_id1(id1),
        m_id2(id2),
        m_radius(0)
    {
    }

    size_t road_edge::id1() const
    {
        return m_id1;
    }

    size_t road_edge::id2() const
    {
        return m_id2;
    }

    decimal_type road_edge::length() const
    {
        return m_length;
    }

    decimal_type road_edge::radius() const
    {
        return m_radius;
    }
}  // namespace noise
