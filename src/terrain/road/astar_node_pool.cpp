#include "terrain/road/astar_node_pool.h"

namespace noise
{
    void astar_node_pool::clear()
    {
        m_nodes.clear();
        m_handles.clear();
    }

    int astar_node_pool::create_node(int index)
    {
        auto const handle_it = m_handles.find(index);
        if (handle_it == m_handles.end())
        {
            m_nodes.push_back(astar_node());
            m_handles[index] = m_nodes.size() - 1;
            return m_handles[index];
        }
        return handle_it->second;
    }

    astar_node& astar_node_pool::get_node(int handle)
    {
        return m_nodes[handle];
    }
}  // namespace noise
