#include "terrain/road/road_renderer.h"

#include <cmath>

#include "math/shape/catmull_rom_spline2.h"

namespace noise
{
    road_renderer::road_renderer(size_t width, size_t height) :
        m_renderer(width, height)
    {
    }

    grid2f& road_renderer::result()
    {
        return m_renderer.heightmap();
    }

    grid2f const& road_renderer::result() const
    {
        return m_renderer.heightmap();
    }

    void road_renderer::draw(std::vector<std::vector<vec2f>> const& paths)
    {
        result().clear();

        for (auto const& points : paths)
        {
            // Can't draw if too few points.
            if (points.size() < 2)
            {
                continue;
            }

            int n = points.size();
            auto const start = 2 * points[0] - points[1];
            auto const end = 2 * points[n - 1] - points[n - 2];

            if (points.size() == 2)
            {
                // Draw splines with 2 waypoints.
                draw_catmull_rom_spline(start, points[0], points[1], end);
            }
            else
            {
                // Draw splines with more than 2 waypoints.
                draw_catmull_rom_spline(start, points[0], points[1], points[2]);
                for (auto i = 0; i < n - 3; ++i)
                {
                    draw_catmull_rom_spline(points[i], points[i + 1], points[i + 2], points[i + 3]);
                }
                draw_catmull_rom_spline(points[n - 3], points[n - 2], points[n - 1], end);
            }
        }
    }

    void road_renderer::draw_catmull_rom_spline(vec2f const& p0, vec2f const& p1, vec2f const& p2, vec2f const& p3)
    {
        auto const spline = catmull_rom_spline2(p0, p1, p2, p3);
        m_renderer.draw_spline(spline);
    }
}  // namespace noise
