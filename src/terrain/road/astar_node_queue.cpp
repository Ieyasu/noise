#include "terrain/road/astar_node_queue.h"

namespace noise
{
    astar_node_queue::astar_node_queue(astar_node_pool* pool) :
        m_pool(pool)
    {
    }

    bool astar_node_queue::empty() const
    {
        return m_heap.empty();
    }

    size_t astar_node_queue::size() const
    {
        return m_heap.size();
    }

    int astar_node_queue::top() const
    {
        return m_heap.front();
    }

    void astar_node_queue::clear()
    {
        m_heap.clear();
    }

    void astar_node_queue::pop()
    {
        trickle_down(m_heap[size() - 1]);
        m_heap.pop_back();
    }

    void astar_node_queue::push(int handle)
    {
        m_heap.push_back(handle);
        bubble_up(size() - 1, handle);
    }

    void astar_node_queue::modify(int handle)
    {
        auto i = m_indices.at(handle);
        bubble_up(i, handle);
    }

    decimal_type astar_node_queue::get_cost(int handle) const
    {
        return m_pool->get_node(handle).cost_heuristic;
    }

    void astar_node_queue::bubble_up(int i, int handle)
    {
        auto parent = (i - 1) / 2;
        while (i > 0 && get_cost(m_heap[parent]) > get_cost(handle))
        {
            m_heap[i] = m_heap[parent];
            m_indices[m_heap[i]] = i;
            i = parent;
            parent = (i - 1) / 2;
        }
        m_heap[i] = handle;
        m_indices[m_heap[i]] = i;
    }

    void astar_node_queue::trickle_down(int handle)
    {
        size_t i = 0;
        auto child = (i * 2) + 1;
        while (child < size() - 1)
        {
            if (child + 1 < size() - 1 && get_cost(m_heap[child]) > get_cost(m_heap[child + 1]))
            {
                child++;
            }
            m_heap[i] = m_heap[child];
            m_indices[m_heap[i]] = i;
            i = child;
            child = (i * 2) + 1;
        }
        bubble_up(i, handle);
    }
}  // namespace noise
