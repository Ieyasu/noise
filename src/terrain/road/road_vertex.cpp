#include "terrain/road/road_vertex.h"

namespace noise
{
    road_vertex::road_vertex() :
        road_vertex(0, 0)
    {
    }

    road_vertex::road_vertex(decimal_type x, decimal_type y) :
        m_x(x),
        m_y(y)
    {
    }

    decimal_type road_vertex::x() const
    {
        return m_x;
    }

    decimal_type road_vertex::y() const
    {
        return m_y;
    }
}  // namespace noise
