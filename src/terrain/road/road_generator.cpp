#include "terrain/road/road_generator.h"

#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <limits>

#include "math/triangulation/delaunator.h"
#include "terrain/road/astar_node_pool.h"
#include "terrain/road/astar_node_queue.h"

namespace noise
{
    // Generates a road graph by creating an Euclidean Minimum Spanning Tree, which is done as follows:
    //  1. Create a delaunay triangulation of the point cloud.
    //  2. Run a regular MST on the resulting graph.
    // The complexity is O(N log N)
    std::vector<road_edge> road_generator::generate_road_graph(std::vector<road_vertex> const& vertices) const
    {
        // Add vertices to graph.
        auto graph = road_graph();
        for (auto const& vertex : vertices)
        {
            boost::add_vertex(vertex, graph);
        }

        // Calculate delaunay triangulation.
        auto const triangulation = delaunator(vertices);
        auto const verts = boost::vertices(graph);
        for (size_t i = 0; i < triangulation.triangles.size(); i += 3)
        {
            for (size_t j = 0; j < 3; ++j)
            {
                auto const v1 = triangulation.triangles[i + j];
                auto const v2 = triangulation.triangles[i + (j + 1) % 3];
                if (v1 < v2 || triangulation.halfedges[v1] == INVALID_INDEX)
                {
                    auto const id1 = verts.first[v1];
                    auto const id2 = verts.first[v2];
                    auto const edge = road_edge(graph[id1], graph[id2], id1, id2);
                    boost::add_edge(id1, id2, edge, graph);
                }
            }
        }

        // Calculate MST from the delaunay triangulation.
        auto mst = std::vector<edge_t>();
        boost::kruskal_minimum_spanning_tree(
                graph,
                std::back_inserter(mst),
                boost::weight_map(boost::get(&road_edge::m_length, graph)));

        // Save MST edges to a list.
        auto edges = std::vector<road_edge>(mst.size());
        for (size_t i = 0; i < mst.size(); ++i)
        {
            edges[i] = graph[mst[i]];
        }

        return edges;
    }

    // Generates road paths by running A* on a heightmap.
    std::vector<std::vector<vec2f>> road_generator::generate_road_paths(std::vector<road_vertex> const& vertices, std::vector<road_edge> const& edges, grid2f const& heightmap) const
    {
        auto const mask = get_mask(5);
        auto paths = std::vector<std::vector<vec2f>>();
        auto const downscale = 1;
        auto const downscale_multiplier = 1 << downscale;
        auto const downscaled_width = heightmap.width() >> downscale;
        auto const downscaled_height = heightmap.height() >> downscale;
        auto const heightmap_r = heightmap.scale(downscaled_width, downscaled_height, interpolation_mode::nearest_neighbour);

#pragma omp parallel
        {
            auto nodes = astar_node_pool();
            auto open_set = astar_node_queue(&nodes);
            auto points = std::vector<vec2f>();

#pragma omp for schedule(dynamic)
            for (size_t i = 0; i < edges.size(); ++i)
            {
                nodes.clear();
                open_set.clear();
                points.clear();

                // Get start and goal posinates.
                auto const& start_vertex = vertices[edges[i].id1()];
                auto const start_x = static_cast<int>(start_vertex.x() * (heightmap_r.width() - 1));
                auto const start_y = static_cast<int>(start_vertex.y() * (heightmap_r.height() - 1));
                auto const start_idx = start_x + start_y * heightmap_r.width();
                auto const start_pos = vec2i(start_x, start_y);

                auto const& goal_vertex = vertices[edges[i].id2()];
                auto const goal_x = static_cast<int>(goal_vertex.x() * (heightmap_r.width() - 1));
                auto const goal_y = static_cast<int>(goal_vertex.y() * (heightmap_r.height() - 1));
                auto const goal_idx = goal_x + goal_y * heightmap_r.width();
                auto const goal_pos = vec2i(goal_x, goal_y);

                // Initialize state.
                auto const start_handle = nodes.create_node(start_idx);
                auto& start_node = nodes.get_node(start_handle);
                start_node.position = start_pos;
                start_node.cost = 0;
                start_node.cost_heuristic = cost_heuristic(start_pos, goal_pos, heightmap_r);
                start_node.state = astar_node_state::open;
                open_set.push(start_handle);

                auto const goal_handle = nodes.create_node(goal_idx);
                auto& goal_node = nodes.get_node(goal_handle);
                goal_node.position = goal_pos;

                while (!open_set.empty())
                {
                    // Check if goal is reached.
                    auto curr_handle = open_set.top();
                    if (curr_handle == goal_handle)
                    {
                        while (curr_handle != -1)
                        {
                            auto const& node = nodes.get_node(curr_handle);
                            auto const pos = downscale_multiplier * vec2f(node.position.x, node.position.y);
                            points.push_back(pos);
                            curr_handle = node.parent_handle;
                        }

// Draw points.
#pragma omp critical
                        {
                            paths.push_back(points);
                        }

                        break;
                    }

                    // Mark node as traversed.
                    nodes.get_node(curr_handle).state = astar_node_state::closed;
                    open_set.pop();

                    // Process neighbours.
                    for (auto const& delta : mask)
                    {
                        // Make sure neighbour is in bounds.
                        auto const neighbour_pos = nodes.get_node(curr_handle).position + delta;
                        if (neighbour_pos.x < 0
                            || neighbour_pos.x >= static_cast<int>(heightmap_r.width())
                            || neighbour_pos.y < 0
                            || neighbour_pos.y >= static_cast<int>(heightmap_r.height()))
                        {
                            continue;
                        }

                        // Get the neighbour node.
                        auto const neighbour_idx = neighbour_pos.x + neighbour_pos.y * heightmap_r.width();
                        auto const neighbour_handle = nodes.create_node(neighbour_idx);
                        auto& neighbour_node = nodes.get_node(neighbour_handle);

                        // Don't process already processed nodes.
                        if (neighbour_node.state == astar_node_state::closed)
                        {
                            continue;
                        }

                        // Get cost from of traversing to the neighbour.
                        auto const& curr_node = nodes.get_node(curr_handle);
                        auto const& parent_pos = curr_node.parent_handle == -1
                                                         ? curr_node.position
                                                         : nodes.get_node(curr_node.parent_handle).position;
                        auto const cost = curr_node.cost + this->cost(parent_pos, curr_node.position, neighbour_pos, heightmap_r);
                        if (cost >= neighbour_node.cost)
                        {
                            continue;
                        }

                        // Update neighbour cost if needed.
                        neighbour_node.parent_handle = curr_handle;
                        neighbour_node.cost = cost;
                        neighbour_node.cost_heuristic = cost + cost_heuristic(neighbour_pos, goal_pos, heightmap_r);
                        if (neighbour_node.state != astar_node_state::open)
                        {
                            neighbour_node.state = astar_node_state::open;
                            neighbour_node.position = neighbour_pos;
                            open_set.push(neighbour_handle);
                        }
                        else
                        {
                            open_set.modify(neighbour_handle);
                        }
                    }
                }
            }
        }

        return paths;
    }

    decimal_type road_generator::cost_heuristic(vec2i const& c1, vec2i const& c2, grid2f const& heightmap) const
    {
        auto const dx = c2.x - c1.x;
        auto const dy = c2.y - c1.y;
        auto const distance = std::abs(dx) + std::abs(dy);
        //auto const distance = std::sqrt(dx * dx + dy * dy);

        auto const height1 = heightmap(c1.x, c1.y) * 100;
        auto const height2 = heightmap(c2.x, c2.y) * 100;
        auto const height_delta = std::abs(height1 - height2);
        auto const slope = std::atan(height_delta / distance);
        auto const t = std::min(1.0, slope / (0.25 * PI));
        auto const slope_cost = (1 - t) + t * 10.0f;

        return distance * slope_cost;
    }

    decimal_type road_generator::cost(vec2i const& c1, vec2i const& c2, vec2i const& c3, grid2f const& heightmap) const
    {
        auto const dx = c3.x - c2.x;
        auto const dy = c3.y - c2.y;
        auto const distance = std::sqrt(dx * dx + dy * dy);

        auto const height1 = heightmap(c2.x, c2.y) * 100;
        auto const height2 = heightmap(c3.x, c3.y) * 100;
        auto const height_delta = std::abs(height1 - height2);
        auto const water_cost = cost_water(height2);

        auto const slope = std::atan(height_delta / distance);
        auto const slope_cost = cost_slope(slope);

        auto const dx2 = c2.x - c1.x;
        auto const dy2 = c2.y - c1.y;
        auto const distance2 = std::sqrt(dx2 * dx2 + dy2 * dy2);
        auto const curvature = c1 == c2 ? 0 : std::acos((dx * dx2 + dy * dy2) / (distance * distance2));
        auto const curvature_cost = cost_curvature(curvature);

        return distance * slope_cost * water_cost * curvature_cost;
    }

    decimal_type road_generator::cost_slope(decimal_type slope) const
    {
        assert(slope >= 0);

        auto const MAX_SLOPE = decimal_type(0.25 * PI);  // 45 degrees.
        auto const MAX_SLOPE_COST = decimal_type(10);

        if (slope > MAX_SLOPE)
        {
            return std::numeric_limits<decimal_type>::max();
        }

        auto const t = slope / MAX_SLOPE;
        return (1 - t) + t * MAX_SLOPE_COST;
    }

    inline decimal_type road_generator::cost_water(decimal_type height) const
    {
        return 1;
        auto const WATER_LEVEL = decimal_type(0);
        return height < WATER_LEVEL ? std::numeric_limits<decimal_type>::max() : 1;
    }

    inline decimal_type road_generator::cost_curvature(decimal_type curvature) const
    {
        return 1;
        auto const MAX_CURVATURE = decimal_type(0.50 * PI);  // 90 degrees.
        auto const MAX_CURVATURE_COST = decimal_type(10);

        if (curvature > MAX_CURVATURE)
        {
            return std::numeric_limits<decimal_type>::max();
        }

        auto const t = curvature / MAX_CURVATURE;
        return (1 - t) + t * MAX_CURVATURE_COST;
    }

    inline int road_generator::gcd(int a, int b) const
    {
        a = std::abs(a);
        b = std::abs(b);
        return b == 0 ? a : gcd(b, a % b);
    }

    inline std::vector<vec2i> road_generator::get_mask(int k) const
    {
        assert(k > 0);

        auto mask = std::vector<vec2i>();
        for (auto y = -k; y <= k; ++y)
        {
            for (auto x = -k; x <= k; ++x)
            {
                if ((x != 0 || y != 0) && gcd(x, y) == 1)
                {
                    mask.push_back(vec2i(x, y));
                }
            }
        }
        return mask;
    }
}  // namespace noise
