#include "terrain/heightmap/heightmap_renderer.h"

#include <algorithm>
#include <cmath>

namespace noise
{
    heightmap_renderer::heightmap_renderer(size_t width, size_t height) :
        m_heightmap(width, height),
        m_color(1),
        m_opacity(1),
        m_thickness(1),
        m_blend_mode(heightmap_renderer_blend_mode::OVERWRITE)
    {
    }

    grid2f& heightmap_renderer::heightmap()
    {
        return m_heightmap;
    }

    grid2f const& heightmap_renderer::heightmap() const
    {
        return m_heightmap;
    }

    heightmap_renderer::value_type heightmap_renderer::color() const
    {
        return m_color;
    }

    heightmap_renderer::value_type heightmap_renderer::opacity() const
    {
        return m_opacity;
    }

    heightmap_renderer::value_type heightmap_renderer::thickness() const
    {
        return m_thickness;
    }

    heightmap_renderer_blend_mode heightmap_renderer::blend_mode() const
    {
        return m_blend_mode;
    }

    void heightmap_renderer::set_color(value_type color)
    {
        m_color = color;
    }

    void heightmap_renderer::set_opacity(value_type opacity)
    {
        m_opacity = std::clamp(opacity, value_type(0), value_type(1));
    }

    void heightmap_renderer::set_thickness(value_type thickness)
    {
        m_thickness = std::max(thickness, value_type(0));
    }

    void heightmap_renderer::set_blend_mode(heightmap_renderer_blend_mode blend_mode)
    {
        m_blend_mode = blend_mode;
    }

    void heightmap_renderer::clear()
    {
        m_heightmap.clear();
    }

    void heightmap_renderer::fill()
    {
#pragma omp parallel for
        for (size_t i = 0; i < m_heightmap.size(); ++i)
        {
            m_heightmap.operator[](i) = lerp_unclamped(m_heightmap.operator[](i), color(), opacity());
        }
    }

    void heightmap_renderer::draw_spline(spline2 const& spline)
    {
        auto const step = value_type(0.25);
        draw_point(spline.interpolate(0));

        auto t = 0;
        do
        {
            t = std::min(1.0f, t + step);
            draw_point(spline.interpolate(t));
        } while (t < 1);
    }

    void heightmap_renderer::draw_line_segment(vec2f const& start, vec2f const& end)
    {
        draw_point(start);
        draw_point(end);
    }

    void heightmap_renderer::draw_line_segment(line_segment2 const& segment)
    {
        draw_line_segment(segment.start, segment.end);
    }

    void heightmap_renderer::draw_point(vec2f const& origin)
    {
        auto const r = static_cast<int>(std::ceil(thickness()));
        auto const r_squared = thickness() * thickness();

        for (auto j = -r; j <= r; ++j)
        {
            for (auto i = -r; i <= r; ++i)
            {
                if (i * i + j * j <= r_squared)
                {
                    auto x = static_cast<int>(origin.x + i);
                    auto y = static_cast<int>(origin.y + j);
                    set_pixel(x, y, color());
                }
            }
        }
    }

    void heightmap_renderer::draw_noise(noise_function const& noise, bounds_type const& bounds)
    {
        auto const x_frac = bounds.width / value_type(m_heightmap.width() - 1);
        auto const y_frac = bounds.height / value_type(m_heightmap.height() - 1);

#pragma omp parallel for
        for (size_t y = 0; y < m_heightmap.height(); ++y)
        {
            auto const dy = y * y_frac + bounds.y;
            for (size_t x = 0; x < m_heightmap.width(); ++x)
            {
                auto const dx = x * x_frac + bounds.x;
                auto const value = noise.evaluate(dx, dy);
                set_pixel(x, y, value);
            }
        }
    }

    void heightmap_renderer::draw_noise(noise_function const& noise,
                                        bounds_type const& bounds,
                                        grid2f const& turbulence_x,
                                        grid2f const& turbulence_y,
                                        float turbulence_strength)
    {
        auto const x_frac = 1.0f / value_type(m_heightmap.width() - 1);
        auto const y_frac = 1.0f / value_type(m_heightmap.height() - 1);

#pragma omp parallel for
        for (size_t y = 0; y < m_heightmap.height(); ++y)
        {
            auto const ypos = y * y_frac;
            auto const dy = bounds.width * ypos + bounds.y;
            for (size_t x = 0; x < m_heightmap.width(); ++x)
            {
                auto const xpos = x * x_frac;
                auto const dx = bounds.height * xpos + bounds.x;
                auto const tx = turbulence_x.get_interpolated_bilinear(xpos, ypos);
                auto const ty = turbulence_y.get_interpolated_bilinear(xpos, ypos);
                auto const value = noise.evaluate(dx + tx * turbulence_strength, dy + ty * turbulence_strength);
                set_pixel(x, y, value);
            }
        }
    }

    void heightmap_renderer::draw_heightmap(grid2f const& heightmap)
    {
        if (heightmap.width() != m_heightmap.width() || heightmap.height() != m_heightmap.height())
        {
            throw std::length_error("Heightmap sizes don't match.");
        }

#pragma omp parallel for
        for (size_t y = 0; y < heightmap.height(); ++y)
        {
            for (size_t x = 0; x < heightmap.width(); ++x)
            {
                set_pixel(x, y, heightmap(x, y));
            }
        }
    }

    void heightmap_renderer::draw_steepness_map(grid2f const& heightmap, vec3f const& map_size)
    {
        if (heightmap.width() != m_heightmap.width() || heightmap.height() != m_heightmap.height())
        {
            throw std::length_error("Heightmap sizes don't match.");
        }

        auto const lx = map_size.up() * (heightmap.width() / map_size.right());
        auto const ly = map_size.up() * (heightmap.height() / map_size.forward());

#pragma omp parallel for
        for (size_t y = 0; y < heightmap.height(); ++y)
        {
            auto const y_low = (y == 0) ? 0 : y - 1;
            auto const y_high = (y == heightmap.height() - 1) ? heightmap.height() - 1 : y + 1;
            for (size_t x = 0; x < heightmap.width(); ++x)
            {
                auto const x_low = (x == 0) ? 0 : x - 1;
                auto const x_high = (x == heightmap.width() - 1) ? heightmap.width() - 1 : x + 1;

                auto const dx = (heightmap(x_low, y) - heightmap(x_high, y)) * lx;
                auto const dy = (heightmap(x, y_low) - heightmap(x, y_high)) * ly;

                auto const normal_up = 2 / std::sqrt(4 + dx * dx + dy * dy);
                auto const angle = std::acos(normal_up);

                set_pixel(x, y, std::sin(angle));
            }
        }
    }

    void heightmap_renderer::set_pixel(int x, int y, value_type color)
    {
        auto const cx = std::clamp(x, 0, static_cast<int>(m_heightmap.width()) - 1);
        auto const cy = std::clamp(y, 0, static_cast<int>(m_heightmap.height()) - 1);

        switch (m_blend_mode)
        {
            case heightmap_renderer_blend_mode::ADD:
                m_heightmap.operator()(cx, cy) += color * opacity();
                break;
            case heightmap_renderer_blend_mode::SUBTRACT:
                m_heightmap.operator()(cx, cy) -= color * opacity();
                break;
            case heightmap_renderer_blend_mode::OVERWRITE:
                m_heightmap.operator()(cx, cy) = color * opacity();
                break;
            case heightmap_renderer_blend_mode::LERP:
                m_heightmap.operator()(cx, cy) = lerp_unclamped(m_heightmap.operator()(cx, cy), color, opacity());
                break;
            case heightmap_renderer_blend_mode::MULTIPLY:
                m_heightmap.operator()(cx, cy) *= color * opacity();
                break;
            default:
                break;
        }
    }
}  // namespace noise
