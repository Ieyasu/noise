#include "noise/sampling/poisson_sampler_grid.h"

namespace noise
{
    poisson_sampler_grid::poisson_sampler_grid(value_type distance) :
        m_distance_sqr(distance * distance)
    {
        auto const size = static_cast<size_t>(std::floor(std::sqrt(2.0) / distance));
        m_grid = grid_type(size, size, invalid_point);
    }

    void poisson_sampler_grid::clear()
    {
        m_grid.fill(invalid_point);
    }

    int poisson_sampler_grid::insert(point_type const& point)
    {
        auto const x = static_cast<int>(point.x * m_grid.width());
        auto const y = static_cast<int>(point.y * m_grid.width());
        auto const index = x + y * m_grid.width();
        m_grid[index] = point;
        return index;
    }

    int poisson_sampler_grid::try_insert(point_type const& point, int x, int y)
    {
        auto const index = static_cast<int>(x + y * m_grid.width());
        if (m_grid[index].x != -1)
        {
            return -1;
        }

        auto const x0 = get_coord(x - 2);
        auto const x1 = get_coord(x - 1);
        auto const x2 = get_coord(x);
        auto const x3 = get_coord(x + 1);
        auto const x4 = get_coord(x + 2);

        auto const y0 = get_coord(y - 2);
        auto const y1 = get_coord(y - 1);
        auto const y2 = get_coord(y);
        auto const y3 = get_coord(y + 1);
        auto const y4 = get_coord(y + 2);

        if (intersects(point, x1, y0))
            return -1;
        if (intersects(point, x2, y0))
            return -1;
        if (intersects(point, x3, y0))
            return -1;

        if (intersects(point, x0, y1))
            return -1;
        if (intersects(point, x1, y1))
            return -1;
        if (intersects(point, x2, y1))
            return -1;
        if (intersects(point, x3, y1))
            return -1;
        if (intersects(point, x4, y1))
            return -1;

        if (intersects(point, x0, y2))
            return -1;
        if (intersects(point, x1, y2))
            return -1;
        if (intersects(point, x3, y2))
            return -1;
        if (intersects(point, x4, y2))
            return -1;

        if (intersects(point, x0, y3))
            return -1;
        if (intersects(point, x1, y3))
            return -1;
        if (intersects(point, x2, y3))
            return -1;
        if (intersects(point, x3, y3))
            return -1;
        if (intersects(point, x4, y3))
            return -1;

        if (intersects(point, x1, y4))
            return -1;
        if (intersects(point, x2, y4))
            return -1;
        if (intersects(point, x3, y4))
            return -1;

        m_grid[index] = point;
        return index;
    }

    int poisson_sampler_grid::try_insert_wrapped(point_type const& point, int x, int y)
    {
        auto const index = static_cast<int>(x + y * m_grid.width());
        if (m_grid[index].x != -1)
        {
            return -1;
        }

        auto const x0 = get_coord_wrapped(x - 2);
        auto const x1 = get_coord_wrapped(x - 1);
        auto const x2 = get_coord_wrapped(x);
        auto const x3 = get_coord_wrapped(x + 1);
        auto const x4 = get_coord_wrapped(x + 2);

        auto const y0 = get_coord_wrapped(y - 2);
        auto const y1 = get_coord_wrapped(y - 1);
        auto const y2 = get_coord_wrapped(y);
        auto const y3 = get_coord_wrapped(y + 1);
        auto const y4 = get_coord_wrapped(y + 2);

        if (intersects_wrapped(point, x1, y0))
            return -1;
        if (intersects_wrapped(point, x2, y0))
            return -1;
        if (intersects_wrapped(point, x3, y0))
            return -1;

        if (intersects_wrapped(point, x0, y1))
            return -1;
        if (intersects_wrapped(point, x1, y1))
            return -1;
        if (intersects_wrapped(point, x2, y1))
            return -1;
        if (intersects_wrapped(point, x3, y1))
            return -1;
        if (intersects_wrapped(point, x4, y1))
            return -1;

        if (intersects_wrapped(point, x0, y2))
            return -1;
        if (intersects_wrapped(point, x1, y2))
            return -1;
        if (intersects_wrapped(point, x3, y2))
            return -1;
        if (intersects_wrapped(point, x4, y2))
            return -1;

        if (intersects_wrapped(point, x0, y3))
            return -1;
        if (intersects_wrapped(point, x1, y3))
            return -1;
        if (intersects_wrapped(point, x2, y3))
            return -1;
        if (intersects_wrapped(point, x3, y3))
            return -1;
        if (intersects_wrapped(point, x4, y3))
            return -1;

        if (intersects_wrapped(point, x1, y4))
            return -1;
        if (intersects_wrapped(point, x2, y4))
            return -1;
        if (intersects_wrapped(point, x3, y4))
            return -1;

        m_grid[index] = point;
        return index;
    }

    inline int poisson_sampler_grid::get_coord(int i)
    {
        if (i < 0)
        {
            i = 0;
        }
        else if (i >= static_cast<int>(m_grid.width()))
        {
            i = m_grid.width() - 1;
        }
        return i;
    }

    inline int poisson_sampler_grid::get_coord_wrapped(int i)
    {
        if (i < 0)
        {
            i += m_grid.width();
        }
        else if (i >= static_cast<int>(m_grid.width()))
        {
            i -= m_grid.width();
        }
        return i;
    }

    inline bool poisson_sampler_grid::intersects(vec2f const& point, int x, int y)
    {
        auto const& p = m_grid[x + y * m_grid.width()];
        if (p.x != invalid_point.x)
        {
            auto const d = point - p;
            if (d.sqr_magnitude() < m_distance_sqr)
            {
                return true;
            }
        }
        return false;
    }

    inline bool poisson_sampler_grid::intersects_wrapped(vec2f const& point, int x, int y)
    {
        auto const& p = m_grid[x + y * m_grid.width()];
        if (p.x != invalid_point.x)
        {
            auto d = point - p;
            if (d.x > value_type(0.5))
            {
                d.x = 1 - d.x;
            }
            else if (d.x < value_type(-0.5))
            {
                d.x = 1 + d.x;
            }

            if (d.y > value_type(0.5))
            {
                d.y = 1 - d.y;
            }
            else if (d.y < value_type(-0.5))
            {
                d.y = 1 + d.y;
            }

            if (d.sqr_magnitude() < m_distance_sqr)
            {
                return true;
            }
        }
        return false;
    }
}  // namespace noise
