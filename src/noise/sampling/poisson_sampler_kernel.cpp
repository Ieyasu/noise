#include "noise/sampling/poisson_sampler_kernel.h"

#include <cassert>

namespace noise
{
    poisson_sampler_kernel::poisson_sampler_kernel()
    {
    }

    poisson_sampler_kernel::poisson_sampler_kernel(id_type id, size_type size, poisson_sampler_grid* grid, poisson_sampler_params params) :
        m_id(id),
        m_size(size),
        m_grid(grid),
        m_params(params)
    {
    }

    std::vector<int> const& poisson_sampler_kernel::indices() const
    {
        return m_indices;
    }

    // Samples the interior of the kernel.
    void poisson_sampler_kernel::sample_interior()
    {
        assert(m_grid != nullptr);
        assert(m_indices.empty());
        assert(m_active_list.empty());

        insert_initial_point(m_min_x, m_max_x, m_min_y, m_max_y);
        sample();
    }

    void poisson_sampler_kernel::sample_edge(poisson_sampler_kernel const& kernel)
    {
        assert(m_grid != nullptr);
        assert(m_active_list.empty());

        // Bounds of the edges of the two connecting kernels. The width of the edge is
        // 2 * half_margin = 6. We want to insert a point to the middle of the edge,
        // so we shrink it by two from each side, resulting in a edge of size 2.
        size_type edge_min_x, edge_max_x, edge_min_y, edge_max_y;

        // Merge kernels aligned on the y-axis.
        if (m_max_x == kernel.m_max_x)
        {
            edge_min_x = m_min_x;
            edge_max_x = m_max_x;
            edge_min_y = m_max_y + 3;
            edge_max_y = kernel.m_min_y - 3;
            m_max_y = kernel.m_max_y;
        }
        // Merge kernels aligned on the x-axis.
        else
        {
            edge_min_x = m_max_x + 3;
            edge_max_x = kernel.m_min_x - 3;
            edge_min_y = m_min_y;
            edge_max_y = m_max_y;
            m_max_x = kernel.m_max_x;
        }

        insert_initial_point(edge_min_x, edge_max_x, edge_min_y, edge_max_y);
        sample();
    }

    void poisson_sampler_kernel::sample_border()
    {
        assert(m_id == 0);
        assert(m_grid != nullptr);
        assert(m_active_list.empty());

        // Update bounds to match the entire grid.
        m_min_x = 0;
        m_max_x = m_grid->size_sqrt() - 1;
        m_min_y = 0;
        m_max_y = m_grid->size_sqrt() - 1;

        // Insert point in the upper border.
        insert_initial_point(0, m_max_x, 0, 0);

        // Insert point in the left border.
        insert_initial_point(0, 0, 0, m_max_y);

        sample_wrapped();
    }

    void poisson_sampler_kernel::reset(seed_type seed)
    {
        m_indices.clear();
        m_active_list.clear();
        m_random_engine = random_engine_type(seed);

        // Convert morton z-order index to (x, y) coordinates.
        auto const coord = morton_decode(m_id);

        // Calculate bounds of the kernel.
        m_min_x = m_size * coord.x + half_margin;
        m_max_x = std::min(m_min_x + m_size - half_margin, m_grid->size_sqrt() - 1) - half_margin - 1;
        m_min_y = m_size * coord.y + half_margin;
        m_max_y = std::min(m_min_y + m_size - half_margin, m_grid->size_sqrt() - 1) - half_margin - 1;

        assert(m_min_x <= m_max_x);
        assert(m_min_y <= m_max_y);
    }

    inline void poisson_sampler_kernel::insert_initial_point(size_type min_x, size_type max_x, size_type min_y, size_type max_y)
    {
        assert(min_x <= max_x);
        assert(min_y <= max_y);

        auto const p_min_x = min_x / value_type(m_grid->size_sqrt());
        auto const p_max_x = max_x / value_type(m_grid->size_sqrt());
        auto const p_min_y = min_y / value_type(m_grid->size_sqrt());
        auto const p_max_y = max_y / value_type(m_grid->size_sqrt());
        auto random_distribution_x = std::uniform_real_distribution<value_type>(p_min_x, p_max_x);
        auto random_distribution_y = std::uniform_real_distribution<value_type>(p_min_y, p_max_y);
        auto const x = random_distribution_x(m_random_engine);
        auto const y = random_distribution_y(m_random_engine);
        auto const point = point_type(x, y);
        auto const index = m_grid->insert(point);
        m_indices.push_back(index);
        m_active_list.push_back(index);
    }

    inline void poisson_sampler_kernel::sample()
    {
        while (!m_active_list.empty())
        {
            auto random_distribution = std::uniform_int_distribution<size_t>(0, m_active_list.size() - 1);
            auto const active_index = random_distribution(m_random_engine);
            auto const index = m_active_list[active_index];
            auto const& point = m_grid->operator[](index);

            if (!try_insert_neighbour_point(point))
            {
                std::swap(m_active_list[active_index], m_active_list.back());
                m_active_list.pop_back();
            }
        }
    }

    inline void poisson_sampler_kernel::sample_wrapped()
    {
        while (!m_active_list.empty())
        {
            auto random_distribution = std::uniform_int_distribution<size_t>(0, m_active_list.size() - 1);
            auto const active_index = random_distribution(m_random_engine);
            auto const index = m_active_list[active_index];
            auto const& point = m_grid->operator[](index);

            if (!try_insert_neighbour_point_wrapped(point))
            {
                std::swap(m_active_list[active_index], m_active_list.back());
                m_active_list.pop_back();
            }
        }
    }

    inline poisson_sampler_kernel::point_type poisson_sampler_kernel::generate_neighbour_point(point_type const& point)
    {
        auto random_distribution = std::uniform_real_distribution<value_type>(0, 1);
        auto const rand1 = random_distribution(m_random_engine);
        auto const rand2 = random_distribution(m_random_engine);
        auto const r = m_params.distance + m_params.distance * std::sqrt(rand1);
        auto const theta = 2 * PI * rand2;
        auto const x = point.x + r * std::cos(theta);
        auto const y = point.y + r * std::sin(theta);
        return point_type(x, y);
    }

    inline poisson_sampler_kernel::point_type poisson_sampler_kernel::generate_neighbour_point_wrapped(point_type const& point)
    {
        auto p = generate_neighbour_point(point);
        if (p.x < 0)
        {
            p.x = p.x + value_type(1);
        }
        else if (p.x > 1)
        {
            p.x = p.x - value_type(1);
        }

        if (p.y < 0)
        {
            p.y = p.y + value_type(1);
        }
        else if (p.y > 1)
        {
            p.y = p.y - value_type(1);
        }

        return p;
    }

    inline bool poisson_sampler_kernel::try_insert_neighbour_point(point_type const& point)
    {
        for (uint8_t k = 0; k < m_params.num_candidates; ++k)
        {
            auto const p = generate_neighbour_point(point);
            auto const x = static_cast<int>(p.x * m_grid->size_sqrt());
            if (x < static_cast<int>(m_min_x) || x > static_cast<int>(m_max_x))
            {
                continue;
            }

            auto const y = static_cast<int>(p.y * m_grid->size_sqrt());
            if (y < static_cast<int>(m_min_y) || y > static_cast<int>(m_max_y))
            {
                continue;
            }

            auto const index = m_grid->try_insert(p, x, y);
            if (index != -1)
            {
                m_indices.push_back(index);
                m_active_list.push_back(index);
                return true;
            }
        }
        return false;
    }

    inline bool poisson_sampler_kernel::try_insert_neighbour_point_wrapped(point_type const& point)
    {
        for (uint8_t k = 0; k < m_params.num_candidates; ++k)
        {
            auto const p = generate_neighbour_point_wrapped(point);
            auto const x = static_cast<int>(p.x * m_grid->size_sqrt());
            if (x < static_cast<int>(m_min_x) || x > static_cast<int>(m_max_x))
            {
                continue;
            }

            auto const y = static_cast<int>(p.y * m_grid->size_sqrt());
            if (y < static_cast<int>(m_min_y) || y > static_cast<int>(m_max_y))
            {
                continue;
            }

            auto const index = m_grid->try_insert_wrapped(p, x, y);
            if (index != -1)
            {
                m_indices.push_back(index);
                m_active_list.push_back(index);
                return true;
            }
        }
        return false;
    }
}  // namespace noise
