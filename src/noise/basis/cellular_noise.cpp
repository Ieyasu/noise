#include "noise/basis/cellular_noise.h"

#include <iostream>

namespace noise
{
    void cellular_noise_cells::initialize()
    {
        // Calculate the cells only once.
        if (m_initialized)
        {
            return;
        }
        m_initialized = true;

        // The cells are created the same way they are created on the GPU
        // to make sure values generated on the GPU and CPU are identical.
        // Otherwise we could just create random points in range [-0.5, 0.5].
        auto const k = decimal_type(0.142857142857);
        auto const ko = decimal_type(0.428571428571);
        auto const k2 = decimal_type(0.020408163265306);
        auto const kz = decimal_type(0.166666666667);
        auto const kzo = decimal_type(0.416666666667);

        for (auto i = 0; i < 256; ++i)
        {
            auto x = i * k;
            x = x - std::floor(x) - ko;

            auto y = std::floor(i * k);
            y = (y - floor(y * (1.0 / 7.0)) * 7.0) * k - ko;

            m_cell_x[i] = x;
            m_cell_y[i] = y;
            m_cell_z[i] = std::floor(i * k2) * kz - kzo;
            m_cell_w[i] = 0;
        }
    }

    std::array<cellular_noise_cells::value_type, 256> cellular_noise_cells::m_cell_x;
    std::array<cellular_noise_cells::value_type, 256> cellular_noise_cells::m_cell_y;
    std::array<cellular_noise_cells::value_type, 256> cellular_noise_cells::m_cell_z;
    std::array<cellular_noise_cells::value_type, 256> cellular_noise_cells::m_cell_w;
    bool cellular_noise_cells::m_initialized = false;

}  // namespace noise
