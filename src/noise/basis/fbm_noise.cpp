#include "noise/basis/fbm_noise.h"

namespace noise
{
    fbm_noise::fbm_noise(noise_function const* noise) :
        fbm_noise(noise, 4)
    {
    }

    fbm_noise::fbm_noise(noise_function const* noise, uint32_t octave_count) :
        fbm_noise(noise, octave_count, 2, 0.4)
    {
    }

    fbm_noise::fbm_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance) :
        fractal_noise(noise, octave_count, lacunarity, persistance)
    {
    }

    fbm_noise::~fbm_noise()
    {
    }

    fbm_noise::value_type fbm_noise::min() const
    {
        return m_noise->min();
    }

    fbm_noise::value_type fbm_noise::max() const
    {
        return m_noise->max();
    }

    fbm_noise::value_type fbm_noise::evaluate(value_type x, value_type y) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += m_noise->evaluate(x * f, y * f) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }

    fbm_noise::value_type fbm_noise::evaluate(value_type x, value_type y, value_type z) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += m_noise->evaluate(x * f, y * f, z * f) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }

    fbm_noise::value_type fbm_noise::evaluate(value_type x, value_type y, value_type z, value_type w) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += m_noise->evaluate(x * f, y * f, z * f, w * f) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }
}  // namespace noise
