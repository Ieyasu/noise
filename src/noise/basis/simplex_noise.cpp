#include "noise/basis/simplex_noise.h"

#include <cmath>
#include <iostream>

namespace noise
{
    simplex_noise::value_type const simplex_noise::grad_x[] = { 1, 1, 1, -1, -1, -1, 0, 0, 1, -1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1, -1, -1, -1, 1, 1, 1, 1, -1, -1, -1, -1 };
    simplex_noise::value_type const simplex_noise::grad_y[] = { 1, 0, -1, 1, 0, -1, 1, -1, 0, 0, 1, -1, 1, 0, 0, -1, 1, 1, -1, -1, 1, 0, 0, -1, 1, 1, -1, -1, 1, 1, -1, -1 };
    simplex_noise::value_type const simplex_noise::grad_z[] = { 0, 1, 0, 0, 1, 0, 1, 1, -1, -1, -1, -1, 0, 1, -1, 0, 1, -1, 1, -1, 0, 1, -1, 0, 1, -1, 1, -1, 1, -1, 1, -1 };
    simplex_noise::value_type const simplex_noise::grad_w[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0 };

    simplex_noise::simplex_noise(seed_type seed)
    {
        set_seed(seed);
    }

    simplex_noise::value_type simplex_noise::min() const
    {
        return -1;
    }

    simplex_noise::value_type simplex_noise::max() const
    {
        return 1;
    }

    seed_type simplex_noise::seed() const
    {
        return m_seed;
    }

    void simplex_noise::set_seed(seed_type seed)
    {
        m_seed = seed;
        generate_permutation(m_perm.begin(), m_perm.end(), seed);

        for (auto i = 0; i < 256; i++)
        {
            m_perm12[i] = m_perm[i] % 12;
        }
    }

    simplex_noise::value_type simplex_noise::evaluate(value_type x, value_type y) const
    {
        auto const f2 = value_type(0.5) * (std::sqrt(value_type(3)) - 1);
        auto const g2 = (3 - std::sqrt(value_type(3))) / 6;
        auto const g22 = 1 - 2 * g2;

        auto const s = (x + y) * f2;
        auto const i0 = fast_floor(x + s);
        auto const j0 = fast_floor(y + s);

        auto const t = (i0 + j0) * g2;
        auto const x0 = x - i0 + t;
        auto const y0 = y - j0 + t;

        int i1, j1;
        if (x0 > y0)
        {
            i1 = 1;
            j1 = 0;
        }
        else
        {
            i1 = 0;
            j1 = 1;
        }

        auto const x1 = x0 - i1 + g2;
        auto const y1 = y0 - j1 + g2;
        auto const x2 = x0 - g22;
        auto const y2 = y0 - g22;

        value_type n0, n1, n2;

        auto t0 = value_type(0.5) - x0 * x0 - y0 * y0;
        if (t0 < 0)
        {
            n0 = 0;
        }
        else
        {
            t0 *= t0;
            n0 = t0 * t0 * grad_coord_2d(i0, j0, x0, y0);
        }

        auto t1 = value_type(0.5) - x1 * x1 - y1 * y1;
        if (t1 < 0)
        {
            n1 = 0;
        }
        else
        {
            t1 *= t1;
            n1 = t1 * t1 * grad_coord_2d(i0 + i1, j0 + j1, x1, y1);
        }

        auto t3 = value_type(0.5) - x2 * x2 - y2 * y2;
        if (t3 < 0)
        {
            n2 = 0;
        }
        else
        {
            t3 *= t3;
            n2 = t3 * t3 * grad_coord_2d(i0 + 1, j0 + 1, x2, y2);
        }

        return 70 * (n0 + n1 + n2);
    }

    simplex_noise::value_type simplex_noise::evaluate(value_type x, value_type y, value_type z) const
    {
        auto const f3 = value_type(1.0 / 3.0);
        auto const g3 = value_type(1.0 / 6.0);
        auto const g32 = 2 * g3;
        auto const g33 = 1 - 3 * g3;

        auto const s = (x + y + z) * f3;
        auto const i0 = fast_floor(x + s);
        auto const j0 = fast_floor(y + s);
        auto const k0 = fast_floor(z + s);

        auto const t = (i0 + j0 + k0) * g3;
        auto const x0 = x - i0 + t;
        auto const y0 = y - j0 + t;
        auto const z0 = z - k0 + t;

        int i1, j1, k1;
        int i2, j2, k2;
        if (x0 > y0)
        {
            if (y0 > z0)
            {
                i1 = 1;
                j1 = 0;
                k1 = 0;
                i2 = 1;
                j2 = 1;
                k2 = 0;
            }
            else if (x0 > z0)
            {
                i1 = 1;
                j1 = 0;
                k1 = 0;
                i2 = 1;
                j2 = 0;
                k2 = 1;
            }
            else
            {
                i1 = 0;
                j1 = 0;
                k1 = 1;
                i2 = 1;
                j2 = 0;
                k2 = 1;
            }
        }
        else
        {
            if (y0 < z0)
            {
                i1 = 0;
                j1 = 0;
                k1 = 1;
                i2 = 0;
                j2 = 1;
                k2 = 1;
            }
            else if (x0 < z0)
            {
                i1 = 0;
                j1 = 1;
                k1 = 0;
                i2 = 0;
                j2 = 1;
                k2 = 1;
            }
            else
            {
                i1 = 0;
                j1 = 1;
                k1 = 0;
                i2 = 1;
                j2 = 1;
                k2 = 0;
            }
        }

        auto const x1 = x0 - i1 + g3;
        auto const y1 = y0 - j1 + g3;
        auto const z1 = z0 - k1 + g3;
        auto const x2 = x0 - i2 + g32;
        auto const y2 = y0 - j2 + g32;
        auto const z2 = z0 - k2 + g32;
        auto const x3 = x0 - g33;
        auto const y3 = y0 - g33;
        auto const z3 = z0 - g33;

        value_type n0, n1, n2, n3;

        auto t0 = value_type(0.6) - x0 * x0 - y0 * y0 - z0 * z0;
        if (t0 < 0)
        {
            n0 = 0;
        }
        else
        {
            t0 *= t0;
            n0 = t0 * t0 * grad_coord_3d(i0, j0, k0, x0, y0, z0);
        }

        auto t1 = value_type(0.6) - x1 * x1 - y1 * y1 - z1 * z1;
        if (t1 < 0)
        {
            n1 = 0;
        }
        else
        {
            t1 *= t1;
            n1 = t1 * t1 * grad_coord_3d(i0 + i1, j0 + j1, k0 + k1, x1, y1, z1);
        }

        auto t2 = value_type(0.6) - x2 * x2 - y2 * y2 - z2 * z2;
        if (t2 < 0)
        {
            n2 = 0;
        }
        else
        {
            t2 *= t2;
            n2 = t2 * t2 * grad_coord_3d(i0 + i2, j0 + j2, k0 + k2, x2, y2, z2);
        }

        auto t3 = value_type(0.6) - x3 * x3 - y3 * y3 - z3 * z3;
        if (t3 < 0)
        {
            n3 = 0;
        }
        else
        {
            t3 *= t3;
            n3 = t3 * t3 * grad_coord_3d(i0 + 1, j0 + 1, k0 + 1, x3, y3, z3);
        }

        return 32 * (n0 + n1 + n2 + n3);
    }

    simplex_noise::value_type simplex_noise::evaluate(value_type x, value_type y, value_type z, value_type w) const
    {
        auto const f4 = (std::sqrt(value_type(5)) - 1) / 4;
        auto const g4 = (5 - std::sqrt(value_type(5))) / 20;
        auto const g42 = 2 * g4;
        auto const g43 = 3 * g4;
        auto const g44 = 1 - 4 * g4;

        auto const s = (x + y + z + w) * f4;
        auto const i0 = fast_floor(x + s);
        auto const j0 = fast_floor(y + s);
        auto const k0 = fast_floor(z + s);
        auto const l0 = fast_floor(w + s);

        auto const t = (i0 + j0 + k0 + l0) * g4;
        auto const x0 = x - i0 + t;
        auto const y0 = y - j0 + t;
        auto const z0 = z - k0 + t;
        auto const w0 = w - l0 + t;

        auto rankx = 0;
        auto ranky = 0;
        auto rankz = 0;
        auto rankw = 0;

        if (x0 > y0)
            rankx++;
        else
            ranky++;
        if (x0 > z0)
            rankx++;
        else
            rankz++;
        if (x0 > w0)
            rankx++;
        else
            rankw++;
        if (y0 > z0)
            ranky++;
        else
            rankz++;
        if (y0 > w0)
            ranky++;
        else
            rankw++;
        if (z0 > w0)
            rankz++;
        else
            rankw++;

        auto const i1 = rankx >= 3 ? 1 : 0;
        auto const j1 = ranky >= 3 ? 1 : 0;
        auto const k1 = rankz >= 3 ? 1 : 0;
        auto const l1 = rankw >= 3 ? 1 : 0;

        auto const i2 = rankx >= 2 ? 1 : 0;
        auto const j2 = ranky >= 2 ? 1 : 0;
        auto const k2 = rankz >= 2 ? 1 : 0;
        auto const l2 = rankw >= 2 ? 1 : 0;

        auto const i3 = rankx >= 1 ? 1 : 0;
        auto const j3 = ranky >= 1 ? 1 : 0;
        auto const k3 = rankz >= 1 ? 1 : 0;
        auto const l3 = rankw >= 1 ? 1 : 0;

        auto const x1 = x0 - i1 + g4;
        auto const y1 = y0 - j1 + g4;
        auto const z1 = z0 - k1 + g4;
        auto const w1 = w0 - l1 + g4;
        auto const x2 = x0 - i2 + g42;
        auto const y2 = y0 - j2 + g42;
        auto const z2 = z0 - k2 + g42;
        auto const w2 = w0 - l2 + g42;
        auto const x3 = x0 - i3 + g43;
        auto const y3 = y0 - j3 + g43;
        auto const z3 = z0 - k3 + g43;
        auto const w3 = w0 - l3 + g43;
        auto const x4 = x0 - g44;
        auto const y4 = y0 - g44;
        auto const z4 = z0 - g44;
        auto const w4 = w0 - g44;

        value_type n0, n1, n2, n3, n4;

        auto t0 = value_type(0.6) - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
        if (t0 < 0)
        {
            n0 = 0;
        }
        else
        {
            t0 *= t0;
            n0 = t0 * t0 * grad_coord_4d(i0, j0, k0, l0, x0, y0, z0, w0);
        }

        auto t1 = value_type(0.6) - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
        if (t1 < 0)
        {
            n1 = 0;
        }
        else
        {
            t1 *= t1;
            n1 = t1 * t1 * grad_coord_4d(i0 + i1, j0 + j1, k0 + k1, l0 + l1, x1, y1, z1, w1);
        }

        auto t2 = value_type(0.6) - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
        if (t2 < 0)
        {
            n2 = 0;
        }
        else
        {
            t2 *= t2;
            n2 = t2 * t2 * grad_coord_4d(i0 + i2, j0 + j2, k0 + k2, l0 + l2, x2, y2, z2, w2);
        }

        auto t3 = value_type(0.6) - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
        if (t3 < 0)
        {
            n3 = 0;
        }
        else
        {
            t3 *= t3;
            n3 = t3 * t3 * grad_coord_4d(i0 + i3, j0 + j3, k0 + k3, l0 + l3, x3, y3, z3, w3);
        }

        auto t4 = value_type(0.6) - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
        if (t4 < 0)
        {
            n4 = 0;
        }
        else
        {
            t4 *= t4;
            n4 = t4 * t4 * grad_coord_4d(i0 + 1, j0 + 1, k0 + 1, l0 + 1, x4, y4, z4, w4);
        }

        return 27 * (n0 + n1 + n2 + n3 + n4);
    }

    inline uint8_t simplex_noise::index_2d_12(int x, int y) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        return m_perm[uint8_t(py + m_perm[px])] & 7;
    }

    inline uint8_t simplex_noise::index_3d_12(int x, int y, int z) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        auto const pz = uint8_t(z & 0xff);
        return m_perm12[uint8_t(pz + m_perm[uint8_t(py + m_perm[px])])];
    }

    inline uint8_t simplex_noise::index_4d_32(int x, int y, int z, int w) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        auto const pz = uint8_t(z & 0xff);
        auto const pw = uint8_t(w & 0xff);
        return m_perm[uint8_t(pw + m_perm[uint8_t(pz + m_perm[uint8_t(py + m_perm[px])])])] & 31;
    }

    inline simplex_noise::value_type simplex_noise::grad_coord_2d(int x, int y, value_type xd, value_type yd) const
    {
        auto const idx = index_2d_12(x, y);
        return xd * grad_x[idx] + yd * grad_y[idx];
    }

    inline simplex_noise::value_type simplex_noise::grad_coord_3d(int x, int y, int z, value_type xd, value_type yd, value_type zd) const
    {
        auto const idx = index_3d_12(x, y, z);
        return xd * grad_x[idx] + yd * grad_y[idx] + zd * grad_z[idx];
    }

    inline simplex_noise::value_type simplex_noise::grad_coord_4d(int x, int y, int z, int w, value_type xd, value_type yd, value_type zd, value_type wd) const
    {
        auto const idx = index_4d_32(x, y, z, w);
        return xd * grad_x[idx] + yd * grad_y[idx] + zd * grad_z[idx] + wd * grad_w[idx];
    }
}  // namespace noise
