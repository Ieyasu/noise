#include "noise/basis/fractal_noise.h"

namespace noise
{
    fractal_noise::fractal_noise(noise_function const* noise) :
        fractal_noise(noise, 4)
    {
        update_fractals();
    }

    fractal_noise::fractal_noise(noise_function const* noise, uint32_t octave_count) :
        fractal_noise(noise, octave_count, 2, 0.4)
    {
        update_fractals();
    }

    fractal_noise::fractal_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance) :
        m_noise(noise),
        m_octave_count(octave_count),
        m_lacunarity(lacunarity),
        m_persistance(persistance),
        m_amplitude_sum(0)
    {
        update_fractals();
    }

    fractal_noise::~fractal_noise()
    {
    }

    noise_function const& fractal_noise::noise() const
    {
        return *m_noise;
    }

    uint32_t fractal_noise::octave_count() const
    {
        return m_octave_count;
    }

    void fractal_noise::set_octave_count(uint32_t octave_count)
    {
        m_octave_count = octave_count;
        update_fractals();
    }

    fractal_noise::value_type fractal_noise::persistance() const
    {
        return m_persistance;
    }

    void fractal_noise::set_persistance(value_type persistance)
    {
        m_persistance = persistance;
        update_fractals();
    }

    fractal_noise::value_type fractal_noise::lacunarity() const
    {
        return m_lacunarity;
    }

    void fractal_noise::set_lacunarity(value_type lacunarity)
    {
        m_lacunarity = lacunarity;
        update_fractals();
    }

    void fractal_noise::update_fractals()
    {
        m_amplitudes.resize(m_octave_count);
        m_frequencies.resize(m_octave_count);
        m_amplitude_sum = 1;

        if (m_octave_count > 0)
        {
            m_frequencies[0] = value_type(1);
            m_amplitudes[0] = value_type(1);
        }

        for (uint32_t i = 1; i < m_octave_count; ++i)
        {
            m_frequencies[i] = m_frequencies[i - 1] * m_lacunarity;
            m_amplitudes[i] = m_amplitudes[i - 1] * m_persistance;
            m_amplitude_sum += m_amplitudes[i];
        }
    }
}  // namespace noise
