#include "noise/basis/turbulence_noise.h"

namespace noise
{
    turbulence_noise::turbulence_noise(noise_function const* noise, value_type strength) :
        turbulence_noise(noise, 4, strength)
    {
    }

    turbulence_noise::turbulence_noise(noise_function const* noise, uint32_t octave_count, value_type strength) :
        turbulence_noise(noise, octave_count, 2, 0.4, strength)
    {
    }

    turbulence_noise::turbulence_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance, value_type strength) :
        fractal_noise(noise, octave_count, lacunarity, persistance),
        m_strength(strength)
    {
    }

    turbulence_noise::~turbulence_noise()
    {
    }

    turbulence_noise::value_type turbulence_noise::min() const
    {
        return 0;
    }

    turbulence_noise::value_type turbulence_noise::max() const
    {
        return std::max(m_noise->max(), -m_noise->min());
    }

    turbulence_noise::value_type turbulence_noise::evaluate(value_type x, value_type y) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += std::pow(std::abs(m_noise->evaluate(x * f, y * f)), m_strength) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }

    turbulence_noise::value_type turbulence_noise::evaluate(value_type x, value_type y, value_type z) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += std::pow(std::abs(m_noise->evaluate(x * f, y * f, z * f)), m_strength) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }

    turbulence_noise::value_type turbulence_noise::evaluate(value_type x, value_type y, value_type z, value_type w) const
    {
        auto value = value_type(0);
        for (size_t i = 0; i < m_octave_count; ++i)
        {
            auto const f = m_frequencies[i];
            value += std::pow(std::abs(m_noise->evaluate(x * f, y * f, z * f, w * f)), m_strength) * m_amplitudes[i];
        }
        return value / m_amplitude_sum;
    }
}  // namespace noise
