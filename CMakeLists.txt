cmake_minimum_required(VERSION 3.10 FATAL_ERROR)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

##########################
# Compiler configuration #
##########################

# set(CMAKE_CXX_STANDARD 20)
# set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

####################
# Project settings #
####################

set(PROJECT_NAME noise)
set(PROJECT_VERSION_MAJOR 0)
set(PROJECT_VERSION_MINOR 1)
project(${PROJECT_NAME} LANGUAGES CXX)

############
# Packages #
############

find_package(PNG REQUIRED)
find_package(OpenMP REQUIRED)

###############
# Directories #
###############

set(PROJECT_TARGET_DIR bin)
set(PROJECT_SOURCE_DIR src)
set(PROJECT_LIBRARY_DIR lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_TARGET_DIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_TARGET_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_TARGET_DIR})

set(PROJECT_INCLUDE_DIRS include)
set(BENCHMARK_INCLUDE_DIRS ${PROJECT_INCLUDE_DIRS})
set(PROJECT_LIBRARIES ${OpenMP_CXX_LIBRARIES})
set(BENCHMARK_LIBRARIES ${PROJECT_LIBRARIES} ${PNG_LIBRARY})

#########
# Files #
#########

set(BENCHMARK_SOURCES
    ${PROJECT_SOURCE_DIR}/benchmark.cpp
    )
set(PROJECT_SOURCES
    ${PROJECT_SOURCE_DIR}/noise/basis/cellular_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/basis/fbm_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/basis/fractal_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/basis/ridge_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/basis/simplex_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/basis/turbulence_noise.cpp
    ${PROJECT_SOURCE_DIR}/noise/sampling/poisson_sampler.cpp
    ${PROJECT_SOURCE_DIR}/noise/sampling/poisson_sampler_grid.cpp
    ${PROJECT_SOURCE_DIR}/noise/sampling/poisson_sampler_kernel.cpp
    ${PROJECT_SOURCE_DIR}/terrain/erosion/flow_erosion_simulation.cpp
    ${PROJECT_SOURCE_DIR}/terrain/erosion/droplet_erosion_simulation.cpp
    ${PROJECT_SOURCE_DIR}/terrain/heightmap/heightmap_renderer.cpp
    # ${PROJECT_SOURCE_DIR}/terrain/mesh/terrain.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/astar_node_pool.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/astar_node_queue.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/road_renderer.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/road_edge.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/road_generator.cpp
    ${PROJECT_SOURCE_DIR}/terrain/road/road_vertex.cpp
    )

##################
# Compiler flags #
##################

set(WARNING_OPTIONS -pedantic -fstrict-aliasing -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-promo -Wswitch-default -Wno-unused -Wzero-as-null-pointer-constant -Wno-deprecated-declarations -Wold-style-cast)

set(PROJECT_MISC_OPTIONS -std=c++2a -fconcepts -fPIC)
set(PROJECT_DEBUG_OPTIONS -Og -g -Wno-unknown-pragmas ${PROJECT_MISC_OPTIONS} ${WARNING_OPTIONS})
set(PROJECT_RELEASE_OPTIONS -O3 -DNDDEBUG -fopenmp ${PROJECT_MISC_OPTIONS} ${WARNING_OPTIONS})

set(BENCHMARK_MISC_OPTIONS -std=c++2a)
set(BENCHMARK_DEBUG_OPTIONS -Og -g -Wno-unknown-pragmas ${BENCHMARK_MISC_OPTIONS} ${WARNING_OPTIONS})
set(BENCHMARK_RELEASE_OPTIONS -O3 -DNDDEBUG -fopenmp ${BENCHMARK_MISC_OPTIONS} ${WARNING_OPTIONS})

######################
# Project generation #
######################

# Google benchmark
set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "Suppressing benchmark's tests" FORCE)
add_subdirectory(${PROJECT_LIBRARY_DIR}/google/benchmark)

# Noise
add_library(noise SHARED ${PROJECT_SOURCES})
target_compile_options(noise PRIVATE "$<$<CONFIG:RELEASE>:${PROJECT_RELEASE_OPTIONS}>")
target_compile_options(noise PRIVATE "$<$<CONFIG:DEBUG>:${PROJECT_DEBUG_OPTIONS}>")
target_include_directories(noise PRIVATE ${PROJECT_INCLUDE_DIRS})
target_include_directories(noise PRIVATE ${PROJECT_INCLUDE_DIRS})
target_link_libraries(noise PRIVATE ${PROJECT_LIBRARIES})

# Benchmark
add_executable(noise_benchmark ${BENCHMARK_SOURCES})
target_compile_options(noise_benchmark PRIVATE "$<$<CONFIG:RELEASE>:${BENCHMARK_RELEASE_OPTIONS}>")
target_compile_options(noise_benchmark PRIVATE "$<$<CONFIG:DEBUG>:${BENCHMARK_DEBUG_OPTIONS}>")
target_include_directories(noise_benchmark PRIVATE ${BENCHMARK_INCLUDE_DIRS})
target_link_libraries(noise_benchmark PRIVATE noise benchmark ${BENCHMARK_LIBRARIES})
