#!/bin/sh

mode=${1:-release}
n=${2:-1}

cd build/${mode} && make && cd bin && ./noise_benchmark --benchmark_repetitions=${n}
