#pragma once

#include <memory>
#include <vector>

#include "noise/noise_function.h"

namespace noise
{
    class fractal_noise : public noise_function
    {
    public:
        using value_type = typename noise_function::value_type;

        fractal_noise(noise_function const* noise);
        fractal_noise(noise_function const* noise, uint32_t octave_count);
        fractal_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance);
        ~fractal_noise() override;

        noise_function const& noise() const;

        uint32_t octave_count() const;
        void set_octave_count(uint32_t octave_count);

        value_type persistance() const;
        void set_persistance(value_type persistance);

        value_type lacunarity() const;
        void set_lacunarity(value_type lacunarity);

    protected:
        noise_function const* m_noise;
        uint32_t m_octave_count;
        value_type m_lacunarity;
        value_type m_persistance;
        value_type m_amplitude_sum;
        std::vector<value_type> m_amplitudes;
        std::vector<value_type> m_frequencies;

        void update_fractals();
    };
}  // namespace noise
