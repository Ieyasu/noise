#pragma once

#include <memory>

#include "container/grid2.h"
#include "math/shape/rect.h"
#include "noise/modifier/noise_clamp_modifier.h"
#include "noise/modifier/noise_clip_modifier.h"
#include "noise/modifier/noise_intensity_modifier.h"
#include "noise/modifier/noise_invert_modifier.h"
#include "noise/modifier/noise_normalize_modifier.h"
#include "noise/modifier/noise_output_modifier.h"
#include "noise/noise_function.h"

namespace noise
{
    class modified_noise : public noise_function
    {
    public:
        using value_type = decimal_type;

        modified_noise(noise_function const* noise) :
            m_noise(noise),
            m_min(noise->min()),
            m_max(noise->max())
        {
        }

        ~modified_noise()
        {
        }

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        }

        noise_function const& noise() const
        {
            return *m_noise;
        }

        template <typename T, typename... Args>
        void add_output_modifier(Args&&... args)
        {
            auto modifier = std::make_unique<T>(std::forward<Args>(args)...);
            m_output_modifiers.push_back(std::move(modifier));
            initialize_modifiers();
        }

        void add_clamp_modifier(value_type min, value_type max)
        {
            add_output_modifier<noise_clamp_modifier>(min, max);
        }

        void add_clip_modifier(value_type min, value_type max)
        {
            add_output_modifier<noise_clip_modifier>(min, max);
        }

        void add_intensity_modifier(value_type multiplier)
        {
            add_output_modifier<noise_intensity_modifier>(multiplier);
        }

        void add_invert_modifier()
        {
            add_output_modifier<noise_invert_modifier>();
        }

        void add_normalize_modifier(value_type min, value_type max)
        {
            add_output_modifier<noise_normalize_modifier>(min, max);
        }

        value_type evaluate(value_type x, value_type y) const override
        {
            return modify_output(m_noise->evaluate(x, y));
        }

        value_type evaluate(value_type x, value_type y, value_type z) const override
        {
            return modify_output(m_noise->evaluate(x, y, z));
        }

        value_type evaluate(value_type x, value_type y, value_type z, value_type w) const override
        {
            return modify_output(m_noise->evaluate(x, y, z, w));
        }

    private:
        noise_function const* m_noise;
        float m_min;
        float m_max;
        std::vector<std::unique_ptr<noise_output_modifier>> m_output_modifiers;

        void initialize_modifiers()
        {
            m_min = m_noise->min();
            m_max = m_noise->max();
            for (auto const& modifier : m_output_modifiers)
            {
                modifier->set_min_max(m_min, m_max);
                m_min = modifier->min();
                m_max = modifier->max();
            }
        }

        value_type modify_output(value_type value) const
        {
            for (auto const& modifier : m_output_modifiers)
            {
                value = modifier->modify(value);
            }
            return value;
        }
    };
}  // namespace noise
