#include "fractal_noise.h"

namespace noise
{
    class ridge_noise : public fractal_noise
    {
    public:
        using value_type = typename fractal_noise::value_type;

        ridge_noise(noise_function const* noise, value_type strength = 1);
        ridge_noise(noise_function const* noise, uint32_t octave_count, value_type strength = 1);
        ridge_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance, value_type strength = 1);
        ~ridge_noise() override;

        value_type min() const override;
        value_type max() const override;

        value_type evaluate(value_type x, value_type y) const override;
        value_type evaluate(value_type x, value_type y, value_type z) const override;
        value_type evaluate(value_type x, value_type y, value_type z, value_type w) const override;

    private:
        value_type m_strength;
    };
}  // namespace noise
