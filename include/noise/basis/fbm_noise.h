#include "fractal_noise.h"

namespace noise
{
    class fbm_noise : public fractal_noise
    {
    public:
        using value_type = typename fractal_noise::value_type;

        fbm_noise(noise_function const* noise);
        fbm_noise(noise_function const* noise, uint32_t octave_count);
        fbm_noise(noise_function const* noise, uint32_t octave_count, value_type lacunarity, value_type persistance);
        ~fbm_noise() override;

        value_type min() const override;
        value_type max() const override;

        value_type evaluate(value_type x, value_type y) const override;
        value_type evaluate(value_type x, value_type y, value_type z) const override;
        value_type evaluate(value_type x, value_type y, value_type z, value_type w) const override;
    };
}  // namespace noise
