#pragma once

#include <array>
#include <cmath>

#include "noise/noise_function.h"

namespace noise
{
    class simplex_noise : public noise_function
    {
    public:
        using value_type = noise_function::value_type;

        simplex_noise(seed_type seed = 0);
        ~simplex_noise() override
        {
        }

        value_type min() const override;
        value_type max() const override;

        seed_type seed() const;
        void set_seed(seed_type seed);

        value_type evaluate(value_type x, value_type y) const override;
        value_type evaluate(value_type x, value_type y, value_type z) const override;
        value_type evaluate(value_type x, value_type y, value_type z, value_type w) const override;

    private:
        static value_type const grad_x[];
        static value_type const grad_y[];
        static value_type const grad_z[];
        static value_type const grad_w[];

        seed_type m_seed;
        std::array<uint8_t, 256> m_perm;
        std::array<uint8_t, 256> m_perm12;

        uint8_t index_2d_12(int x, int y) const;
        uint8_t index_3d_12(int x, int y, int z) const;
        uint8_t index_4d_32(int x, int y, int z, int w) const;

        value_type grad_coord_2d(int x, int y, value_type xd, value_type yd) const;
        value_type grad_coord_3d(int x, int y, int z, value_type xd, value_type yd, value_type zd) const;
        value_type grad_coord_4d(int x, int y, int z, int w, value_type xd, value_type yd, value_type zd, value_type wd) const;
    };
}  // namespace noise
