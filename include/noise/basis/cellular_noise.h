#pragma once

#include <algorithm>
#include <array>
#include <random>

#include "math/math.h"
#include "noise/noise_function.h"
#include "noise/util/distance_metric.h"

namespace noise
{
    class cellular_noise_cells
    {
    public:
        using value_type = noise_function::value_type;

        static void initialize();

        static value_type x(size_t i)
        {
            return m_cell_x[i];
        }

        static value_type y(size_t i)
        {
            return m_cell_y[i];
        }

        static value_type z(size_t i)
        {
            return m_cell_z[i];
        }

        static value_type w(size_t i)
        {
            return m_cell_w[i];
        }

    private:
        static std::array<value_type, 256> m_cell_x;
        static std::array<value_type, 256> m_cell_y;
        static std::array<value_type, 256> m_cell_z;
        static std::array<value_type, 256> m_cell_w;
        static bool m_initialized;
    };

    template <int TNumDistances = 2, typename TDistanceMetric = euclidean_distance_metric>
    class cellular_noise : public noise_function
    {
    public:
        using value_type = noise_function::value_type;
        using distance_metric_type = TDistanceMetric;
        using weights_type = std::array<value_type, TNumDistances>;

        cellular_noise(seed_type seed = 0);
        cellular_noise(seed_type seed, weights_type weights);
        ~cellular_noise() override;

        value_type min() const override;
        value_type max() const override;

        seed_type seed() const;
        void set_seed(seed_type seed);

        weights_type const& weights() const;
        void set_weights(weights_type const& weights);

        value_type evaluate(value_type x, value_type y) const override;
        value_type evaluate(value_type x, value_type y, value_type z) const override;
        value_type evaluate(value_type x, value_type y, value_type z, value_type w) const override;

    private:
        distance_metric_type m_distance_metric;
        value_type m_max_distance_inv;
        value_type m_min;
        value_type m_max;
        value_type m_jitter;
        seed_type m_seed;
        weights_type m_weights;
        std::array<uint8_t, 256> m_perm;

        value_type get_value(weights_type const& distances, value_type weight) const;

        uint8_t index_256(int x, int y) const;
        uint8_t index_256(int x, int y, int z) const;
        uint8_t index_256(int x, int y, int z, int w) const;

        static weights_type get_distances();
        static void update_distances(weights_type& distances, value_type distance);
    };

    template <typename TDistanceMetric = euclidean_distance_metric>
    using cellular_noise_F1 = cellular_noise<1, TDistanceMetric>;

    template <typename TDistanceMetric = euclidean_distance_metric>
    using cellular_noise_F1_F2 = cellular_noise<2, TDistanceMetric>;

    template <int T1, typename T2>
    cellular_noise<T1, T2>::cellular_noise(seed_type seed) :
        m_distance_metric(),
        m_min(0),
        m_max(0),
        m_jitter(value_type(1.0))
    {
        auto const max_distance_x = value_type(1.5 - 0.5 * (1.0 - m_jitter));
        auto const max_distance_y = value_type(0.5 - 0.5 * (1.0 - m_jitter));
        auto const max_distance = m_distance_metric.distance(max_distance_x, max_distance_y);
        m_max_distance_inv = 1 / max_distance;
        set_seed(seed);
    }

    template <int T1, typename T2>
    cellular_noise<T1, T2>::cellular_noise(seed_type seed, weights_type weights) :
        cellular_noise(seed)
    {
        set_weights(weights);
    }

    template <int T1, typename T2>
    cellular_noise<T1, T2>::~cellular_noise()
    {
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::min() const
    {
        return m_min;
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::max() const
    {
        return m_max;
    }

    template <int T1, typename T2>
    seed_type cellular_noise<T1, T2>::seed() const
    {
        return m_seed;
    }

    template <int T1, typename T2>
    void cellular_noise<T1, T2>::set_seed(seed_type seed)
    {
        cellular_noise_cells::initialize();
        m_seed = seed;
        generate_permutation(m_perm.begin(), m_perm.end(), seed);
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::weights_type const& cellular_noise<T1, T2>::weights() const
    {
        return m_weights;
    }

    template <int T1, typename T2>
    void cellular_noise<T1, T2>::set_weights(weights_type const& weights)
    {
        m_weights = weights;
        m_min = 0;
        m_max = 0;
        for (size_t i = 1; i <= m_weights.size(); ++i)
        {
            auto value = value_type(0);
            for (size_t j = 1; j <= i; ++j)
            {
                value += m_weights[m_weights.size() - j];
            }
            m_min = std::min(m_min, value);
            m_max = std::max(m_max, value);
        }
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::evaluate(value_type x, value_type y) const
    {
        auto const xr = fast_round(x);
        auto const yr = fast_round(y);

        auto closest_weight = value_type(1);
        auto distances = get_distances();
        for (auto xi = xr - 1; xi <= xr + 1; ++xi)
        {
            for (auto yi = yr - 1; yi <= yr + 1; ++yi)
            {
                auto const i = index_256(xi, yi);
                auto const vec_x = xi - x + cellular_noise_cells::x(i) * m_jitter;
                auto const vec_y = yi - y + cellular_noise_cells::y(i) * m_jitter;
                auto const distance = m_distance_metric.distance(vec_x, vec_y);
                update_distances(distances, distance);
            }
        }

        return get_value(distances, closest_weight);
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::evaluate(value_type x, value_type y, value_type z) const
    {
        auto const xr = fast_round(x);
        auto const yr = fast_round(y);
        auto const zr = fast_round(z);

        auto closest_weight = value_type(1);
        auto distances = get_distances();
        for (auto xi = xr - 1; xi <= xr + 1; ++xi)
        {
            for (auto yi = yr - 1; yi <= yr + 1; ++yi)
            {
                for (auto zi = zr - 1; zi <= zr + 1; ++zi)
                {
                    auto const i = index_256(xi, yi, zi);
                    auto const vec_x = xi - x + cellular_noise_cells::x(i) * m_jitter;
                    auto const vec_y = yi - y + cellular_noise_cells::y(i) * m_jitter;
                    auto const vec_z = zi - z + cellular_noise_cells::z(i) * m_jitter;
                    auto const distance = m_distance_metric.distance(vec_x, vec_y, vec_z);
                    update_distances(distances, distance);
                }
            }
        }

        return get_value(distances, closest_weight);
    }

    template <int T1, typename T2>
    typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::evaluate(value_type x, value_type y, value_type z, value_type w) const
    {
        auto const xr = fast_round(x);
        auto const yr = fast_round(y);
        auto const zr = fast_round(z);
        auto const wr = fast_round(w);

        auto closest_weight = value_type(1);
        auto distances = get_distances();
        for (auto xi = xr - 1; xi <= xr + 1; ++xi)
        {
            for (auto yi = yr - 1; yi <= yr + 1; ++yi)
            {
                for (auto zi = zr - 1; zi <= zr + 1; ++zi)
                {
                    for (auto wi = wr - 1; wi <= wr + 1; ++wi)
                    {
                        auto const i = index_256(xi, yi, zi, wi);
                        auto const vec_x = xi - x + cellular_noise_cells::x(i) * m_jitter;
                        auto const vec_y = yi - y + cellular_noise_cells::y(i) * m_jitter;
                        auto const vec_z = zi - z + cellular_noise_cells::z(i) * m_jitter;
                        auto const vec_w = wi - w + cellular_noise_cells::w(i) * m_jitter;
                        auto const distance = m_distance_metric.distance(vec_x, vec_y, vec_z, vec_w);
                        update_distances(distances, distance);
                    }
                }
            }
        }

        return get_value(distances, closest_weight);
    }

    template <int T1, typename T2>
    inline typename cellular_noise<T1, T2>::value_type cellular_noise<T1, T2>::get_value(weights_type const& distances, value_type weight) const
    {
        auto value = value_type(0);
        for (auto i = 0; i < T1; ++i)
        {
            value += distances[i] * m_max_distance_inv * m_weights[i];
        }
        return value * weight;
    }

    template <int T1, typename T2>
    inline typename cellular_noise<T1, T2>::weights_type cellular_noise<T1, T2>::get_distances()
    {
        auto distances = weights_type();
        for (auto i = 0; i < T1; ++i)
        {
            distances[i] = std::numeric_limits<value_type>::max();
        }
        return distances;
    }

    template <int T1, typename T2>
    inline void cellular_noise<T1, T2>::update_distances(weights_type& distances, value_type distance)
    {
        if (distance >= distances[T1 - 1])
        {
            return;
        }

        distances[T1 - 1] = distance;
        for (auto j = T1 - 1; j > 0; --j)
        {
            if (distances[j] < distances[j - 1])
            {
                std::swap(distances[j - 1], distances[j]);
            }
            else
            {
                break;
            }
        }
    }

    template <int T1, typename T2>
    inline uint8_t cellular_noise<T1, T2>::index_256(int x, int y) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        return m_perm[uint8_t(py + m_perm[px])];
    }

    template <int T1, typename T2>
    inline uint8_t cellular_noise<T1, T2>::index_256(int x, int y, int z) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        auto const pz = uint8_t(z & 0xff);
        return m_perm[uint8_t(pz + m_perm[uint8_t(py + m_perm[px])])];
    }

    template <int T1, typename T2>
    inline uint8_t cellular_noise<T1, T2>::index_256(int x, int y, int z, int w) const
    {
        auto const px = uint8_t(x & 0xff);
        auto const py = uint8_t(y & 0xff);
        auto const pz = uint8_t(z & 0xff);
        auto const pw = uint8_t(w & 0xff);
        return m_perm[uint8_t(pw + m_perm[uint8_t(pz + m_perm[uint8_t(py + m_perm[px])])])];
    }
}  // namespace noise
