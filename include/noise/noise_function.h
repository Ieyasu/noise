#pragma once

#include "math/math.h"

namespace noise
{
    class noise_function
    {
    public:
        using value_type = decimal_type;

        virtual ~noise_function() = default;

        virtual value_type min() const = 0;
        virtual value_type max() const = 0;

        virtual value_type evaluate(value_type x, value_type y) const = 0;
        virtual value_type evaluate(value_type x, value_type y, value_type z) const = 0;
        virtual value_type evaluate(value_type x, value_type y, value_type z, value_type w) const = 0;
    };
}  // namespace noise
