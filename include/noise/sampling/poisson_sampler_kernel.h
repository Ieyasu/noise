#pragma once

#include <random>

#include "noise/sampling/poisson_sampler_grid.h"
#include "noise/sampling/poisson_sampler_params.h"

namespace noise
{
    class poisson_sampler_kernel
    {
    public:
        using id_type = uint16_t;
        using size_type = size_t;
        using point_type = poisson_sampler_grid::point_type;
        using value_type = poisson_sampler_grid::value_type;

        static constexpr size_t half_margin = 3;

        poisson_sampler_kernel();
        poisson_sampler_kernel(id_type id, size_type size, poisson_sampler_grid* grid, poisson_sampler_params params);

        std::vector<int> const& indices() const;
        void reset(seed_type seed);
        void sample_interior();
        void sample_edge(poisson_sampler_kernel const& kernel);
        void sample_border();

    private:
        size_type m_min_x;
        size_type m_max_x;
        size_type m_min_y;
        size_type m_max_y;

        id_type m_id;
        size_type m_size;
        poisson_sampler_grid* m_grid;
        poisson_sampler_params m_params;

        std::vector<int> m_indices;
        std::vector<int> m_active_list;
        random_engine_type m_random_engine;

        void sample();
        void sample_wrapped();
        void insert_initial_point(size_type min_x, size_type max_x, size_type min_y, size_type max_y);

        point_type generate_neighbour_point(point_type const& point);
        point_type generate_neighbour_point_wrapped(point_type const& point);
        bool try_insert_neighbour_point(point_type const& point);
        bool try_insert_neighbour_point_wrapped(point_type const& point);
    };
}  // namespace noise
