#pragma once

#include "noise/sampling/poisson_sampler_kernel.h"

namespace noise
{
    class poisson_sampler
    {
    public:
        // Must be a power of two.
        static constexpr poisson_sampler_kernel::size_type max_kernels_sqrt = 8;

        using id_type = poisson_sampler_kernel::id_type;
        using point_type = poisson_sampler_grid::point_type;
        using value_type = poisson_sampler_grid::value_type;

        poisson_sampler(poisson_sampler_params params);
        std::vector<point_type> sample();

    private:
        poisson_sampler_grid m_grid;
        poisson_sampler_params m_params;
        std::vector<poisson_sampler_kernel> m_kernels;
    };
}  // namespace noise
