#pragma once

#include "math/math.h"

namespace noise
{
    struct poisson_sampler_params
    {
        using value_type = decimal_type;

        // Seed of the sampler.
        seed_type seed = 0;

        // Distance between points.
        value_type distance = 0.01;

        // Number of candidate points to test.
        uint8_t num_candidates = 30;
    };
}  // namespace noise
