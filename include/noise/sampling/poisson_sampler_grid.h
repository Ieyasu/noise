#pragma once

#include <vector>

#include "container/grid2.h"
#include "math/math.h"

namespace noise
{
    class poisson_sampler_grid
    {
    public:
        using value_type = decimal_type;
        using point_type = vec<decimal_type, 2>;
        using grid_type = grid2<poisson_sampler_grid::point_type>;
        using size_type = grid_type::size_type;

        static constexpr point_type invalid_point = point_type(-1);

        poisson_sampler_grid(value_type distance);

        size_type size_sqrt() const;
        point_type const& operator[](size_t i) const;

        void clear();
        int insert(point_type const& point);
        int try_insert(point_type const& point, int x, int y);
        int try_insert_wrapped(point_type const& point, int x, int y);

    private:
        grid_type m_grid;
        value_type m_distance_sqr;

        int get_coord(int i);
        int get_coord_wrapped(int i);
        bool intersects(vec2f const& point, int x, int y);
        bool intersects_wrapped(vec2f const& point, int x, int y);
    };

    inline poisson_sampler_grid::size_type poisson_sampler_grid::size_sqrt() const
    {
        return m_grid.width();
    }

    inline poisson_sampler_grid::point_type const& poisson_sampler_grid::operator[](size_t i) const
    {
        return m_grid[i];
    }

}  // namespace noise
