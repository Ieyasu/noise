#pragma once

#include "math/math.h"

namespace noise
{
    class euclidean_distance_metric
    {
    public:
        template <typename T>
        static T distance(T x, T y)
        {
            return std::sqrt(x * x + y * y);
        }

        template <typename T>
        static T distance(T x, T y, T z)
        {
            return std::sqrt(x * x + y * y + z * z);
        }

        template <typename T>
        static T distance(T x, T y, T z, T w)
        {
            return std::sqrt(x * x + y * y + z * z + w * w);
        }
    };

    class euclidean_squared_distance_metric
    {
    public:
        template <typename T>
        static T distance(T x, T y)
        {
            return x * x + y * y;
        }

        template <typename T>
        static T distance(T x, T y, T z)
        {
            return x * x + y * y + z * z;
        }

        template <typename T>
        static T distance(T x, T y, T z, T w)
        {
            return x * x + y * y + z * z + w * w;
        }
    };

    class manhattan_distance_metric
    {
    public:
        template <typename T>
        static T distance(T x, T y)
        {
            return std::abs(x) + std::abs(y);
        }

        template <typename T>
        static T distance(T x, T y, T z)
        {
            return std::abs(x) + std::abs(y) + std::abs(z);
        }

        template <typename T>
        static T distance(T x, T y, T z, T w)
        {
            return std::abs(x) + std::abs(y) + std::abs(z) + std::abs(w);
        }
    };
}  // namespace noise
