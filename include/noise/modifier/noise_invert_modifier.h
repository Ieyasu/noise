#pragma once

#include "noise/modifier/noise_output_modifier.h"

namespace noise
{
    class noise_invert_modifier : public noise_output_modifier
    {
    public:
        using value_type = noise_output_modifier::value_type;

        noise_invert_modifier() :
            m_offset(0),
            m_min(0),
            m_max(0)
        {
        }

        ~noise_invert_modifier() override
        {
        }

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        };

        void set_min_max(value_type min, value_type max) override
        {
            m_offset = min + max;
            m_min = min;
            m_max = max;
        }

        value_type modify(value_type value) const override
        {
            return m_offset - value;
        }

    private:
        value_type m_offset;
        value_type m_min;
        value_type m_max;
    };
}  // namespace noise
