#pragma once

#include "math/math.h"

namespace noise
{
    class noise_output_modifier
    {
    public:
        using value_type = decimal_type;

        virtual ~noise_output_modifier() {};

        virtual value_type min() const = 0;
        virtual value_type max() const = 0;
        virtual void set_min_max(value_type min, value_type max) = 0;
        virtual value_type modify(value_type x) const = 0;
    };
}  // namespace noise
