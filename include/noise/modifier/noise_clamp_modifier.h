#pragma once

#include <algorithm>

#include "noise/modifier/noise_output_modifier.h"

namespace noise
{
    class noise_clamp_modifier : public noise_output_modifier
    {
    public:
        using value_type = noise_output_modifier::value_type;

        noise_clamp_modifier(value_type min_limit, value_type max_limit) :
            m_min_limit(min_limit),
            m_max_limit(max_limit),
            m_min(0),
            m_max(0)
        {
        }

        ~noise_clamp_modifier() override
        {
        }

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        }

        value_type min_limit() const
        {
            return m_min_limit;
        }

        value_type max_limit() const
        {
            return m_max_limit;
        }

        void set_min_max(value_type min, value_type max) override
        {
            m_min = std::max(min, m_min_limit);
            m_max = std::min(max, m_max_limit);
        }

        value_type modify(value_type value) const override
        {
            return std::clamp(value, m_min_limit, m_max_limit);
        }

    private:
        value_type m_min_limit;
        value_type m_max_limit;
        value_type m_min;
        value_type m_max;
    };
}  // namespace noise
