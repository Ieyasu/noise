#pragma once

#include "noise/modifier/noise_output_modifier.h"

namespace noise
{
    class noise_clip_modifier : public noise_output_modifier
    {
    public:
        using value_type = noise_output_modifier::value_type;

        noise_clip_modifier(value_type min, value_type max) :
            m_clip_min(min),
            m_clip_max(max),
            m_clip_min_clamped(0),
            m_clip_max_clamped(0),
            m_min(0),
            m_max(0)
        {
        }

        ~noise_clip_modifier() override
        {
        }

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        }

        void set_min_max(value_type min, value_type max) override
        {
            m_min = min;
            m_max = max;
            m_clip_min_clamped = std::clamp(m_clip_min, min, max);
            m_clip_max_clamped = std::clamp(m_clip_max, min, max);
            m_scale = 1 / (m_clip_max_clamped - m_clip_min_clamped);
        }

    private:
        value_type m_clip_min;
        value_type m_clip_max;
        value_type m_clip_min_clamped;
        value_type m_clip_max_clamped;
        value_type m_min;
        value_type m_max;
        value_type m_scale;

        value_type modify(value_type value) const override
        {
            if (value <= m_clip_min_clamped)
            {
                return m_min;
            }
            if (value >= m_clip_max_clamped)
            {
                return m_max;
            }

            auto t = m_scale * (value - m_clip_min_clamped);
            return std::lerp(m_min, m_max, t);
        }
    };
}  // namespace noise
