#pragma once

#include "noise/modifier/noise_output_modifier.h"

namespace noise
{
    class noise_normalize_modifier : public noise_output_modifier
    {
    public:
        using value_type = noise_output_modifier::value_type;

        noise_normalize_modifier(value_type min, value_type max) :
            m_min(min),
            m_max(max),
            m_scale(0),
            m_offset(0)
        {
        }

        ~noise_normalize_modifier() override
        {
        }

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        };

        void set_min_max(value_type min, value_type max) override
        {
            m_scale = (m_max - m_min) / (max - min);
            m_offset = m_min - m_scale * min;
        }

        value_type modify(value_type value) const override
        {
            return value * m_scale + m_offset;
        }

    private:
        value_type m_min;
        value_type m_max;
        value_type m_scale;
        value_type m_offset;
    };
}  // namespace noise
