#pragma once

#include "noise/modifier/noise_output_modifier.h"

namespace noise
{
    class noise_intensity_modifier : public noise_output_modifier
    {
    public:
        using value_type = noise_output_modifier::value_type;

        noise_intensity_modifier(value_type intensity) :
            m_intensity(intensity),
            m_min(0),
            m_max(0)
        {
        }

        ~noise_intensity_modifier() override
        {
        }

        value_type intensity() const
        {
            return m_intensity;
        };

        value_type min() const override
        {
            return m_min;
        }

        value_type max() const override
        {
            return m_max;
        }

        void set_min_max(value_type min, value_type max) override
        {
            m_min = modify(min);
            m_max = modify(max);
        }

        value_type modify(value_type value) const override
        {
            return value * m_intensity;
        };

    private:
        value_type m_intensity;
        value_type m_min;
        value_type m_max;
    };
}  // namespace noise
