#pragma once

#include "math/math.h"

namespace noise
{
    struct droplet_erosion_simulation_params
    {
        // Seed of the simulation.
        seed_type seed = 0;

        // Size of the terrain in world units.
        vec<decimal_type, 3> terrain_size = vec<decimal_type, 3>::make_right_up_forward(128, 64, 128);

        // Gravity used in the simulation.
        decimal_type gravity = 9.81;

        // How much sediment can the water carry.
        decimal_type capacity = 4.0;
        decimal_type minimum_capacity = 0.01;
        decimal_type dissolve = 0.3;
        decimal_type deposition = 0.3;
        decimal_type evaporation = 0.01;

        // At zero, water will instantly change direction to flow downhill. At 1, water will never change direction.
        decimal_type droplet_inertia = 0.05;
        decimal_type droplet_speed = 1;
        decimal_type droplet_volume = 1;

        size_t droplet_radius = 3;
        size_t droplet_lifetime = 30;
    };
}  // namespace noise
