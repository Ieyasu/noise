#pragma once

#include "math/math.h"

namespace noise
{
    struct flow_erosion_simulation_params
    {
        // Seed of the simulation.
        seed_type seed = 0;

        // How much sediment can the water carry.
        decimal_type capacity = 1.0;
        decimal_type dissolve = 0.5;
        decimal_type deposition = 1.0;

        // Size of the terrain in world units.
        vec<decimal_type, 3> terrain_size = vec<decimal_type, 3>::make_right_up_forward(128, 64, 128);

        decimal_type talus_angle = degree_to_radian(45);

        // Time step of the simulation.
        decimal_type hydraulic_time_step = 0.01;
        decimal_type thermal_time_step = 0.1;

        // Gravity used in the simulation.
        decimal_type gravity = 9.81;

        // Controls water rain and evaporation.
        decimal_type evaporation = 1.0;
        decimal_type rain = 1.0;

        decimal_type max_erosion_depth = 10.0;

        // Virtual pipes connecting the grid cells.
        decimal_type virtual_pipe_length = 1.0;
        decimal_type virtual_pipe_area = 1.0;
    };
}  // namespace noise
