#pragma once

// Method from http://ranmantaru.com/blog/2011/10/08/water-erosion-on-heightmap-terrain/
// Code adapted from https://github.com/SebLague/Erosion-Demo

#include "container/grid2.h"
#include "math/math.h"
#include "terrain/erosion/droplet_erosion_simulation_params.h"

namespace noise
{
    class droplet_erosion_simulation
    {
    public:
        droplet_erosion_simulation(grid2f const& heightmap, droplet_erosion_simulation_params const& params);

        grid2f const& heightmap() const;
        droplet_erosion_simulation_params const& params() const;

        void update(size_t steps);

    private:
        struct erosion_brush
        {
            std::vector<vec2i> deltas;
            std::vector<decimal_type> weights;
        } m_erosion_brush;

        size_t m_grid_size;
        size_t m_grid_width;
        size_t m_grid_height;
        grid2f m_heightmap;
        random_engine_type m_random_engine;
        droplet_erosion_simulation_params m_params;

        void simulate_droplet(vec2f pos);
        erosion_brush get_brush(int radius) const;

        decimal_type calculate_height(vec2f const& pos) const;
        vec3f calculate_height_with_gradients(vec2f const& pos) const;
    };
}  // namespace noise
