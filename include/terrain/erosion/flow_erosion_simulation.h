#include <random>

#include "container/grid2.h"
#include "math/math.h"
#include "terrain/erosion/flow_erosion_simulation_params.h"

namespace noise
{
    class flow_erosion_simulation
    {
    public:
        flow_erosion_simulation(grid2f const& heightmap, flow_erosion_simulation_params const& params);

        grid2f const& heightmap() const;
        grid2f const& steepness() const;
        grid2f const& sediment() const;
        grid2f const& water() const;
        grid2f const& left_flux() const;
        grid2f const& right_flux() const;
        grid2f const& bottom_flux() const;
        grid2f const& top_flux() const;
        grid2<vec2f> const& velocity() const;

        flow_erosion_simulation_params const& params() const;

        void update(unsigned int steps);

    private:
        size_t m_grid_size;
        size_t m_grid_width;
        size_t m_grid_height;
        flow_erosion_simulation_params m_params;
        std::default_random_engine m_random_generator;

        grid2f m_heightmap;
        grid2f m_steepness;
        grid2f m_sediment;
        grid2f m_tmp_buffer;
        grid2f m_water;
        grid2f m_l_flux;
        grid2f m_r_flux;
        grid2f m_b_flux;
        grid2f m_t_flux;
        grid2<vec2f> m_velocity;

        void update_flux();
        void update_water();
        void update_material_slippage();
        void update_erosion();
        void update_sediment_transportation();

        static decimal_type get_delta_height(decimal_type delta, decimal_type max_delta);
    };
}  // namespace noise
