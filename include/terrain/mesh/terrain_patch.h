#pragma once

#include <vector>

#include "math/math.h"

namespace noise
{
    struct TerrainPatch
    {
        std::vector<vec3f> vertices;
        std::vector<int> triangles;
    };
}  // namespace noise
