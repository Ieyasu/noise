#pragma once

#include <unordered_map>
#include <vector>

#include "container/grid2.h"
#include "container/kary_tree.h"
#include "math/math.h"
#include "math/shape/aabb.h"
#include "terrain/mesh/terrain_patch.h"

namespace noise
{
    class Terrain
    {
    public:
        using lod_type = uint32_t;

        lod_type max_lod() const;
        grid2f const& heightmap() const;

        vec3f const& patch_size() const;
        vec2s const& patch_resolution() const;
        vec2s const& patch_count() const;

        vec3f const& world_size() const;
        vec2s const& world_resolution() const;

        void set_heightmap(grid2f const& heightmap);
        void set_world_size(vec3f const& world_size);

        TerrainPatch generate_patch(lod_type lod) const;

    private:
        grid2f m_heightmap;
        lod_type m_max_lod;
        vec2s m_patch_count;
        vec2s m_patch_resolution;
        vec3f m_patch_size;
        vec3f m_world_size;
        Quadtree<AABB> m_quadtree;
        std::vector<> m_visible_patches;

        lod_type calculate_lod(decimal_type distance) const;
        bool calculate_visibility(AABB const& bounds) const;

        void update_bounding_boxes();
        void update_patch_size();
        void update_quadtree_depth();
        void update_visibility();
    };
}  // namespace noise
