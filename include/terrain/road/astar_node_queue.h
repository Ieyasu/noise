#pragma once

#include <unordered_map>
#include <vector>

#include "terrain/road/astar_node_pool.h"

namespace noise
{
    class astar_node_comparator
    {
    public:
        astar_node_comparator(astar_node_pool* pool) :
            m_pool(pool)
        {
        }

        inline bool operator()(int handle1, int handle2)
        {
            return m_pool->get_node(handle1).cost_heuristic > m_pool->get_node(handle2).cost_heuristic;
        }

    private:
        astar_node_pool* m_pool;
    };

    class astar_node_queue
    {
    public:
        astar_node_queue(astar_node_pool* pool);
        bool empty() const;
        size_t size() const;
        int top() const;
        void clear();
        void pop();
        void push(int handle);
        void modify(int handle);

    private:
        size_t m_size;
        astar_node_pool* m_pool;
        std::vector<int> m_heap;
        std::unordered_map<int, size_t> m_indices;

        decimal_type get_cost(int handle) const;
        void bubble_up(int i, int handle);
        void trickle_down(int handle);
    };
}  // namespace noise
