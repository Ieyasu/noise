#pragma once

#include <vector>

#include "container/grid2.h"
#include "math/math.h"
#include "terrain/road/road_edge.h"
#include "terrain/road/road_vertex.h"

namespace noise
{
    class road_generator
    {
    public:
        // Generate a road graph between the given vertices.
        std::vector<road_edge> generate_road_graph(std::vector<road_vertex> const& vertices) const;

        // Generate road paths from the given road graph and terrain heightmap.
        std::vector<std::vector<vec2f>> generate_road_paths(std::vector<road_vertex> const& vertices, std::vector<road_edge> const& edges, grid2f const& heightmap) const;

    private:
        // Cost functions.
        decimal_type cost_heuristic(vec2i const& c1, vec2i const& c2, grid2f const& heightmap) const;
        decimal_type cost(vec2i const& c1, vec2i const& c2, vec2i const& c3, grid2f const& heightmap) const;
        decimal_type cost_slope(decimal_type slope) const;
        decimal_type cost_water(decimal_type height) const;
        decimal_type cost_curvature(decimal_type curvature) const;

        int gcd(int a, int b) const;
        std::vector<vec2i> get_mask(int k) const;
    };
}  // namespace noise
