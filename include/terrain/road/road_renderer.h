#pragma once

#include <memory>
#include <vector>

#include "math/math.h"
#include "math/shape/spline2.h"
#include "terrain/heightmap/heightmap_renderer.h"

namespace noise
{
    class road_renderer
    {
    public:
        road_renderer(size_t width, size_t height);

        grid2f& result();
        grid2f const& result() const;
        void draw(std::vector<std::vector<vec2f>> const& points);

    private:
        heightmap_renderer m_renderer;

        void draw_catmull_rom_spline(vec2f const& p0, vec2f const& p1, vec2f const& p2, vec2f const& p3);
    };
}  // namespace noise
