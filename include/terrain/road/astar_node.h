#pragma once

#include <limits>

#include "math/math.h"
#include "terrain/road/astar_node_state.h"

namespace noise
{
    struct astar_node
    {
        int parent_handle = -1;
        astar_node_state state;
        vec2i position;
        decimal_type cost = std::numeric_limits<decimal_type>::max();
        decimal_type cost_heuristic = std::numeric_limits<decimal_type>::max();
    };
}  // namespace noise
