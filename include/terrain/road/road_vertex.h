#pragma once

#include <vector>

#include "math/math.h"

namespace noise
{
    class road_vertex
    {
    public:
        road_vertex();
        road_vertex(decimal_type x, decimal_type y);

        decimal_type x() const;
        decimal_type y() const;

    private:
        decimal_type m_x;
        decimal_type m_y;
    };
}  // namespace noise
