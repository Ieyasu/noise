#pragma once

#include <unordered_map>
#include <vector>

#include "terrain/road/astar_node.h"

namespace noise
{
    class astar_node_pool
    {
    public:
        void clear();
        int create_node(int index);
        astar_node& get_node(int handle);

    private:
        std::vector<astar_node> m_nodes;
        std::unordered_map<int, int> m_handles;
    };
}  // namespace noise
