#pragma once

#include "terrain/road/road_graph.h"
#include "terrain/road/road_vertex.h"

namespace noise
{
    class road_edge
    {
    public:
        road_edge();
        road_edge(road_vertex const& v1, road_vertex const& v2, vertex_t id1, vertex_t id2);
        size_t id1() const;
        size_t id2() const;
        decimal_type length() const;
        decimal_type radius() const;

        decimal_type m_length;

    private:
        size_t m_id1;
        size_t m_id2;
        decimal_type m_radius;
    };
}  // namespace noise
