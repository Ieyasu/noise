#pragma once

namespace noise
{
    enum astar_node_state
    {
        none,
        open,
        closed,
    };
}
