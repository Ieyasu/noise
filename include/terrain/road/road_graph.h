#pragma once

#include <boost/graph/adjacency_list.hpp>

namespace noise
{
    class road_edge;
    class road_vertex;
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, road_vertex, road_edge> road_graph;
    typedef boost::graph_traits<road_graph>::edge_descriptor edge_t;
    typedef boost::graph_traits<road_graph>::vertex_descriptor vertex_t;
}  // namespace noise
