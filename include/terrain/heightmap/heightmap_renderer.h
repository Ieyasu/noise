#pragma once

#include <memory>
#include <vector>

#include "container/grid2.h"
#include "math/math.h"
#include "math/shape/line_segment2.h"
#include "math/shape/rect.h"
#include "math/shape/spline2.h"
#include "noise/noise_function.h"
#include "terrain/heightmap/heightmap_renderer_blend_mode.h"

namespace noise
{
    class heightmap_renderer
    {
    public:
        using value_type = decimal_type;
        using bounds_type = rect<decimal_type>;

        heightmap_renderer(size_t width, size_t height);

        grid2f& heightmap();
        grid2f const& heightmap() const;
        value_type color() const;
        value_type opacity() const;
        value_type thickness() const;
        heightmap_renderer_blend_mode blend_mode() const;

        void set_color(value_type color);
        void set_opacity(value_type opacity);
        void set_thickness(value_type thickness);
        void set_blend_mode(heightmap_renderer_blend_mode blend_mode);

        void clear();
        void fill();
        void draw_line_segment(line_segment2 const& segment);
        void draw_line_segment(vec2f const& start, vec2f const& end);
        void draw_spline(spline2 const& spline);
        void draw_point(vec2f const& point);
        void draw_noise(noise_function const& source, bounds_type const& bounds);
        void draw_noise(noise_function const& source, bounds_type const& bounds, grid2f const& turbulence_x, grid2f const& turbulence_y, float turbulence_strength);
        void draw_heightmap(grid2f const& source);
        void draw_steepness_map(grid2f const& source, vec3f const& map_size);

    private:
        grid2f m_heightmap;
        value_type m_color;
        value_type m_opacity;
        value_type m_thickness;
        heightmap_renderer_blend_mode m_blend_mode;

        void set_pixel(int x, int y, value_type color);
    };
}  // namespace noise
