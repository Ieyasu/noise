#pragma once

namespace noise
{
    enum heightmap_renderer_blend_mode
    {
        ADD = 1,
        LERP = 2,
        MULTIPLY = 3,
        OVERWRITE = 4,
        SUBTRACT = 5,
    };
}  // namespace noise
