#pragma once

#include "math/vec2.h"

namespace noise
{
    class spline2
    {
    public:
        virtual ~spline2()
        {
        }

        virtual vec2f interpolate(decimal_type t) const = 0;
    };
}  // namespace noise
