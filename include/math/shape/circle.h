#pragma once

#include "math/math.h"

namespace noise
{
    class circle
    {
    public:
        vec2f origin;
        decimal_type radius;

        circle() :
            circle(vec2f(), 0)
        {
        }

        circle(vec2f const& p_origin, decimal_type p_radius) :
            origin(p_origin),
            radius(p_radius)
        {
        }
    };
}  // namespace noise
