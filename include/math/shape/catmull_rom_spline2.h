#pragma once

#include "math/shape/spline2.h"

namespace noise
{
    class catmull_rom_spline2 : public spline2
    {
    public:
        catmull_rom_spline2(vec2f const& p0, vec2f const& p1, vec2f const& p2, vec2f const& p3)
        {
            set_points(p0, p1, p2, p3);
        }

        ~catmull_rom_spline2() override
        {
        }

        vec2f interpolate(decimal_type t) const override
        {
            t = (1 - t) * m_t1 + t * m_t2;

            auto const a1 = (m_t1 - t) / (m_t1 - m_t0) * m_p0 + (t - m_t0) / (m_t1 - m_t0) * m_p1;
            auto const a2 = (m_t2 - t) / (m_t2 - m_t1) * m_p1 + (t - m_t1) / (m_t2 - m_t1) * m_p2;
            auto const a3 = (m_t3 - t) / (m_t3 - m_t2) * m_p2 + (t - m_t2) / (m_t3 - m_t2) * m_p3;
            auto const b1 = (m_t2 - t) / (m_t2 - m_t0) * a1 + (t - m_t0) / (m_t2 - m_t0) * a2;
            auto const b2 = (m_t3 - t) / (m_t3 - m_t1) * a2 + (t - m_t1) / (m_t3 - m_t1) * a3;
            return (m_t2 - t) / (m_t2 - m_t1) * b1 + (t - m_t1) / (m_t2 - m_t1) * b2;
        }

        void set_points(vec2f const& p0, vec2f const& p1, vec2f const& p2, vec2f const& p3)
        {
            m_p0 = p0;
            m_p1 = p1;
            m_p2 = p2;
            m_p3 = p3;
            m_t0 = 0;
            m_t1 = get_t(m_t0, p0, p1);
            m_t2 = get_t(m_t1, p1, p2);
            m_t3 = get_t(m_t2, p2, p3);
        }

    private:
        decimal_type m_t0;
        decimal_type m_t1;
        decimal_type m_t2;
        decimal_type m_t3;
        vec2f m_p0;
        vec2f m_p1;
        vec2f m_p2;
        vec2f m_p3;

        inline decimal_type get_t(decimal_type t, vec2f const& p0, vec2f const& p1) const
        {
            return std::sqrt((p0 - p1).magnitude()) + t;
        }
    };
}  // namespace noise
