#pragma once

#include "math/math.h"

namespace noise
{
    class line_segment2
    {
    public:
        vec2f start;
        vec2f end;

        line_segment2() :
            line_segment2(vec2f(), vec2f())
        {
        }

        line_segment2(vec2f const& s, vec2f const& e) :
            start(s),
            end(e)
        {
        }
    };
}  // namespace noise
