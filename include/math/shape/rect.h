#pragma once

#include "math/math.h"

namespace noise
{
    template <typename T>
    class rect
    {
    public:
        using value_type = T;

        value_type x;
        value_type y;
        value_type width;
        value_type height;

        rect() :
            rect(0, 0, 0, 0)
        {
        }

        rect(value_type p_x, value_type p_y, value_type p_w, value_type p_h) :
            x(p_x),
            y(p_y),
            width(p_w),
            height(p_h)
        {
        }
    };

    using rectf = rect<decimal_type>;
}  // namespace noise
