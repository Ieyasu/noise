
#pragma once

#include <algorithm>

#include "math/math.h"

namespace noise
{
    class AABB
    {
    public:
        using value_type = decimal_type;
        using vector_type = vec<value_type, 3>;

        constexpr AABB();
        constexpr AABB(vector_type const& min, vector_type const& max);

        vector_type const& min() const;
        vector_type const& max() const;
        vector_type size() const;
        vector_type center() const;
        void set_bounds(vector_type const& min, vector_type const& max);
        void encapsulate(AABB const& other);

        AABB::vector_type closest_point(vector_type const& p) const;
        AABB::value_type distance_to_point(vector_type const& p) const;
        AABB::value_type distance_sqr_to_point(vector_type const& p) const;

        static AABB combine(std::initializer_list<AABB> boxes);

    private:
        vector_type m_min;
        vector_type m_max;
    };

    constexpr AABB::AABB() :
        m_min(),
        m_max()
    {
    }

    constexpr AABB::AABB(vector_type const& min, vector_type const& max) :
        m_min(),
        m_max()
    {
        set_bounds(min, max);
    }

    inline AABB::vector_type const& AABB::min() const
    {
        return m_min;
    }

    inline AABB::vector_type const& AABB::max() const
    {
        return m_max;
    }

    inline AABB::vector_type AABB::center() const
    {
        return value_type(0.5) * (m_min + m_max);
    }

    inline AABB::vector_type AABB::size() const
    {
        return m_max - m_min;
    }

    inline void AABB::set_bounds(vector_type const& min, vector_type const& max)
    {
        m_min = min;
        m_max = max;
        if (m_min.x > m_max.x)
        {
            std::swap(m_min.x, m_max.x);
        }
        if (m_min.y > m_max.y)
        {
            std::swap(m_min.y, m_max.y);
        }
        if (m_min.z > m_max.z)
        {
            std::swap(m_min.z, m_max.z);
        }
    }

    inline void AABB::encapsulate(AABB const& other)
    {
        m_min = vector_type::min(m_min, other.m_min);
        m_max = vector_type::max(m_max, other.m_max);
    }

    inline AABB::vector_type AABB::closest_point(vector_type const& p) const
    {
        return vec3f::clamp(p, m_min, m_max);
    }

    inline AABB::value_type AABB::distance_to_point(vector_type const& p) const
    {
        return (p - closest_point(p)).magnitude();
    }

    inline AABB::value_type AABB::distance_sqr_to_point(vector_type const& p) const
    {
        return (p - closest_point(p)).sqr_magnitude();
    }

    inline static AABB combine(AABB const& box1, AABB const& box2)
    {
        auto result = box1;
        result.encapsulate(box2);
        return result;
    }

    inline static AABB combine(std::initializer_list<AABB> boxes)
    {
        if (boxes.size() == 0)
        {
            return AABB();
        }

        auto result = *boxes.begin();
        for (auto it = boxes.begin() + 1; it != boxes.end(); ++it)
        {
            result.encapsulate(*it);
        }
        return result;
    }
}  // namespace noise
