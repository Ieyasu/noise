#pragma once

#include <algorithm>
#include <cmath>
#include <ostream>

namespace noise
{
    template <typename T, size_t TDimension>
    class vec
    {
    public:
        constexpr vec(vec const& vec) = default;

        constexpr vec() :
            vec(0)
        {
        }

        constexpr vec(T value)
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                m_data[i] = value;
            }
        }

        template <typename... Args>
        constexpr vec(Args... args) :
            m_data(args...)
        {
        }

        size_t size() const
        {
            return TDimension;
        }

        T& operator[](int i)
        {
            return m_data[i];
        }

        T operator[](int i) const
        {
            return m_data[i];
        }

        template <typename U>
        vec operator*(U value) const
        {
            auto result = vec();
            for (size_t i = 0; i < TDimension; ++i)
            {
                result[i] = m_data[i] * value;
            }
            return result;
        }

        template <typename U>
        vec& operator*=(U value)
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                m_data[i] *= value;
            }
            return *this;
        }

        template <typename U>
        vec operator/(U value) const
        {
            auto result = vec();
            for (size_t i = 0; i < TDimension; ++i)
            {
                result[i] = m_data[i] / value;
            }
            return result;
        }

        template <typename U>
        vec& operator/=(U value)
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                m_data[i] /= value;
            }
            return *this;
        }

        vec operator+(vec const& other) const
        {
            auto result = vec();
            for (size_t i = 0; i < TDimension; ++i)
            {
                result[i] = m_data[i] + other[i];
            }
            return result;
        }

        vec& operator+=(vec const& other)
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                m_data[i] += other[i];
            }
            return *this;
        }

        vec operator-(vec const& other) const
        {
            auto result = vec();
            for (size_t i = 0; i < TDimension; ++i)
            {
                result[i] = m_data[i] - other[i];
            }
            return result;
        }

        vec& operator-=(vec const& other)
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                m_data[i] -= other[i];
            }
            return *this;
        }

        bool operator==(vec const& other) const
        {
            for (size_t i = 0; i < TDimension; ++i)
            {
                if (m_data[i] != other[i])
                {
                    return false;
                }
            }
            return true;
        }

        bool operator!=(vec const& other) const
        {
            return !this->operator==(other);
        }

        float magnitude() const
        {
            return std::sqrt(sqr_magnitude());
        }

        T sqr_magnitude() const
        {
            auto value = T(0);
            for (size_t i = 0; i < TDimension; ++i)
            {
                value += m_data[i] * m_data[i];
            }
            return value;
        }

        void normalize()
        {
            auto mag = magnitude();
            if (mag > 0)
            {
                for (size_t i = 0; i < TDimension; ++i)
                {
                    m_data[i] /= mag;
                }
            }
        }

        vec normalized() const
        {
            auto copy = *this;
            copy.normalize();
            return copy;
        }

        T min()
        {
            auto min = m_data[0];
            for (size_t i = 1; i < TDimension; ++i)
            {
                min = std::min(min, m_data[i]);
            }
            return min;
        }

        T max()
        {
            auto max = m_data[0];
            for (size_t i = 1; i < TDimension; ++i)
            {
                max = std::max(max, m_data[i]);
            }
            return max;
        }

        static T dot(vec const& vec1, vec const& vec2)
        {
            auto value = T(0);
            for (size_t i = 0; i < TDimension; ++i)
            {
                value += vec1[i] * vec2[i];
            }
            return value;
        }

    protected:
        T m_data[TDimension];
    };

    template <typename U, typename T, uint8_t TDimension>
    vec<T, TDimension> operator*(U value, vec<T, TDimension> const& v)
    {
        return v * value;
    }

    template <typename T, uint8_t TDimension>
    std::ostream& operator<<(std::ostream& out, vec<T, TDimension> const& v)
    {
        out << "vec" << TDimension << "(" << v[0];
        for (size_t i = 1; i < TDimension; ++i)
        {
            out << ", " << v[i];
        }
        out << ")";
        return out;
    }
}  // namespace noise
