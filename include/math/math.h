#pragma once

#include <algorithm>
#include <cmath>
#include <random>

#include "math/vec2.h"
#include "math/vec3.h"

/* #include "simdpp/simd.h" */

namespace noise
{
#ifdef NOISE_USE_DOUBLE
    using decimal_type = double;
#else
    using decimal_type = float;
#endif

    using vec2i = vec<int32_t, 2>;
    using vec2u = vec<uint32_t, 2>;
    using vec2s = vec<size_t, 2>;
    using vec2f = vec<decimal_type, 2>;

    using vec3i = vec<int32_t, 3>;
    using vec3u = vec<uint32_t, 3>;
    using vec3s = vec<size_t, 3>;
    using vec3f = vec<decimal_type, 3>;

    static constexpr decimal_type PI = decimal_type(M_PI);

#ifdef NOISE_USE_32_BIT_RANDOM
    using random_engine_type = std::mt19937;
    using seed_type = random_engine_t::result_type;
#else
    using random_engine_type = std::mt19937_64;
    using seed_type = random_engine_type::result_type;
#endif

    template <typename T, typename U>
    inline constexpr T lerp(T a, T b, U t)
    {
        t = std::clamp(t, U(0), U(1));
        return a + t * (b - a);
    }

    template <typename T, typename U>
    inline constexpr T lerp_unclamped(T a, T b, U t)
    {
        return a + t * (b - a);
    }

    template <typename T, typename U>
    inline constexpr T cerp(T a, T b, T c, T d, U t)
    {
        auto const x0 = (d - c) - (a - b);
        auto const x1 = (a - b) - x0;
        auto const x2 = c - a;
        auto const x3 = b;
        return ((x0 * t + x1) * t + x2) * t + x3;
    }

    template <typename T>
    inline constexpr int fast_floor(T f)
    {
        auto const i = static_cast<int>(f);
        return (f < i) ? (i - 1) : i;
    }

    template <typename T>
    inline constexpr int fast_round(T f)
    {
        return (f >= 0) ? static_cast<int>(f + T(0.5)) : static_cast<int>(f - T(0.5));
    }

    template <typename T>
    inline constexpr bool is_power_of_two(T n)
    {
        return (n <= 0) ? false : ((n & (n - 1)) == 0);
    }

    template <typename T>
    inline constexpr double degree_to_radian(T degree)
    {
        return double(degree) * M_PI / 180;
    }

    inline constexpr double degree_to_radian(double degree)
    {
        return degree * M_PI / 180;
    }

    inline constexpr float degree_to_radian(float degree)
    {
        return degree * static_cast<float>(M_PI) / 180;
    }

    template <typename T>
    inline constexpr double radian_to_degree(T radian)
    {
        return double(radian) * 180 / M_PI;
    }

    inline constexpr double radian_to_degree(double radian)
    {
        return radian * 180 / M_PI;
    }

    inline constexpr float radian_to_degree(float degree)
    {
        return degree * 180 / static_cast<float>(M_PI);
    }

    template <typename T>
    inline constexpr T powi(T n, uint8_t exp)
    {
        if (is_power_of_two(n))
        {
            return 1 << ((n >> 1) * exp);
        }

        T result = 1;
        while (true)
        {
            if (exp & 1)
            {
                result *= n;
            }

            exp = exp >> 1;
            if (exp == 0)
            {
                break;
            }

            n *= n;
        }
        return result;
    }

    inline constexpr uint16_t morton_encode(uint8_t x, uint8_t y)
    {
        uint16_t tx = x;
        tx = (tx | (tx << 4)) & 0x0F0F;
        tx = (tx | (tx << 2)) & 0x3333;
        tx = (tx | (tx << 1)) & 0x5555;

        uint16_t ty = y;
        ty = (ty | (ty << 4)) & 0x0F0F;
        ty = (ty | (ty << 2)) & 0x3333;
        ty = (ty | (ty << 1)) & 0x5555;

        return tx & (ty << 1);
    }

    inline constexpr uint32_t morton_encode(uint16_t x, uint16_t y)
    {
        uint32_t tx = x;
        tx = (tx | (tx << 8)) & 0x00FF00FF;
        tx = (tx | (tx << 4)) & 0x0F0F0F0F;
        tx = (tx | (tx << 2)) & 0x33333333;
        tx = (tx | (tx << 1)) & 0x55555555;

        uint32_t ty = y;
        ty = (ty | (ty << 8)) & 0x00FF00FF;
        ty = (ty | (ty << 4)) & 0x0F0F0F0F;
        ty = (ty | (ty << 2)) & 0x33333333;
        ty = (ty | (ty << 1)) & 0x55555555;

        return tx & (ty << 1);
    }

    inline constexpr uint64_t morton_encode(uint32_t x, uint32_t y)
    {
        uint64_t tx = x;
        tx = (tx | (tx << 16)) & 0x0000FFFF0000FFFF;
        tx = (tx | (tx << 8)) & 0x00FF00FF00FF00FF;
        tx = (tx | (tx << 4)) & 0x0F0F0F0F0F0F0F0F;
        tx = (tx | (tx << 2)) & 0x3333333333333333;
        tx = (tx | (tx << 1)) & 0x5555555555555555;

        uint64_t ty = y;
        ty = (ty | (ty << 16)) & 0x0000FFFF0000FFFF;
        ty = (ty | (ty << 8)) & 0x00FF00FF00FF00FF;
        ty = (ty | (ty << 4)) & 0x0F0F0F0F0F0F0F0F;
        ty = (ty | (ty << 2)) & 0x3333333333333333;
        ty = (ty | (ty << 1)) & 0x5555555555555555;

        return tx & (ty << 1);
    }

    inline constexpr vec<uint8_t, 2> morton_decode(uint16_t idx)
    {
        uint16_t tx = idx & 0x5555;
        tx = (tx | (tx >> 1)) & 0x3333;
        tx = (tx | (tx >> 2)) & 0x0F0F;
        tx = (tx | (tx >> 4)) & 0x00FF;

        uint16_t ty = (idx >> 1) & 0x5555;
        ty = (ty | (ty >> 1)) & 0x3333;
        ty = (ty | (ty >> 2)) & 0x0F0F;
        ty = (ty | (ty >> 4)) & 0x00FF;

        return vec<uint8_t, 2>(tx, ty);
    }

    inline constexpr vec<uint16_t, 2> morton_decode(uint32_t idx)
    {
        uint32_t tx = idx & 0x55555555;
        tx = (tx | (tx >> 1)) & 0x33333333;
        tx = (tx | (tx >> 2)) & 0x0F0F0F0F;
        tx = (tx | (tx >> 4)) & 0x00FF00FF;
        tx = (tx | (tx >> 8)) & 0x0000FFFF;

        uint32_t ty = (idx >> 1) & 0x55555555;
        ty = (ty | (ty >> 1)) & 0x33333333;
        ty = (ty | (ty >> 2)) & 0x0F0F0F0F;
        ty = (ty | (ty >> 4)) & 0x00FF00FF;
        ty = (ty | (ty >> 8)) & 0x0000FFFF;

        return vec<uint16_t, 2>(tx, ty);
    }

    inline constexpr vec<uint32_t, 2> morton_decode(uint64_t idx)
    {
        uint64_t tx = idx & 0x5555555555555555;
        tx = (tx | (tx >> 1)) & 0x3333333333333333;
        tx = (tx | (tx >> 2)) & 0x0F0F0F0F0F0F0F0F;
        tx = (tx | (tx >> 4)) & 0x00FF00FF00FF00FF;
        tx = (tx | (tx >> 8)) & 0x0000FFFF0000FFFF;
        tx = (tx | (tx >> 16)) & 0x00000000FFFFFFFF;

        uint64_t ty = (idx >> 1) & 0x5555555555555555;
        ty = (ty | (ty >> 1)) & 0x3333333333333333;
        ty = (ty | (ty >> 2)) & 0x0F0F0F0F0F0F0F0F;
        ty = (ty | (ty >> 4)) & 0x00FF00FF00FF00FF;
        ty = (ty | (ty >> 8)) & 0x0000FFFF0000FFFF;
        ty = (ty | (ty >> 16)) & 0x00000000FFFFFFFF;

        return vec<uint32_t, 2>(tx, ty);
    }

    template <class InputIterator>
    void generate_permutation(InputIterator first, InputIterator last, seed_type seed)
    {
        auto random_engine = random_engine_type(seed);
        auto i = 0;
        for (auto it = first; it != last; ++it)
        {
            *it = i++;
        }
        std::shuffle(first, last, random_engine);
    }

    /* using TEST_FLOAT32 = float; */
    /* using TEST_FLOAT64 = double; */
    /* using TEST_FLOAT32_SIMD = simdpp::float32<SIMDPP_FAST_FLOAT32_SIZE>; */
    /* using TEST_FLOAT64_SIMD = simdpp::float64<SIMDPP_FAST_FLOAT64_SIZE>; */

    /* using TEST_UINT8 = uint8_t; */
    /* using TEST_UINT16 = uint16_t; */
    /* using TEST_UINT32 = uint32_t; */
    /* using TEST_UINT64 = uint64_t; */
    /* using TEST_UINT8_SIMD = simdpp::uint8<SIMDPP_FAST_INT8_SIZE>; */
    /* using TEST_UINT16_SIMD = simdpp::uint16<SIMDPP_FAST_INT16_SIZE>; */
    /* using TEST_UINT32_SIMD = simdpp::uint32<SIMDPP_FAST_INT32_SIZE>; */
    /* using TEST_UINT64_SIMD = simdpp::uint64<SIMDPP_FAST_INT64_SIZE>; */

    /* using TEST_INT8 = int8_t; */
    /* using TEST_INT16 = int16_t; */
    /* using TEST_INT32 = int32_t; */
    /* using TEST_INT64 = int64_t; */
    /* using TEST_INT8_SIMD = simdpp::int8<SIMDPP_FAST_INT8_SIZE>; */
    /* using TEST_INT16_SIMD = simdpp::int16<SIMDPP_FAST_INT16_SIZE>; */
    /* using TEST_INT32_SIMD = simdpp::int32<SIMDPP_FAST_INT32_SIZE>; */
    /* using TEST_INT64_SIMD = simdpp::int64<SIMDPP_FAST_INT64_SIZE>; */

    /* #ifdef NOISE_USE_DOUBLE */
    /*     using TEST_FLOAT = TEST_FLOAT64; */
    /*     using TEST_FLOAT_SIMD = TEST_FLOAT64_SIMD; */
    /* #else */
    /*     using TEST_FLOAT = TEST_FLOAT32; */
    /*     using TEST_FLOAT_SIMD = TEST_FLOAT32_SIMD; */
    /* #endif */
}  // namespace noise

namespace noise
{
    namespace simd
    {
        /* using simdpp::to_int32; */
        /* using simdpp::to_float32; */
        /* using simdpp::sqrt; */
        /* using simdpp::floor; */
        /* using simdpp::make_float; */
        /* using simdpp::make_int; */
        /* using simdpp::make_uint; */

        /* inline TEST_INT32_SIMD hash(TEST_INT32_SIMD seed, TEST_INT32_SIMD x, TEST_INT32_SIMD y) */
        /* { */
        /*     auto hash = seed; */
        /*     hash = x ^ hash; */
        /*     hash = y ^ hash; */
        /*     hash = hash * hash * hash * make_int<TEST_INT32_SIMD>(60493); */
        /*     hash = (hash >> 13) ^ hash; */
        /*     return hash; */
        /* } */

        /* inline TEST_INT32_SIMD hash(TEST_INT32_SIMD seed, TEST_INT32_SIMD x, TEST_INT32_SIMD y, TEST_INT32_SIMD z) */
        /* { */
        /*     auto hash = seed; */
        /*     hash = x ^ hash; */
        /*     hash = y ^ hash; */
        /*     hash = z ^ hash; */
        /*     hash = hash * hash * hash * make_int<TEST_INT32_SIMD>(60493); */
        /*     hash = (hash >> 13) ^ hash; */
        /*     return hash; */
        /* } */

        /* inline TEST_INT32_SIMD hash(TEST_INT32_SIMD seed, TEST_INT32_SIMD x, TEST_INT32_SIMD y, TEST_INT32_SIMD z, TEST_INT32_SIMD w) */
        /* { */
        /*     auto hash = seed; */
        /*     hash = x ^ hash; */
        /*     hash = y ^ hash; */
        /*     hash = z ^ hash; */
        /*     hash = w ^ hash; */
        /*     hash = hash * hash * hash * make_int<TEST_INT32_SIMD>(60493); */
        /*     hash = (hash >> 13) ^ hash; */
        /*     return hash; */
        /* } */
    }  // namespace simd
}  // namespace noise
