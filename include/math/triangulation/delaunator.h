#pragma once

#include <algorithm>
#include <cmath>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <utility>
#include <vector>

#include "terrain/road/road_vertex.h"

namespace noise
{
    //@see https://stackoverflow.com/questions/33333363/built-in-mod-vs-custom-mod-function-improve-the-performance-of-modulus-op/33333636#33333636
    inline size_t fast_mod(size_t i, size_t c)
    {
        return i >= c ? i % c : i;
    }

    // Kahan and Babuska summation, Neumaier variant; accumulates less FP error
    inline decimal_type sum(std::vector<decimal_type> const& x)
    {
        auto sum = x[0];
        auto err = decimal_type(0);

        for (size_t i = 1; i < x.size(); ++i)
        {
            auto k = x[i];
            auto m = sum + k;
            err += std::fabs(sum) >= std::fabs(k) ? sum - m + k : k - m + sum;
            sum = m;
        }
        return sum + err;
    }

    inline decimal_type dist(decimal_type ax, decimal_type ay, decimal_type bx, decimal_type by)
    {
        auto dx = ax - bx;
        auto dy = ay - by;
        return dx * dx + dy * dy;
    }

    inline decimal_type circumradius(decimal_type ax, decimal_type ay, decimal_type bx, decimal_type by, decimal_type cx, decimal_type cy)
    {
        auto dx = bx - ax;
        auto dy = by - ay;
        auto ex = cx - ax;
        auto ey = cy - ay;

        auto bl = dx * dx + dy * dy;
        auto cl = ex * ex + ey * ey;
        auto d = dx * ey - dy * ex;

        auto x = (ey * bl - dy * cl) * decimal_type(0.5) / d;
        auto y = (dx * cl - ex * bl) * decimal_type(0.5) / d;

        if ((bl > 0.0 || bl < 0.0) && (cl > 0.0 || cl < 0.0) && (d > 0.0 || d < 0.0))
        {
            return x * x + y * y;
        }
        else
        {
            return std::numeric_limits<decimal_type>::max();
        }
    }

    inline bool orient(decimal_type px, decimal_type py, decimal_type qx, decimal_type qy, decimal_type rx, decimal_type ry)
    {
        return (qy - py) * (rx - qx) - (qx - px) * (ry - qy) < 0.0;
    }

    inline std::pair<decimal_type, decimal_type> circumcenter(decimal_type ax, decimal_type ay, decimal_type bx, decimal_type by, decimal_type cx, decimal_type cy)
    {
        auto dx = bx - ax;
        auto dy = by - ay;
        auto ex = cx - ax;
        auto ey = cy - ay;

        auto bl = dx * dx + dy * dy;
        auto cl = ex * ex + ey * ey;
        auto d = dx * ey - dy * ex;

        auto x = ax + (ey * bl - dy * cl) * decimal_type(0.5) / d;
        auto y = ay + (dx * cl - ex * bl) * decimal_type(0.5) / d;

        return std::make_pair(x, y);
    }

    struct compare
    {
        std::vector<road_vertex> const& vertices;
        decimal_type cx;
        decimal_type cy;

        bool operator()(size_t i, size_t j)
        {
            auto d1 = dist(vertices[i].x(), vertices[i].y(), cx, cy);
            auto d2 = dist(vertices[j].x(), vertices[j].y(), cx, cy);
            auto diff1 = d1 - d2;
            auto diff2 = vertices[i].x() - vertices[j].x();
            auto diff3 = vertices[i].x() - vertices[j].y();

            if (diff1 > 0.0 || diff1 < 0.0)
            {
                return diff1 < 0;
            }
            else if (diff2 > 0.0 || diff2 < 0.0)
            {
                return diff2 < 0;
            }
            else
            {
                return diff3 < 0;
            }
        }
    };

    inline bool in_circle(decimal_type ax, decimal_type ay, decimal_type bx, decimal_type by, decimal_type cx, decimal_type cy, decimal_type px, decimal_type py)
    {
        auto dx = ax - px;
        auto dy = ay - py;
        auto ex = bx - px;
        auto ey = by - py;
        auto fx = cx - px;
        auto fy = cy - py;

        auto ap = dx * dx + dy * dy;
        auto bp = ex * ex + ey * ey;
        auto cp = fx * fx + fy * fy;

        return (dx * (ey * cp - bp * fy) - dy * (ex * cp - bp * fx) + ap * (ex * fy - ey * fx)) < 0.0;
    }

    constexpr decimal_type EPSILON = std::numeric_limits<decimal_type>::epsilon();
    constexpr size_t INVALID_INDEX = std::numeric_limits<size_t>::max();

    inline bool check_pts_equal(decimal_type x1, decimal_type y1, decimal_type x2, decimal_type y2)
    {
        return std::fabs(x1 - x2) <= EPSILON && std::fabs(y1 - y2) <= EPSILON;
    }

    // monotonically increases with real angle, but doesn't need expensive trigonometry
    inline decimal_type pseudo_angle(decimal_type dx, decimal_type dy)
    {
        auto p = dx / (std::abs(dx) + std::abs(dy));
        return decimal_type((dy > 0.0 ? 3.0 - p : 1.0 + p) / 4.0);  // [0..1)
    }

    struct delaunator_point
    {
        size_t i;
        decimal_type x;
        decimal_type y;
        size_t t;
        size_t prev;
        size_t next;
        bool removed;
    };

    class delaunator
    {
    public:
        std::vector<size_t> triangles;
        std::vector<size_t> halfedges;
        std::vector<size_t> hull_prev;
        std::vector<size_t> hull_next;
        std::vector<size_t> hull_tri;
        size_t hull_start;

        delaunator(std::vector<road_vertex> const& vertices);

    private:
        decimal_type m_center_x;
        decimal_type m_center_y;
        size_t m_hash_size;
        std::vector<size_t> m_hash;
        std::vector<size_t> m_edge_stack;

        decimal_type get_hull_area(std::vector<road_vertex> const& vertices);
        size_t legalize(std::vector<road_vertex> const& vertices, size_t a);
        size_t hash_key(decimal_type x, decimal_type y) const;
        size_t add_triangle(size_t i0, size_t i1, size_t i2, size_t a, size_t b, size_t c);
        void link(size_t a, size_t b);
    };

    delaunator::delaunator(std::vector<road_vertex> const& vertices) :
        triangles(),
        halfedges(),
        hull_prev(),
        hull_next(),
        hull_tri(),
        hull_start(),
        m_center_x(),
        m_center_y(),
        m_hash_size(),
        m_hash(),
        m_edge_stack()
    {
        auto ids = std::vector<size_t>(vertices.size());
        auto max_x = std::numeric_limits<decimal_type>::min();
        auto max_y = std::numeric_limits<decimal_type>::min();
        auto min_x = std::numeric_limits<decimal_type>::max();
        auto min_y = std::numeric_limits<decimal_type>::max();

        for (size_t i = 0; i < vertices.size(); ++i)
        {
            auto x = vertices[i].x();
            auto y = vertices[i].y();

            if (x < min_x)
            {
                min_x = x;
            }
            if (y < min_y)
            {
                min_y = y;
            }
            if (x > max_x)
            {
                max_x = x;
            }
            if (y > max_y)
            {
                max_y = y;
            }

            ids[i] = i;
        }

        auto cx = (min_x + max_x) / 2;
        auto cy = (min_y + max_y) / 2;
        auto min_dist = std::numeric_limits<decimal_type>::max();

        auto i0 = INVALID_INDEX;
        auto i1 = INVALID_INDEX;
        auto i2 = INVALID_INDEX;

        // pick a seed point close to the centroid
        for (size_t i = 0; i < vertices.size(); ++i)
        {
            auto d = dist(cx, cy, vertices[i].x(), vertices[i].y());
            if (d < min_dist)
            {
                i0 = i;
                min_dist = d;
            }
        }

        auto i0x = vertices[i0].x();
        auto i0y = vertices[i0].y();

        min_dist = std::numeric_limits<decimal_type>::max();

        // find the point closest to the seed
        for (size_t i = 0; i < vertices.size(); ++i)
        {
            if (i == i0)
            {
                continue;
            }
            auto d = dist(i0x, i0y, vertices[i].x(), vertices[i].y());
            if (d < min_dist && d > 0.0)
            {
                i1 = i;
                min_dist = d;
            }
        }

        auto i1x = vertices[i1].x();
        auto i1y = vertices[i1].y();

        auto min_radius = std::numeric_limits<decimal_type>::max();

        // find the third point which forms the smallest circumcircle with the first two
        for (size_t i = 0; i < vertices.size(); ++i)
        {
            if (i == i0 || i == i1)
            {
                continue;
            }

            auto r = circumradius(i0x, i0y, i1x, i1y, vertices[i].x(), vertices[i].y());
            if (r < min_radius)
            {
                i2 = i;
                min_radius = r;
            }
        }

        if (!(min_radius < std::numeric_limits<decimal_type>::max()))
        {
            throw std::runtime_error("not triangulation");
        }

        auto i2x = vertices[i2].x();
        auto i2y = vertices[i2].y();

        if (orient(i0x, i0y, i1x, i1y, i2x, i2y))
        {
            std::swap(i1, i2);
            std::swap(i1x, i2x);
            std::swap(i1y, i2y);
        }

        std::tie(m_center_x, m_center_y) = circumcenter(i0x, i0y, i1x, i1y, i2x, i2y);

        // sort the points by distance from the seed triangle circumcenter
        std::sort(ids.begin(), ids.end(), compare { vertices, m_center_x, m_center_y });

        // initialize a hash table for storing edges of the advancing convex hull
        m_hash_size = static_cast<size_t>(std::llround(std::ceil(std::sqrt(vertices.size()))));
        m_hash.resize(m_hash_size);
        std::fill(m_hash.begin(), m_hash.end(), INVALID_INDEX);

        // initialize arrays for tracking the edges of the advancing convex hull
        hull_prev.resize(vertices.size());
        hull_next.resize(vertices.size());
        hull_tri.resize(vertices.size());

        hull_start = i0;

        size_t hull_size = 3;

        hull_next[i0] = hull_prev[i2] = i1;
        hull_next[i1] = hull_prev[i0] = i2;
        hull_next[i2] = hull_prev[i1] = i0;

        hull_tri[i0] = 0;
        hull_tri[i1] = 1;
        hull_tri[i2] = 2;

        m_hash[hash_key(i0x, i0y)] = i0;
        m_hash[hash_key(i1x, i1y)] = i1;
        m_hash[hash_key(i2x, i2y)] = i2;

        size_t max_triangles = vertices.size() < 3 ? 1 : 2 * vertices.size() - 5;
        triangles.reserve(max_triangles * 3);
        halfedges.reserve(max_triangles * 3);
        add_triangle(i0, i1, i2, INVALID_INDEX, INVALID_INDEX, INVALID_INDEX);
        auto xp = std::numeric_limits<decimal_type>::quiet_NaN();
        auto yp = std::numeric_limits<decimal_type>::quiet_NaN();
        for (size_t k = 0; k < vertices.size(); ++k)
        {
            auto i = ids[k];
            auto x = vertices[i].x();
            auto y = vertices[i].y();

            // skip near-duplicate points
            if (k > 0 && check_pts_equal(x, y, xp, yp))
            {
                continue;
            }
            xp = x;
            yp = y;

            // skip seed triangle points
            if (check_pts_equal(x, y, i0x, i0y) || check_pts_equal(x, y, i1x, i1y) || check_pts_equal(x, y, i2x, i2y))
            {
                continue;
            }

            // find a visible edge on the convex hull using edge hash
            size_t start = 0;
            size_t key = hash_key(x, y);
            for (size_t j = 0; j < m_hash_size; ++j)
            {
                start = m_hash[fast_mod(key + j, m_hash_size)];
                if (start != INVALID_INDEX && start != hull_next[start])
                {
                    break;
                }
            }

            start = hull_prev[start];
            size_t e = start;
            size_t q;

            while (q = hull_next[e], !orient(x, y, vertices[e].x(), vertices[e].y(), vertices[q].x(), vertices[q].y()))
            {
                e = q;
                if (e == start)
                {
                    e = INVALID_INDEX;
                    break;
                }
            }

            if (e == INVALID_INDEX)
            {
                continue;  // likely a near-duplicate point; skip it
            }

            // add the first triangle from the point
            size_t t = add_triangle(
                    e,
                    i,
                    hull_next[e],
                    INVALID_INDEX,
                    INVALID_INDEX,
                    hull_tri[e]);

            hull_tri[i] = legalize(vertices, t + 2);
            hull_tri[e] = t;
            hull_size++;

            // walk forward through the hull, adding more triangles and flipping recursively
            size_t next = hull_next[e];
            while (q = hull_next[next], orient(x, y, vertices[next].x(), vertices[next].y(), vertices[q].x(), vertices[q].y()))
            {
                t = add_triangle(next, i, q, hull_tri[i], INVALID_INDEX, hull_tri[next]);
                hull_tri[i] = legalize(vertices, t + 2);
                hull_next[next] = next;  // mark as removed
                hull_size--;
                next = q;
            }

            // walk backward from the other side, adding more triangles and flipping
            if (e == start)
            {
                while (q = hull_prev[e], orient(x, y, vertices[q].x(), vertices[q].y(), vertices[e].x(), vertices[e].y()))
                {
                    t = add_triangle(q, i, e, INVALID_INDEX, hull_tri[e], hull_tri[q]);
                    legalize(vertices, t + 2);
                    hull_tri[q] = t;
                    hull_next[e] = e;  // mark as removed
                    hull_size--;
                    e = q;
                }
            }

            // update the hull indices
            hull_prev[i] = e;
            hull_start = e;
            hull_prev[next] = i;
            hull_next[e] = i;
            hull_next[i] = next;

            m_hash[hash_key(x, y)] = i;
            m_hash[hash_key(vertices[e].x(), vertices[e].y())] = e;
        }
    }

    decimal_type delaunator::get_hull_area(std::vector<road_vertex> const& vertices)
    {
        std::vector<decimal_type> hull_area;
        size_t e = hull_start;
        do
        {
            hull_area.push_back((vertices[e].x() - vertices[hull_prev[e]].x()) * (vertices[e].y() + vertices[hull_prev[e]].y()));
            e = hull_next[e];
        } while (e != hull_start);

        return sum(hull_area);
    }

    size_t delaunator::legalize(std::vector<road_vertex> const& vertices, size_t a)
    {
        size_t i = 0;
        size_t ar = 0;
        m_edge_stack.clear();

        // recursion eliminated with a fixed-size stack
        while (true)
        {
            size_t b = halfedges[a];

            /* if the pair of triangles doesn't satisfy the Delaunay condition
            * (p1 is inside the circumcircle of [p0, pl, pr]), flip them,
            * then do the same check/flip recursively for the new pair of triangles
            *
            *           pl                    pl
            *          /||\                  /  \
            *       al/ || \bl            al/    \a
            *        /  ||  \              /      \
            *       /  a||b  \    flip    /___ar___\
            *     p0\   ||   /p1   =>   p0\---bl---/p1
            *        \  ||  /              \      /
            *       ar\ || /br             b\    /br
            *          \||/                  \  /
            *           pr                    pr
            */
            size_t a0 = 3 * (a / 3);
            ar = a0 + (a + 2) % 3;

            if (b == INVALID_INDEX)
            {
                if (i > 0)
                {
                    i--;
                    a = m_edge_stack[i];
                    continue;
                }
                else
                {
                    //i = INVALID_INDEX;
                    break;
                }
            }

            size_t b0 = 3 * (b / 3);
            size_t al = a0 + (a + 1) % 3;
            size_t bl = b0 + (b + 2) % 3;

            size_t p0 = triangles[ar];
            size_t pr = triangles[a];
            size_t pl = triangles[al];
            size_t p1 = triangles[bl];

            bool const illegal = in_circle(
                    vertices[p0].x(),
                    vertices[p0].y(),
                    vertices[pr].x(),
                    vertices[pr].y(),
                    vertices[pl].x(),
                    vertices[pl].y(),
                    vertices[p1].x(),
                    vertices[p1].y());

            if (illegal)
            {
                triangles[a] = p1;
                triangles[b] = p0;

                auto hbl = halfedges[bl];

                // edge swapped on the other side of the hull (rare); fix the halfedge reference
                if (hbl == INVALID_INDEX)
                {
                    size_t e = hull_start;
                    do
                    {
                        if (hull_tri[e] == bl)
                        {
                            hull_tri[e] = a;
                            break;
                        }
                        e = hull_next[e];
                    } while (e != hull_start);
                }
                link(a, hbl);
                link(b, halfedges[ar]);
                link(ar, bl);
                size_t br = b0 + (b + 1) % 3;

                if (i < m_edge_stack.size())
                {
                    m_edge_stack[i] = br;
                }
                else
                {
                    m_edge_stack.push_back(br);
                }
                i++;
            }
            else
            {
                if (i > 0)
                {
                    i--;
                    a = m_edge_stack[i];
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        return ar;
    }

    inline size_t delaunator::hash_key(decimal_type x, decimal_type y) const
    {
        auto dx = x - m_center_x;
        auto dy = y - m_center_y;
        return fast_mod(
                static_cast<size_t>(std::llround(std::floor(pseudo_angle(dx, dy) * static_cast<decimal_type>(m_hash_size)))),
                m_hash_size);
    }

    size_t delaunator::add_triangle(size_t i0, size_t i1, size_t i2, size_t a, size_t b, size_t c)
    {
        auto t = triangles.size();
        triangles.push_back(i0);
        triangles.push_back(i1);
        triangles.push_back(i2);
        link(t, a);
        link(t + 1, b);
        link(t + 2, c);
        return t;
    }

    void delaunator::link(size_t a, size_t b)
    {
        auto s = halfedges.size();
        if (a == s)
        {
            halfedges.push_back(b);
        }
        else if (a < s)
        {
            halfedges[a] = b;
        }
        else
        {
            throw std::runtime_error("Cannot link edge");
        }
        if (b != INVALID_INDEX)
        {
            auto s2 = halfedges.size();
            if (b == s2)
            {
                halfedges.push_back(a);
            }
            else if (b < s2)
            {
                halfedges[b] = a;
            }
            else
            {
                throw std::runtime_error("Cannot link edge");
            }
        }
    }

}  // namespace noise
