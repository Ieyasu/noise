#pragma once

#include <string>

#include "math/vec.h"

namespace noise
{
    template <typename T>
    class vec<T, 3>
    {
    public:
        T x;
        T y;
        T z;

        constexpr vec(vec const& vec) = default;

        constexpr vec() :
            vec(0)
        {
        }

        constexpr vec(T value) :
            x(value),
            y(value),
            z(value)
        {
        }

        constexpr vec(T p_x, T p_y, T p_z) :
            x(p_x),
            y(p_y),
            z(p_z)
        {
        }

        T& operator[](int i)
        {
            switch (i)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                default:
                    throw std::out_of_range(std::to_string(i));
            }
        }

        T operator[](int i) const
        {
            switch (i)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                default:
                    throw std::out_of_range(std::to_string(i));
            }
        }

        template <typename U>
        vec operator*(U value) const
        {
            return vec(x * value, y * value, z * value);
        }

        template <typename U>
        vec& operator*=(U value)
        {
            x *= value;
            y *= value;
            z *= value;
            return *this;
        }

        template <typename U>
        vec operator/(U value) const
        {
            return vec(x / value, y / value, z / value);
        }

        template <typename U>
        vec& operator/=(U value)
        {
            x /= value;
            y /= value;
            z /= value;
            return *this;
        }

        vec operator*(vec const& other) const
        {
            return vec(x * other.x, y * other.y, z * other.z);
        }

        vec& operator*=(vec const& other)
        {
            x *= other.x;
            y *= other.y;
            z *= other.z;
            return *this;
        }

        vec operator+(vec const& other) const
        {
            return vec(x + other.x, y + other.y, z + other.z);
        }

        vec& operator+=(vec const& other)
        {
            x += other.x;
            y += other.y;
            z += other.z;
            return *this;
        }

        vec operator-(vec const& other) const
        {
            return vec(x - other.x, y - other.y, z - other.z);
        }

        vec& operator-=(vec const& other)
        {
            x -= other.x;
            y -= other.y;
            z -= other.z;
            return *this;
        }

        bool operator==(vec const& other) const
        {
            return x == other.x && y == other.y && z == other.z;
        }

        bool operator!=(vec const& other) const
        {
            return x != other.x || y != other.y || z != other.z;
        }

        float magnitude() const
        {
            return std::sqrt(sqr_magnitude());
        }

        T sqr_magnitude() const
        {
            return x * x + y * y + z * z;
        }

        void normalize()
        {
            auto const mag = magnitude();
            if (mag > 0)
            {
                x /= mag;
                y /= mag;
                z /= mag;
            }
        }

        vec normalized() const
        {
            auto copy = *this;
            copy.normalize();
            return copy;
        }

        T min() const
        {
            return std::min(std::min(x, y), z);
        }

        T max() const
        {
            return std::max(std::max(x, y), z);
        }

        T& right()
        {
            return x;
        }

        T& up()
        {
            return y;
        }

        T& forward()
        {
            return z;
        }

        T right() const
        {
            return x;
        }

        T up() const
        {
            return y;
        }

        T forward() const
        {
            return z;
        }

        static T dot(vec const& a, vec const& b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        static vec min(vec const& a, vec const& b)
        {
            return vec(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z));
        }

        static vec max(vec const& a, vec const& b)
        {
            return vec(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z));
        }

        static vec clamp(vec const& v, vec const& lo, vec const& hi)
        {
            return vec(std::clamp(v.x, lo.x, hi.x), std::clamp(v.y, lo.y, hi.y), std::clamp(v.z, lo.z, hi.z));
        }

        static constexpr vec<T, 3> zero()
        {
            return vec<T, 3>(0, 0, 0);
        }

        static constexpr vec<T, 3> unit_right()
        {
            return vec<T, 3>(1, 0, 0);
        }

        static constexpr vec<T, 3> unit_forward()
        {
            return vec<T, 3>(0, 0, 1);
        }

        static constexpr vec<T, 3> unit_up()
        {
            return vec<T, 3>(0, 1, 0);
        }

        static constexpr vec<T, 3> make_right_up_forward(T right, T up, T forward)
        {
            return vec<T, 3>(right, up, forward);
        }
    };

    template <typename U, typename T>
    vec<T, 3> operator*(U value, vec<T, 3> const& v)
    {
        return v * value;
    }

    template <typename T>
    std::ostream& operator<<(std::ostream& out, vec<T, 3> const& v)
    {
        out << "vec3"
            << "(" << v.x << ", " << v.y << ", " << v.z << ")";
        return out;
    }

}  // namespace noise
