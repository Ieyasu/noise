#pragma once

namespace noise
{
    enum interpolation_mode
    {
        nearest_neighbour,
        bilinear,
        bicubic
    };
}
