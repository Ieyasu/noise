#pragma once

#include <string>

#include "math/vec.h"

namespace noise
{
    template <typename T>
    class vec<T, 2>
    {
    public:
        T x;
        T y;

        constexpr vec(vec const& vec) = default;

        constexpr vec() :
            vec(0)
        {
        }

        constexpr vec(T value) :
            x(value),
            y(value)
        {
        }

        constexpr vec(T p_x, T p_y) :
            x(p_x),
            y(p_y)
        {
        }

        T& operator[](int i)
        {
            switch (i)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                default:
                    throw std::out_of_range(std::to_string(i));
            }
        }

        T operator[](int i) const
        {
            switch (i)
            {
                case 0:
                    return x;
                case 1:
                    return y;
                default:
                    throw std::out_of_range(std::to_string(i));
            }
        }

        template <typename U>
        vec operator*(U value) const
        {
            return vec(x * value, y * value);
        }

        template <typename U>
        vec& operator*=(U value)
        {
            x *= value;
            y *= value;
            return *this;
        }

        template <typename U>
        vec operator/(U value) const
        {
            return vec(x / value, y / value);
        }

        template <typename U>
        vec& operator/=(U value)
        {
            x /= value;
            y /= value;
            return *this;
        }

        vec operator*(vec const& other) const
        {
            return vec(x * other.x, y * other.y);
        }

        vec& operator*=(vec const& other)
        {
            x *= other.x;
            y *= other.y;
            return *this;
        }

        vec operator+(vec const& other) const
        {
            return vec(x + other.x, y + other.y);
        }

        vec& operator+=(vec const& other)
        {
            x += other.x;
            y += other.y;
            return *this;
        }

        vec operator-(vec const& other) const
        {
            return vec(x - other.x, y - other.y);
        }

        vec& operator-=(vec const& other)
        {
            x -= other.x;
            y -= other.y;
            return *this;
        }

        bool operator==(vec const& other) const
        {
            return x == other.x && y == other.y;
        }

        bool operator!=(vec const& other) const
        {
            return x != other.x || y != other.y;
        }

        float magnitude() const
        {
            return std::sqrt(sqr_magnitude());
        }

        T sqr_magnitude() const
        {
            return x * x + y * y;
        }

        void normalize()
        {
            auto const mag = magnitude();
            if (mag > 0)
            {
                x /= mag;
                y /= mag;
            }
        }

        vec normalized() const
        {
            auto copy = *this;
            copy.normalize();
            return copy;
        }

        T min() const
        {
            return std::min(x, y);
        }

        T max() const
        {
            return std::max(x, y);
        }

        T& right()
        {
            return x;
        }

        T& up()
        {
            return y;
        }

        T right() const
        {
            return x;
        }

        T up() const
        {
            return y;
        }

        static T dot(vec const& a, vec const& b)
        {
            return a.x * b.x + a.y * b.y;
        }

        static vec min(vec const& a, vec const& b)
        {
            return vec(std::min(a.x, b.x), std::min(a.y, b.y));
        }

        static vec max(vec const& a, vec const& b)
        {
            return vec(std::max(a.x, b.x), std::max(a.y, b.y));
        }

        static vec clamp(vec const& v, vec const& lo, vec const& hi)
        {
            return vec(std::clamp(v.x, lo.x, hi.x), std::clamp(v.y, lo.y, hi.y));
        }

        static constexpr vec<T, 2> zero()
        {
            return vec<T, 2>(0, 0);
        }

        static constexpr vec<T, 2> unit_right()
        {
            return vec<T, 2>(1, 0);
        }

        static constexpr vec<T, 2> unit_up()
        {
            return vec<T, 2>(0, 1);
        }

        static constexpr vec<T, 2> make_right_up(T right, T up)
        {
            return vec<T, 2>(right, up);
        }
    };

    template <typename U, typename T>
    inline vec<T, 2> operator*(U value, vec<T, 2> const& v)
    {
        return v * value;
    }

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, vec<T, 2> const& v)
    {
        out << "vec2"
            << "(" << v.x << ", " << v.y << ")";
        return out;
    }
}  // namespace noise
