#pragma once

#include <functional>
#include <stack>
#include <vector>

#include "container/kary_tree_bfs_iterator.h"
#include "container/kary_tree_dfs_iterator.h"

namespace noise
{
    template <typename T, size_t K>
    class KAryTree
    {
    public:
        friend KAryTreeNode<T, K>;
        friend KAryTreeDfsIterator<T, K>;

        using bfs_iterator = KAryTreeBfsIterator<T, K>;
        using const_bfs_iterator = KAryTreeBfsIterator<T const, K>;
        using reverse_bfs_iterator = KAryTreeReverseBfsIterator<T, K>;
        using const_reverse_bfs_iterator = KAryTreeReverseBfsIterator<T const, K>;

        using dfs_iterator = KAryTreeDfsIterator<T, K>;
        using const_dfs_iterator = KAryTreeDfsIterator<T const, K>;

        KAryTree(size_t depth);

        void initialize(size_t depth);

        KAryTreeNode<T, K> root();
        size_t k() const;
        size_t depth() const;
        size_t size() const;

        static size_t level_size(size_t depth);
        static size_t level_start(size_t depth);
        static size_t level_end(size_t depth);

        bfs_iterator make_bfs_iterator(size_t node_id);
        const_bfs_iterator make_bfs_iterator(size_t node_id) const;
        const_bfs_iterator make_const_bfs_iterator(size_t node_id) const;
        reverse_bfs_iterator make_reverse_bfs_iterator(size_t node_id);
        const_reverse_bfs_iterator make_reverse_bfs_iterator(size_t node_id) const;
        const_reverse_bfs_iterator make_const_reverse_bfs_iterator(size_t node_id) const;

        bfs_iterator begin_bfs();
        bfs_iterator end_bfs();
        const_bfs_iterator begin_bfs() const;
        const_bfs_iterator end_bfs() const;
        const_bfs_iterator cbegin_bfs() const;
        const_bfs_iterator cend_bfs() const;

        reverse_bfs_iterator rbegin_bfs();
        reverse_bfs_iterator rend_bfs();
        const_reverse_bfs_iterator rbegin_bfs() const;
        const_reverse_bfs_iterator rend_bfs() const;
        const_reverse_bfs_iterator crbegin_bfs() const;
        const_reverse_bfs_iterator crend_bfs() const;

        dfs_iterator make_dfs_iterator(size_t node_id);
        const_dfs_iterator make_dfs_iterator(size_t node_id) const;
        const_dfs_iterator make_const_dfs_iterator(size_t node_id) const;

        dfs_iterator begin_dfs();
        dfs_iterator end_dfs();
        const_dfs_iterator begin_dfs() const;
        const_dfs_iterator end_dfs() const;
        const_dfs_iterator cbegin_dfs() const;
        const_dfs_iterator cend_dfs() const;

    private:
        size_t m_depth;
        size_t m_size;

        // Keep in separate arrays for better caching performance.
        std::vector<T> m_data;
        std::vector<size_t> m_depths;
        std::vector<size_t> m_dfs_map;

        void calculate_depths();
        void calculate_dfs_map();
        size_t calculate_dfs_map(size_t idx, size_t prev_idx);
    };

    template <typename T>
    using BinaryTree = KAryTree<T, 2>;

    template <typename T>
    using Quadtree = KAryTree<T, 4>;

    template <typename T>
    using Octree = KAryTree<T, 8>;

    template <typename T, size_t K>
    KAryTree<T, K>::KAryTree(size_t depth) :
        m_depth(0),
        m_size(0)
    {
        initialize(depth);
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTree<T, K>::root()
    {
        return KAryTreeNode<T, K>(this, 0);
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::k() const
    {
        return K;
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::depth() const
    {
        return m_depth;
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::size() const
    {
        return m_size;
    }

    template <typename T, size_t K>
    inline void KAryTree<T, K>::initialize(size_t depth)
    {
        if (depth >= 15)
        {
            throw std::invalid_argument("Binarytree depth too large");
        }

        m_depth = depth;
        m_size = level_start(m_depth + 1);
        m_data.clear();
        m_data.resize(m_size);
        calculate_depths();
        calculate_dfs_map();
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::level_size(size_t depth)
    {
        return powi(K, depth);
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::level_start(size_t depth)
    {
        return (powi(K, depth) - 1) / (K - 1);
    }

    template <typename T, size_t K>
    inline size_t KAryTree<T, K>::level_end(size_t depth)
    {
        return level_start(depth + 1);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::bfs_iterator KAryTree<T, K>::make_bfs_iterator(size_t node_id)
    {
        return bfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::make_bfs_iterator(size_t node_id) const
    {
        return bfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::make_const_bfs_iterator(size_t node_id) const
    {
        return make_bfs_iterator(node_id);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::reverse_bfs_iterator KAryTree<T, K>::make_reverse_bfs_iterator(size_t node_id)
    {
        return reverse_bfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::make_reverse_bfs_iterator(size_t node_id) const
    {
        return reverse_bfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::make_const_reverse_bfs_iterator(size_t node_id) const
    {
        return make_reverse_bfs_iterator(node_id);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::bfs_iterator KAryTree<T, K>::begin_bfs()
    {
        return make_bfs_iterator(0);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::bfs_iterator KAryTree<T, K>::end_bfs()
    {
        return make_bfs_iterator(m_size);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::begin_bfs() const
    {
        return make_bfs_iterator(0);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::end_bfs() const
    {
        return make_bfs_iterator(m_size);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::cbegin_bfs() const
    {
        return begin_bfs();
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_bfs_iterator KAryTree<T, K>::cend_bfs() const
    {
        return end_bfs();
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::reverse_bfs_iterator KAryTree<T, K>::rbegin_bfs()
    {
        return make_reverse_bfs_iterator(m_size - 1);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::reverse_bfs_iterator KAryTree<T, K>::rend_bfs()
    {
        return make_reverse_bfs_iterator(-1);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::rbegin_bfs() const
    {
        return make_const_reverse_bfs_iterator(m_size - 1);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::rend_bfs() const
    {
        return make_const_reverse_bfs_iterator(-1);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::crbegin_bfs() const
    {
        return rbegin_bfs();
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_reverse_bfs_iterator KAryTree<T, K>::crend_bfs() const
    {
        return rend_bfs();
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::dfs_iterator KAryTree<T, K>::make_dfs_iterator(size_t node_id)
    {
        return dfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::make_dfs_iterator(size_t node_id) const
    {
        return dfs_iterator(KAryTreeNode<T, K>(this, node_id));
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::make_const_dfs_iterator(size_t node_id) const
    {
        return make_dfs_iterator(node_id);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::dfs_iterator KAryTree<T, K>::begin_dfs()
    {
        return make_dfs_iterator(0);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::dfs_iterator KAryTree<T, K>::end_dfs()
    {
        return make_dfs_iterator(m_size);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::begin_dfs() const
    {
        return make_dfs_iterator(0);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::end_dfs() const
    {
        return make_dfs_iterator(m_size);
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::cbegin_dfs() const
    {
        return begin_dfs();
    }

    template <typename T, size_t K>
    inline typename KAryTree<T, K>::const_dfs_iterator KAryTree<T, K>::cend_dfs() const
    {
        return end_dfs();
    }

    template <typename T, size_t K>
    void KAryTree<T, K>::calculate_depths()
    {
        m_depths.clear();
        m_depths.reserve(m_size);
        for (size_t d = 0; d <= m_depth; ++d)
        {
            auto s = level_size(d);
            for (size_t i = 0; i < s; ++i)
            {
                m_depths.push_back(d);
            }
        }
    }

    template <typename T, size_t K>
    void KAryTree<T, K>::calculate_dfs_map()
    {
        m_dfs_map.clear();
        m_dfs_map.resize(m_size);

        calculate_dfs_map(0, 0);
        m_dfs_map.back() = m_size;
        // Push one extra index to mark the end for iterators.
    }

    template <typename T, size_t K>
    size_t KAryTree<T, K>::calculate_dfs_map(size_t idx, size_t prev_idx)
    {
        auto const child_idx = K * idx + 1;
        if (child_idx >= m_size)
        {
            return idx;
        }

        prev_idx = idx;
        for (size_t i = 0; i < K; ++i)
        {
            m_dfs_map[prev_idx] = child_idx + i;
            prev_idx = calculate_dfs_map(child_idx + i, prev_idx);
        }
        return prev_idx;
    }
}  // namespace noise
