#pragma once

#include <vector>

namespace noise
{
    template <typename T, size_t K>
    class KAryTree;

    template <typename T, size_t K>
    class KAryTreeNode
    {
    public:
        KAryTreeNode(KAryTree<T, K>* tree, size_t index);

        KAryTree<T, K>& tree();
        KAryTree<T, K> const& tree() const;

        T& data();
        T const& data() const;

        bool is_leaf() const;
        size_t depth() const;
        size_t index() const;
        size_t local_index() const;
        size_t level_index() const;

        KAryTreeNode parent();
        KAryTreeNode parent() const;
        KAryTreeNode child(size_t i);
        KAryTreeNode child(size_t i) const;

    private:
        KAryTree<T, K>* m_tree;
        size_t m_index;
    };

    template <typename T, size_t K>
    inline KAryTreeNode<T, K>::KAryTreeNode(KAryTree<T, K>* tree, size_t index) :
        m_tree(tree),
        m_index(index)
    {
    }

    template <typename T, size_t K>
    inline KAryTree<T, K>& KAryTreeNode<T, K>::tree()
    {
        return *m_tree;
    }

    template <typename T, size_t K>
    inline KAryTree<T, K> const& KAryTreeNode<T, K>::tree() const
    {
        return *m_tree;
    }

    template <typename T, size_t K>
    inline T& KAryTreeNode<T, K>::data()
    {
        return m_tree->m_data[m_index];
    }

    template <typename T, size_t K>
    inline T const& KAryTreeNode<T, K>::data() const
    {
        return m_tree->m_data[m_index];
    }

    template <typename T, size_t K>
    inline bool KAryTreeNode<T, K>::is_leaf() const
    {
        return depth() == m_tree->depth();
    }

    template <typename T, size_t K>
    inline size_t KAryTreeNode<T, K>::depth() const
    {
        return m_tree->m_depths[m_index];
    }

    template <typename T, size_t K>
    inline size_t KAryTreeNode<T, K>::index() const
    {
        return m_index;
    }

    template <typename T, size_t K>
    inline size_t KAryTreeNode<T, K>::local_index() const
    {
        return (m_index == 0) ? 0 : (m_index - 1) % K;
    }

    template <typename T, size_t K>
    inline size_t KAryTreeNode<T, K>::level_index() const
    {
        return m_index - m_tree->level_start(depth());
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeNode<T, K>::parent()
    {
        return KAryTreeNode(m_tree, (m_index - 1) / K);
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeNode<T, K>::parent() const
    {
        return KAryTreeNode(m_tree, (m_index - 1) / K);
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeNode<T, K>::child(size_t i)
    {
        auto const child_index = K * m_index + i + 1;
        return KAryTreeNode(m_tree, child_index);
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeNode<T, K>::child(size_t i) const
    {
        auto const child_index = K * m_index + i + 1;
        return KAryTreeNode(m_tree, child_index);
    }
}  // namespace noise
