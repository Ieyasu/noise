#pragma once

#include <algorithm>
#include <stdexcept>
#include <vector>

#include "math/interpolation/interpolation_mode.h"
#include "math/math.h"

namespace noise
{
    template <typename T>
    class grid2
    {
    public:
        using value_type = T;
        using size_type = typename std::vector<T>::size_type;

        grid2() :
            grid2(0, 0)
        {
        }

        grid2(size_t width, size_t height) :
            m_width(width),
            m_height(height),
            m_values(std::vector<value_type>(width * height, value_type()))
        {
        }

        grid2(size_t width, size_t height, value_type const& value) :
            grid2(width, height)
        {
            fill(value);
        }

        value_type* data()
        {
            return &m_values[0];
        }

        value_type const* data() const
        {
            return &m_values[0];
        }

        size_t size() const
        {
            return m_values.size();
        }

        size_t width() const
        {
            return m_width;
        }

        size_t height() const
        {
            return m_height;
        }

        value_type const& get_wrapped(int x, int y) const
        {
            x = (x % width() + width()) % width();
            y = (y % height() + height()) % height();
            return this->operator()(x, y);
        }

        void set_wrapped(int x, int y, value_type const& value)
        {
            x = (x % width() + width()) % width();
            y = (y % height() + height()) % height();
            this->operator()(x, y) = value;
        }

        value_type const& get_clamped(int x, int y) const
        {
            x = std::clamp(x, 0, static_cast<int>(width()) - 1);
            y = std::clamp(y, 0, static_cast<int>(height()) - 1);
            return this->operator()(x, y);
        }

        void set_clamped(int x, int y, value_type const& value)
        {
            x = std::clamp(x, 0, static_cast<int>(width()) - 1);
            y = std::clamp(y, 0, static_cast<int>(height()) - 1);
            this->operator()(x, y) = value;
        }

        value_type get_interpolated_bilinear(decimal_type x, decimal_type y) const
        {
            auto const cx = std::clamp(x * (width() - 1), decimal_type(0), decimal_type(width() - 1));
            auto const cy = std::clamp(y * (width() - 1), decimal_type(0), decimal_type(height() - 1));
            auto const x0 = static_cast<int>(cx);
            auto const y0 = static_cast<int>(cy);
            auto const x1 = (x0 == static_cast<int>(width()) - 1) ? x0 : x0 + 1;
            auto const y1 = (y0 == static_cast<int>(height()) - 1) ? y0 : y0 + 1;

            auto const fx = cx - x0;
            auto const fy = cy - y0;

            auto const val11 = this->operator()(x0, y0);
            auto const val12 = this->operator()(x0, y1);
            auto const val21 = this->operator()(x1, y0);
            auto const val22 = this->operator()(x1, y1);
            auto const val1 = lerp_unclamped(val11, val21, fx);
            auto const val2 = lerp_unclamped(val12, val22, fx);
            return lerp_unclamped(val1, val2, fx);
        }

        value_type get_interpolated_bicubic(decimal_type x, decimal_type y) const
        {
            auto const cx = std::clamp(x * (width() - 1), decimal_type(0), decimal_type(width() - 1));
            auto const cy = std::clamp(y * (width() - 1), decimal_type(0), decimal_type(height() - 1));

            auto const x01 = static_cast<int>(cx);
            auto const x02 = (x01 == 0) ? 0 : x01 - 1;
            auto const x11 = (x01 == static_cast<int>(width()) - 1) ? x01 : x01 + 1;
            auto const x12 = (x11 == static_cast<int>(width()) - 1) ? x11 : x11 + 1;

            auto const y01 = static_cast<int>(cy);
            auto const y02 = (y01 == 0) ? 0 : y01 - 1;
            auto const y11 = (y01 == static_cast<int>(height()) - 1) ? y01 : y01 + 1;
            auto const y12 = (y11 == static_cast<int>(height()) - 1) ? y11 : y11 + 1;

            auto const fx = cx - x01;
            auto const fy = cy - y01;

            auto const val0 = cerp(
                    this->operator()(x02, y02),
                    this->operator()(x01, y02),
                    this->operator()(x11, y02),
                    this->operator()(x12, y02),
                    fx);
            auto const val1 = cerp(
                    this->operator()(x02, y01),
                    this->operator()(x01, y01),
                    this->operator()(x11, y01),
                    this->operator()(x12, y01),
                    fx);
            auto const val2 = cerp(
                    this->operator()(x02, y11),
                    this->operator()(x01, y11),
                    this->operator()(x11, y11),
                    this->operator()(x12, y11),
                    fx);
            auto const val3 = cerp(
                    this->operator()(x02, y12),
                    this->operator()(x01, y12),
                    this->operator()(x11, y12),
                    this->operator()(x12, y12),
                    fx);
            return cerp(val0, val1, val2, val3, fy);
        }

        value_type& operator()(size_t x, size_t y)
        {
            auto const index = width() * y + x;
            return m_values[index];
        }

        value_type const& operator()(size_t x, size_t y) const
        {
            auto const index = width() * y + x;
            return m_values[index];
        }

        value_type& operator[](size_t i)
        {
            return m_values[i];
        }

        value_type const& operator[](size_t i) const
        {
            return m_values[i];
        }

        void clear()
        {
            fill(0);
        }

        void fill(value_type const& value)
        {
            std::fill(m_values.begin(), m_values.end(), value);
        }

        void copy_to(grid2& other) const
        {
            other.m_width = m_width;
            other.m_height = m_height;
            other.m_values = m_values;
        }

        void copy_from(grid2 const& other)
        {
            m_width = other.m_width;
            m_height = other.m_height;
            m_values = other.m_values;
        }

        void scale(grid2& target, interpolation_mode mode) const
        {
            if (width() == target.width() && height() == target.height())
            {
                target = *this;
                return;
            }

            switch (mode)
            {
                case nearest_neighbour:
                    scale_nearest_neighbour(*this, target);
                    break;
                case bilinear:
                    scale_bilinear(*this, target);
                    break;
                case bicubic:
                    scale_bicubic(*this, target);
                    break;
                default:
                    throw std::invalid_argument("Unknown interpolation mode.");
            }
        }

        grid2 scale(size_t width, size_t height, interpolation_mode mode) const
        {
            auto target = grid2(width, height);
            scale(target, mode);
            return target;
        }

    private:
        size_t m_width;
        size_t m_height;
        std::vector<value_type> m_values;

        static void scale_nearest_neighbour(grid2 const& source, grid2& target)
        {
            auto const x_ratio = static_cast<int>((source.width() << 16) / target.width()) + 1;
            auto const y_ratio = static_cast<int>((source.height() << 16) / target.height()) + 1;

#pragma omp parallel for
            for (size_t i = 0; i < target.height(); ++i)
            {
                auto const y = (i * y_ratio) >> 16;
                for (size_t j = 0; j < target.width(); ++j)
                {
                    auto const x = (j * x_ratio) >> 16;
                    target[i * target.width() + j] = source[y * source.width() + x];
                }
            }
        }

        static void scale_bilinear(grid2 const& source, grid2& target)
        {
#pragma omp parallel for
            for (size_t j = 0; j < target.height(); ++j)
            {
                auto const y = j / decimal_type(target.height() - 1);
                for (size_t i = 0; i < target.width(); ++i)
                {
                    auto const x = i / decimal_type(target.width() - 1);
                    target(i, j) = source.get_interpolated_bilinear(x, y);
                }
            }
        }

        static void scale_bicubic(grid2 const& source, grid2& target)
        {
#pragma omp parallel for
            for (size_t j = 0; j < target.height(); ++j)
            {
                auto const y = j / decimal_type(target.height() - 1);
                for (size_t i = 0; i < target.width(); ++i)
                {
                    auto const x = i / decimal_type(target.width() - 1);
                    target(i, j) = source.get_interpolated_bicubic(x, y);
                }
            }
        }
    };

    using grid2i = grid2<int>;
    using grid2f = grid2<decimal_type>;
}  // namespace noise
