#pragma once

#include <iterator>

#include "container/kary_tree_node.h"

namespace noise
{
    template <typename T, size_t K>
    class KAryTreeDfsIterator : public std::iterator<std::forward_iterator_tag, KAryTreeNode<T, K>, ptrdiff_t, KAryTreeNode<T, K> const*, KAryTreeNode<T, K> const&>
    {
    public:
        KAryTreeDfsIterator(KAryTreeNode<T, K> const& node);

        KAryTreeNode<T, K> operator*();
        KAryTreeNode<T, K> operator*() const;
        KAryTreeNode<T, K>* operator->();
        KAryTreeNode<T, K> const* operator->() const;

        KAryTreeDfsIterator& jump_over();
        KAryTreeDfsIterator& operator++();
        KAryTreeDfsIterator operator++(int);

        bool operator==(KAryTreeDfsIterator const& rhs) const;
        bool operator!=(KAryTreeDfsIterator const& rhs) const;

    private:
        KAryTreeNode<T, K> m_node;
    };

    template <typename T, size_t K>
    KAryTreeDfsIterator<T, K>::KAryTreeDfsIterator(KAryTreeNode<T, K> const& node) :
        m_node(node)
    {
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeDfsIterator<T, K>::operator*()
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeDfsIterator<T, K>::operator*() const
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K>* KAryTreeDfsIterator<T, K>::operator->()
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> const* KAryTreeDfsIterator<T, K>::operator->() const
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeDfsIterator<T, K>& KAryTreeDfsIterator<T, K>::jump_over()
    {
        auto const delta_depth = m_node.tree().depth() - m_node.depth();
        auto const delta_idx = m_node.tree().level_start(delta_depth + 1);
        m_node = KAryTreeNode(&m_node.tree(), m_node.index() + delta_idx);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeDfsIterator<T, K>& KAryTreeDfsIterator<T, K>::operator++()
    {
        // Use the precomputed dfs map to calculate the next index for dfs.
        // No need to worry about out-of-bounds here. KAryTree adds an extra element ot the map.
        auto const idx = m_node.tree().m_dfs_map[m_node.index()];
        m_node = KAryTreeNode<T, K>(&m_node.tree(), idx);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeDfsIterator<T, K> KAryTreeDfsIterator<T, K>::operator++(int)
    {
        auto const it = *this;
        ++*this;
        return it;
    }

    template <typename T, size_t K>
    inline bool KAryTreeDfsIterator<T, K>::operator==(KAryTreeDfsIterator<T, K> const& rhs) const
    {
        return m_node.index() == rhs.m_node.index();
    }

    template <typename T, size_t K>
    inline bool KAryTreeDfsIterator<T, K>::operator!=(KAryTreeDfsIterator<T, K> const& rhs) const
    {
        return m_node.index() != rhs.m_node.index();
    }
}  // namespace noise
