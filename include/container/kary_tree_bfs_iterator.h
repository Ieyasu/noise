#pragma once

#include <iterator>

#include "container/kary_tree_node.h"

namespace noise
{
    template <typename T, size_t K>
    class KAryTreeBfsIterator : public std::iterator<std::bidirectional_iterator_tag, KAryTreeNode<T, K>, ptrdiff_t, KAryTreeNode<T, K> const*, KAryTreeNode<T, K> const&>
    {
    public:
        KAryTreeBfsIterator(KAryTreeNode<T, K> const& node);

        KAryTreeNode<T, K> operator*();
        KAryTreeNode<T, K> operator*() const;
        KAryTreeNode<T, K>* operator->();
        KAryTreeNode<T, K> const* operator->() const;

        KAryTreeBfsIterator& operator++();
        KAryTreeBfsIterator operator++(int);
        KAryTreeBfsIterator& operator--();
        KAryTreeBfsIterator operator--(int);

        bool operator==(KAryTreeBfsIterator const& rhs) const;
        bool operator!=(KAryTreeBfsIterator const& rhs) const;

    protected:
        KAryTreeNode<T, K> m_node;
    };

    template <typename T, size_t K>
    KAryTreeBfsIterator<T, K>::KAryTreeBfsIterator(KAryTreeNode<T, K> const& node) :
        m_node(node)
    {
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeBfsIterator<T, K>::operator*()
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeBfsIterator<T, K>::operator*() const
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K>* KAryTreeBfsIterator<T, K>::operator->()
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> const* KAryTreeBfsIterator<T, K>::operator->() const
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeBfsIterator<T, K>& KAryTreeBfsIterator<T, K>::operator++()
    {
        m_node = KAryTreeNode<T, K>(&m_node.tree(), m_node.index() + 1);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeBfsIterator<T, K> KAryTreeBfsIterator<T, K>::operator++(int)
    {
        auto const it = *this;
        ++*this;
        return it;
    }

    template <typename T, size_t K>
    inline KAryTreeBfsIterator<T, K>& KAryTreeBfsIterator<T, K>::operator--()
    {
        m_node = KAryTreeNode<T, K>(&m_node.tree(), m_node.index() - 1);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeBfsIterator<T, K> KAryTreeBfsIterator<T, K>::operator--(int)
    {
        auto const it = *this;
        --*this;
        return it;
    }

    template <typename T, size_t K>
    inline bool KAryTreeBfsIterator<T, K>::operator==(KAryTreeBfsIterator<T, K> const& rhs) const
    {
        return m_node.index() == rhs.m_node.index();
    }

    template <typename T, size_t K>
    inline bool KAryTreeBfsIterator<T, K>::operator!=(KAryTreeBfsIterator<T, K> const& rhs) const
    {
        return m_node.index() != rhs.m_node.index();
    }

    template <typename T, size_t K>
    class KAryTreeReverseBfsIterator : public std::iterator<std::bidirectional_iterator_tag, KAryTreeNode<T, K>, ptrdiff_t, KAryTreeNode<T, K> const*, KAryTreeNode<T, K> const&>
    {
    public:
        KAryTreeReverseBfsIterator(KAryTreeNode<T, K> const& node);

        KAryTreeNode<T, K> operator*();
        KAryTreeNode<T, K> operator*() const;
        KAryTreeNode<T, K>* operator->();
        KAryTreeNode<T, K> const* operator->() const;

        KAryTreeReverseBfsIterator& operator++();
        KAryTreeReverseBfsIterator operator++(int);
        KAryTreeReverseBfsIterator& operator--();
        KAryTreeReverseBfsIterator operator--(int);

        bool operator==(KAryTreeReverseBfsIterator const& rhs) const;
        bool operator!=(KAryTreeReverseBfsIterator const& rhs) const;

    protected:
        KAryTreeNode<T, K> m_node;
    };

    template <typename T, size_t K>
    KAryTreeReverseBfsIterator<T, K>::KAryTreeReverseBfsIterator(KAryTreeNode<T, K> const& node) :
        m_node(node)
    {
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeReverseBfsIterator<T, K>::operator*()
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> KAryTreeReverseBfsIterator<T, K>::operator*() const
    {
        return m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K>* KAryTreeReverseBfsIterator<T, K>::operator->()
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeNode<T, K> const* KAryTreeReverseBfsIterator<T, K>::operator->() const
    {
        return &m_node;
    }

    template <typename T, size_t K>
    inline KAryTreeReverseBfsIterator<T, K>& KAryTreeReverseBfsIterator<T, K>::operator++()
    {
        m_node = KAryTreeNode<T, K>(&m_node.tree(), m_node.index() + 1);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeReverseBfsIterator<T, K> KAryTreeReverseBfsIterator<T, K>::operator++(int)
    {
        auto const it = *this;
        ++*this;
        return it;
    }

    template <typename T, size_t K>
    inline KAryTreeReverseBfsIterator<T, K>& KAryTreeReverseBfsIterator<T, K>::operator--()
    {
        m_node = KAryTreeNode<T, K>(&m_node.tree(), m_node.index() - 1);
        return *this;
    }

    template <typename T, size_t K>
    inline KAryTreeReverseBfsIterator<T, K> KAryTreeReverseBfsIterator<T, K>::operator--(int)
    {
        auto const it = *this;
        --*this;
        return it;
    }

    template <typename T, size_t K>
    inline bool KAryTreeReverseBfsIterator<T, K>::operator==(KAryTreeReverseBfsIterator<T, K> const& rhs) const
    {
        return m_node.index() == rhs.m_node.index();
    }

    template <typename T, size_t K>
    inline bool KAryTreeReverseBfsIterator<T, K>::operator!=(KAryTreeReverseBfsIterator<T, K> const& rhs) const
    {
        return m_node.index() != rhs.m_node.index();
    }
}  // namespace noise
