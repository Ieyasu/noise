# Noise

The project has a bunch of multithreaded noise functions for C++ as well as parallelized poissons disk sampler. There are also some reference implementations for terrain erosion and road generation.

## Features

* Fully parallel
* Simplex noise
* Cellular noise
* Ridge/Turbulence noise
* Noise fractals
* Noise modifiers
* Poisson disk sampler
* Flow based heightmap erosion
* Droplet based heightmap erosion
* A* based road generation

## Getting started

Make sure to pull the submodules:

```bash
> git submodule update --init --recursive
```

If you are using an other platform than linux, manually initialize cmake in folder `build/release`. On linux you can simply run the following script from the root fo the project:

```bash
> ./scripts/init.sh
```

Build the project:

```bash
> cd build/release
> make
```

Run benchmarks and generate images:

```bash
> cd build/release/bin
> ./noise_benchmark --benchmark_repetitions=1
```

The images of all tested noise functions and erosion methods are saved to `build/release/bin`.
